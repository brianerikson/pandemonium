#version 330

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv;

out vec2 texcoord;

uniform mat4 projection;

void main() {
	texcoord = uv;
	gl_Position = projection * vec4(position, 1.0);
}