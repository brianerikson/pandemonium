#version 330

struct Material {
    sampler2D texture_diffuse1;
	vec3 diffuse1_color;
    float shininess;
};

struct Light {
    float ambient;
    float brightness;
    vec3 color;
};

in vec3 normal;
in vec2 texCoord;
in vec3 fragPos;
in vec3 lightDir;
in vec3 viewDir;
in vec3 reflectDir;

out vec4 color;

uniform vec3 viewPos;
uniform Material material;
uniform Light light;
uniform int hasTexture;
uniform int enableLighting;

void main(void) {

    // color out
	if (hasTexture == 1) {
		color = texture(material.texture_diffuse1, texCoord);
	}
	else {
		color.xyz = material.diffuse1_color;
		color.a = 1.0;
	}

	if (enableLighting == 1) {
		// diffuse
		float diffImpact = max(dot(normal, lightDir), 0.0);
		vec3 lightDiffuse = diffImpact * (light.color * light.brightness);

		// specular
		float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
		vec3 specular = spec * (light.color * light.brightness);

		// lighting out
		color.xyz += specular;
		color.xyz *= (light.ambient + lightDiffuse);
	}
}