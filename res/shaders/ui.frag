#version 330

in vec2 texcoord;
uniform sampler2D tex;
uniform vec4 color;
uniform bool hasTex;
uniform bool isText;

out vec4 outColor;

void main(void) {
	if (isText) { outColor = vec4(1, 1, 1, texture(tex, texcoord).r) * color; }
	else if (hasTex) { outColor = texture(tex, texcoord) * color; }
	else { outColor = color; }
}