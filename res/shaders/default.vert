#version 330

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 uv;

out vec3 normal;
out vec2 texCoord;
out vec3 fragPos;
out vec3 lightDir;
out vec3 viewDir;
out vec3 reflectDir;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform vec3 lightPosition;

void main() {
    mat3 normalMatrix = transpose(inverse(mat3(model)));

    // out values
    normal = normalize(normalMatrix * in_normal);
    texCoord = uv;
    
    vec3 lightPos = vec3(view * vec4(lightPosition, 1.0));
    fragPos = vec3(view * model * vec4(position, 1.0f));
    
    lightDir = normalize(lightPos - fragPos);
    viewDir = normalize(-fragPos);
    reflectDir = reflect(-lightDir, normal);

    gl_Position = projection * view * model * vec4(position, 1.0);
}