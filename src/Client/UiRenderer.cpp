#include "stdafx.h"
#include "UiRenderer.h"
#include "FontManager.h"

using namespace glm;

// START UVQuad

UVQuad::UVQuad() {
	reset();
}

void UVQuad::set(float x, float y, float width, float height) {
	GLfloat bl[2] = { x, y };
	memcpy(rectData, bl, sizeof(bl));

	GLfloat br[2] = { x + width, y };
	memcpy(rectData + 5, br, sizeof(br));

	GLfloat tr[2] = { x + width, y + height };
	memcpy(rectData + 10, tr, sizeof(tr));

	GLfloat tl[2] = { x, y + height };
	memcpy(rectData + 15, tl, sizeof(tl));
}

// Defaults to a non-inverted UV
void UVQuad::flipUV() {

	if (rectData[4] > 0.0f) { // State is inverted. Flip to normal arrangement
							  // bl
		rectData[3] = 0.0f;
		rectData[4] = 0.0f;

		// br
		rectData[8] = 1.0f;
		rectData[9] = 0.0f;

		// tr
		rectData[13] = 1.0f;
		rectData[14] = 1.0f;

		// tl
		rectData[18] = 0.0f;
		rectData[19] = 1.0f;
	}
	else {
		// bl
		rectData[3] = 0.0f;
		rectData[4] = 1.0f;

		// br
		rectData[8] = 1.0f;
		rectData[9] = 1.0f;

		// tr
		rectData[13] = 1.0f;
		rectData[14] = 0.0f;

		// tl
		rectData[18] = 0.0f;
		rectData[19] = 0.0f;
	}
}

void UVQuad::reset() {
	memcpy(rectData, DATA, 20 * sizeof(GLfloat));
}


// START UiRenderer

#define CHECK_RUNNING() if (!running) { return; }

UiRenderer::UiRenderer() 
	: ShaderProgram("res/shaders/ui.vert", "res/shaders/ui.frag"),
	  texLoc(glGetUniformLocation(programId, "tex")),
	  colorLoc(glGetUniformLocation(programId, "color")),
	  coordLoc(glGetAttribLocation(programId, "coord")),
	  projLoc(glGetUniformLocation(programId, "projection")),
	  hasTexLoc(glGetUniformLocation(programId, "hasTex")),
	  isTextLoc(glGetUniformLocation(programId, "isText")) {

	// ======================
	// Quad buffer
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &ebo);

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, 20 * sizeof(GLfloat), quad.DATA, GL_STREAM_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(GLuint), quad.INDICES, GL_STATIC_DRAW);

	// Vertex Positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), 0);

	// Vertex Texture Coords
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void*) (3 * sizeof(GLfloat)));

	glBindVertexArray(0);

	// =====================
	// Line buffer
	glGenVertexArrays(1, &lvao);
	glGenBuffers(1, &lvbo);

	glBindVertexArray(lvao);
	glBindBuffer(GL_ARRAY_BUFFER, lvbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(lineData), &lineData, GL_STREAM_DRAW);
	// Vertex Positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glBindVertexArray(0);
}

void UiRenderer::initialize(unsigned int screenWidth, unsigned int screenHeight) {
	if (initialized) { return; }
	FontManager::Instance().initialize(screenWidth, screenHeight, texLoc);

	this->defaultsLoaded = true;
	this->start();
	mat4 proj = ortho(0.0f, (float) screenWidth, (float) screenHeight, 0.0f);
	glUniformMatrix4fv(projLoc, 1, GL_FALSE, value_ptr(proj));
	glUniform1i(hasTexLoc, false);
	glUniform1i(isTextLoc, false);
	this->end();
}

void UiRenderer::start() {
	ShaderProgram::start();
	glDisable(GL_DEPTH_TEST);
}

void UiRenderer::drawBox(float x, float y, float width, float height, glm::vec4 color) {
	CHECK_RUNNING();
	glUniform4fv(colorLoc, 1, value_ptr(color));

	quad.set(x, y, width, height);

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, 20 * sizeof(GLfloat), quad.rectData, GL_STREAM_DRAW);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0);
}

void UiRenderer::drawText(EFont font, int pt, float x, float y, glm::vec4 color, const char *fmt, ...) {
	va_list ap;						// Pointer To List Of Arguments
	char text[256];					// Holds Our String

	CHECK_RUNNING();
	glUniform4fv(colorLoc, 1, value_ptr(color));
	glUniform1i(isTextLoc, true);

	if (fmt == nullptr)
		*text = 0; 
	else {
		va_start(ap, fmt);			// Parses The String For Variables
		vsprintf(text, fmt, ap);	// And Converts Symbols To Actual Numbers
		va_end(ap);					// Results Are Stored In Text
	}

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	FontManager::Instance().render(font, pt, x, y, text);
	glBindVertexArray(0);

	glUniform1i(isTextLoc, false);
}

void UiRenderer::drawLine(glm::vec2 from, glm::vec2 to, float thickness, glm::vec4 color) {
	CHECK_RUNNING();
	glUniform4fv(colorLoc, 1, value_ptr(color));
	glLineWidth(thickness);
	lineData[0] = from.x;
	lineData[1] = from.y;
	lineData[2] = to.x;
	lineData[3] = to.y;

	glBindVertexArray(lvao);
	glBindBuffer(GL_ARRAY_BUFFER, lvbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(lineData), &lineData, GL_STREAM_DRAW);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);
}

void UiRenderer::drawBorder(float x, float y, float width, float height, float thickness, glm::vec4 color) {
	CHECK_RUNNING();
	glUniform4fv(colorLoc, 1, value_ptr(color));
	vec2 tl = {x, y };
	vec2 tr = {x + width, y };
	vec2 bl = {x, y + height };
	vec2 br = {x + width, y + height };

	this->drawLine(tl, tr, thickness, color);
	this->drawLine(tr, br, thickness, color);
	this->drawLine(tl, bl, thickness, color);
	this->drawLine(bl, br, thickness, color);
}

void UiRenderer::end() {
	ShaderProgram::end();
	glEnable(GL_DEPTH_TEST);
}

#undef CHECK_RUNNING