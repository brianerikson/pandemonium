// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include <stdio.h>
#include <stdarg.h>
#include <memory>
#include <stdexcept>
#include <ctime>
#include <exception>
#include <csignal>
#include <string>
#include <locale>
#include <codecvt>
#include <vector>
#include <queue>
#include <algorithm>
#include <unordered_map>
#include <map>
#include <iostream>
#include <sstream>
#include <fstream>
#include <functional>
#include <string>
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include "assimp/cimport.h"
#include "assimp/scene.h"
#include "assimp/Importer.hpp"
#include "assimp/postprocess.h"
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"
#include "BulletCollision/CollisionDispatch/btGhostObject.h"
#include "LinearMath/btIDebugDraw.h"
#include "ft2build.h"
#include FT_FREETYPE_H
#include "fmod_studio.hpp"
#include "fmod_errors.h"
#include "fmod_common.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/quaternion.hpp"
#include "glm/gtx/euler_angles.hpp"
#include "glm/gtx/quaternion.hpp"
#include "glm/gtx/rotate_vector.hpp"
#include "glm/gtx/common.hpp"
#include "slikenet/PeerInterface.h"
#include "slikenet/Types.h"
#include "slikenet/MessageIdentifiers.h"
#include "slikenet/BitStream.h"
#include "SOIL/image_helper.h"
#include "Guid.hpp"
#ifdef G_DBG
#include <cassert>
#endif
#ifdef __linux__
#include <unistd.h>
#elif _WIN32
#include <Windows.h>
#include <tchar.h>
#else
#error "OS not supported!"
#endif

#ifdef G_DBG
#define DBG_ASSERT(x) assert(x)
#define DBG_PRINTF(x) std::printf(x)
#endif

