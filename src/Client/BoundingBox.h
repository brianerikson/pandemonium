#pragma once
#include "GeometryCommon.h"

class Mesh;

class BoundingBox {
public:
	//! A bounding box 1 unit in size, centered on 0.5f, 0.5f, 0.5f
	BoundingBox();

	//! A centered bounding box for all vertices in a mesh
	BoundingBox(const std::vector<Vertex> &vertices);
	//! A centered bounding box for all meshes in a model
	BoundingBox(const std::vector<Mesh> &meshes);

	//! { x = Width, y = Height, z = Depth }
	glm::vec3 getSize() const;

	// front face (think facing towards the camera = least z value)
	glm::vec3 fbl;
	glm::vec3 fbr;
	glm::vec3 ftl;
	glm::vec3 ftr;

	// back face (furthest away from the camera)
	glm::vec3 bbl;
	glm::vec3 bbr;
	glm::vec3 btl;
	glm::vec3 btr;

private:
	void create(float minX, float maxX, float minY, float maxY, float minZ, float maxZ);
};