#pragma once
#include "Mesh.h"
#include "NetCommon/GoTypes.h"
#include "BoundingBox.h"

class Model {
public:
	BoundingBox bBox;
	std::vector<Mesh> meshes;
	std::vector<Texture> loaded_textures;
	std::string directory;
	aiMatrix4x4 matrix;

	Model(const char *modelPath, Netcom::GoType type);
    Model(const char *folder, aiNode *root, const aiScene *scene, Netcom::GoType type);
    virtual ~Model();

	Netcom::GoType type;

protected:
	Model() = default;
private:
	void parse(const char *folder, aiNode *root, const aiScene *scene);
    void processNode(aiNode *node, const aiScene *scene);
    Mesh processMesh(aiMesh *mesh, const aiScene *scene);
	std::vector<Texture> loadMaterialTextures(aiMaterial *mat, aiTextureType type, const char *typeName);
};