#pragma once
#include "ShaderProgram.h"
#include "Actor.h"

class GameRenderer : public ShaderProgram {
public:
	GameRenderer();
	virtual void drawLine(const glm::vec3 &from, const glm::vec3 &to, const glm::vec4 &color);
	virtual void drawBoundingBox(const Actor *actor, const glm::vec4 &color);
	virtual void update(const Camera *camera);

	//! Draw a circle of a given radius centered on pos.
	// The less the resolution, the more points in the circle
	virtual void drawCircle(const glm::vec3 &pos, float radius, const glm::vec4 &color);
private:
	static const unsigned int CIRCLE_RES = 100;
	static const unsigned int BUFFER_SIZE = 6 + (CIRCLE_RES * 3);
	// 0-5 line, 6-CIRCLE_RES circle
	GLfloat bufferData[BUFFER_SIZE] = {};
	GLuint vao, vbo, viewLoc, projLoc, colorLoc;
};