#include "stdafx.h"
#include "GameObject.h"
#include "NetCommon/net_common.h"
#include "ManagerGame.h"

GameObject::GameObject(Netcom::GoType t, const SLNet::RakNetGUID &owner, bool isLocal, bool locallyOwned)
	: type(t), owner(owner), isLocal(isLocal), updateNet(true), locallyOwned(locallyOwned),
	  client(ManagerGame::Instance().gameCtx.net) {}

void GameObject::update(const float &deltaTime) {
	if (isLocal) { 
		transform.update();
		return; 
	}
	bool updated = transform.update();
	if (locallyOwned) {
		lastNetUpdateDt += deltaTime;
		if (updated && lastNetUpdateDt > NETWORK_TICKRATE_SEC) {
			Netcom::CPL_GameObjectUpdate update(
				new Netcom::UdtTransform(guid, transform.serialize())
			);
			SLNet::BitStream stream;
			update.serialize(stream);
			delete update.part;
			client->send(&stream, PacketPriority::LOW_PRIORITY, 
				PacketReliability::UNRELIABLE, 0);
		}
	}
}
