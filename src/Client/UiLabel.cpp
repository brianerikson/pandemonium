#include "stdafx.h"
#include "UiLabel.h"
#include "UiRenderer.h"
#include "FontManager.h"

UiLabel::UiLabel(EFont font, int pt, std::string text)
	: UiWidget(LABEL), font(font), pt(pt), text(text) {
	this->pack();
}

UiLabel::UiLabel() : UiLabel(EFont::ROBOTO_REGULAR, 12, "NULL") {}

void UiLabel::draw(UiRenderer *renderer) {
	renderer->drawText(font, pt, pos.x, pos.y, color, text.c_str());
}

UiWidget* UiLabel::delegateFocus() {
	if (this->containsMouse()) {
		return this;
	}

	return nullptr;
}

void UiLabel::resizeToContent() {
	pack();
}

std::string UiLabel::getText() {
	return text;
}

void UiLabel::pack() {
	if (text.empty()) { return; }
	glm::vec2 textSize = FontManager::Instance().findBounds(font, pt, text);
	dimensions.content = { textSize.x, textSize.y };
}

void UiLabel::setText(std::string text) {
	this->text = text;
	this->pack();
}

