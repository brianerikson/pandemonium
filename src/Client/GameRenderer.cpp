#include "stdafx.h"
#include "GameRenderer.h"

using namespace glm;

#define CHECK_RUNNING() if (!running) { return; }

GameRenderer::GameRenderer()
	: ShaderProgram("res/shaders/debug.vert", "res/shaders/debug.frag") {
	viewLoc = glGetUniformLocation(programId, "view");
	projLoc = glGetUniformLocation(programId, "projection");
	colorLoc = glGetUniformLocation(programId, "color");

	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(bufferData), &bufferData, GL_STREAM_DRAW);
	// Vertex Positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindVertexArray(0);

	this->defaultsLoaded = true;
}

void GameRenderer::drawLine(const glm::vec3 &from, const glm::vec3 &to, const glm::vec4 &color) {
	CHECK_RUNNING();
	glUniform4fv(colorLoc, 1, glm::value_ptr(color));
	bufferData[0] = from.x;
	bufferData[1] = from.y;
	bufferData[2] = from.z;
	bufferData[3] = to.x;
	bufferData[4] = to.y;
	bufferData[5] = to.z;

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(bufferData), &bufferData, GL_STREAM_DRAW);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);
}

void GameRenderer::drawBoundingBox(const Actor *actor, const glm::vec4 &color) {
	BoundingBox &bbox = actor->model->bBox;
	auto &m = actor->transform.matrix;
	vec4 bbl = m * vec4{ bbox.bbl.x, bbox.bbl.y, bbox.bbl.z, 1.f };
	vec4 bbr = m * vec4{ bbox.bbr.x, bbox.bbr.y, bbox.bbr.z, 1.f };
	vec4 btr = m * vec4{ bbox.btr.x, bbox.btr.y, bbox.btr.z, 1.f };
	vec4 btl = m * vec4{ bbox.btl.x, bbox.btl.y, bbox.btl.z, 1.f };

	vec4 fbl = m * vec4{ bbox.fbl.x, bbox.fbl.y, bbox.fbl.z, 1.f };
	vec4 fbr = m * vec4{ bbox.fbr.x, bbox.fbr.y, bbox.fbr.z, 1.f };
	vec4 ftr = m * vec4{ bbox.ftr.x, bbox.ftr.y, bbox.ftr.z, 1.f };
	vec4 ftl = m * vec4{ bbox.ftl.x, bbox.ftl.y, bbox.ftl.z, 1.f };

	float llMul = 0.3f;
	// Back face
	drawLine(bbl, bbl - ((bbl - bbr) * llMul), color);
	drawLine(bbl, bbl - ((bbl - btl) * llMul), color);
	drawLine(bbl, bbl - ((bbl - fbl) * llMul), color);

	drawLine(btl, btl - ((btl - bbl) * llMul), color);
	drawLine(btl, btl - ((btl - btr) * llMul), color);
	drawLine(btl, btl - ((btl - ftl) * llMul), color);

	drawLine(btr, btr - ((btr - btl) * llMul), color);
	drawLine(btr, btr - ((btr - bbr) * llMul), color);
	drawLine(btr, btr - ((btr - ftr) * llMul), color);

	drawLine(bbr, bbr - ((bbr - bbl) * llMul), color);
	drawLine(bbr, bbr - ((bbr - btr) * llMul), color);
	drawLine(bbr, bbr - ((bbr - fbr) * llMul), color);

	// Front face
	drawLine(fbl, fbl - ((fbl - fbr) * llMul), color);
	drawLine(fbl, fbl - ((fbl - ftl) * llMul), color);
	drawLine(fbl, fbl - ((fbl - bbl) * llMul), color);

	drawLine(ftl, ftl - ((ftl - fbl) * llMul), color);
	drawLine(ftl, ftl - ((ftl - ftr) * llMul), color);
	drawLine(ftl, ftl - ((ftl - btl) * llMul), color);

	drawLine(ftr, ftr - ((ftr - ftl) * llMul), color);
	drawLine(ftr, ftr - ((ftr - fbr) * llMul), color);
	drawLine(ftr, ftr - ((ftr - btr) * llMul), color);

	drawLine(fbr, fbr - ((fbr - fbl) * llMul), color);
	drawLine(fbr, fbr - ((fbr - ftr) * llMul), color);
	drawLine(fbr, fbr - ((fbr - bbr) * llMul), color);
}

void GameRenderer::update(const Camera *camera) {
	CHECK_RUNNING();
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, value_ptr((glm::mat4)camera->view()));
	glUniformMatrix4fv(projLoc, 1, GL_FALSE, value_ptr(camera->projection));
}

void GameRenderer::drawCircle(const glm::vec3 &pos, float radius, const glm::vec4 &color) {
	CHECK_RUNNING();
	glUniform4fv(colorLoc, 1, glm::value_ptr(color));
	
	float angInc = glm::radians(360.f / CIRCLE_RES);
	float angle = 0;
	for (unsigned int i = 6; i < BUFFER_SIZE; i+=3) {
		bufferData[i] = radius * glm::cos(angle);
		bufferData[i] += pos.x;
		bufferData[i+1] = pos.y;
		bufferData[i+2] = radius * glm::sin(angle);
		bufferData[i+2] += pos.z;
		angle += angInc;
	}

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(bufferData), &bufferData, GL_STREAM_DRAW);
	glDrawArrays(GL_LINE_LOOP, 2, CIRCLE_RES);
	glBindVertexArray(0);
}

#undef CHECK_RUNNING