#include "stdafx.h"
#include "UiTextBox.h"
#include "FontManager.h"

#define ALIGN_LABEL(l) l.setPosition( \
	pos.x + dimensions.padding.left, \
	pos.y + dimensions.padding.top \
)

UiTextBox::UiTextBox(int charAmt, EFont font, int pt, std::string defaultText) : UiWidget(TEXTBOX) {
	this->setWidthToChars(charAmt);
	textLabel.pt = pt;
	textLabel.font = font;
	textLabel.setText(defaultText);
	ALIGN_LABEL(textLabel);
}

UiTextBox::UiTextBox() : UiTextBox(10, EFont::ROBOTO_REGULAR, 16, "") {}

void UiTextBox::draw(UiRenderer *renderer) {
	renderer->drawBox(pos.x, pos.y,
		getInnerWidth(),
		getInnerHeight(),
		color
	);

	textLabel.draw(renderer);
}

void UiTextBox::processInput(std::queue<std::shared_ptr<KeyEvent>> &keyEvents) {
	using Action = KeyEvent::Action;

	std::string buffer = textLabel.getText();
	while (!keyEvents.empty()) {
		auto ke = keyEvents.front();
		switch (ke->action) {
		case Action::BACKSPACE:
			if (!buffer.empty()) { buffer.pop_back(); }
			break;
		case Action::CHAR_ADDED:
		{
			auto cae = std::static_pointer_cast<CharAddedEvent>(ke);
			buffer.append(cae->ch);
			break;
		}
		case Action::ENTER:
			if (onSubmit) { onSubmit(buffer); }
		default:
			break;
		}
		keyEvents.pop();
	}

	textLabel.setText(buffer);
}

UiWidget* UiTextBox::delegateFocus() {
	if (this->containsMouse()) {
		return this;
	}

	return nullptr;
}

void UiTextBox::propagateVisibilty(bool visible) {
	UiWidget::propagateVisibilty(visible);
	textLabel.propagateVisibilty(visible);
}

void UiTextBox::setPosition(float x, float y) {
	UiWidget::setPosition(x, y);
	ALIGN_LABEL(textLabel);
}

void UiTextBox::setText(std::string text) {
	textLabel.setText(text);
}

std::string UiTextBox::getText() {
	return textLabel.getText();
}

void UiTextBox::setWidthToChars(int charAmt) {
	glm::vec2 charSize = FontManager::Instance().findBounds(textLabel.font, textLabel.pt, "A");
	dimensions.content.width = charSize.x * charAmt;
	dimensions.content.height = charSize.y;
	float padding = charSize.y * 0.3f;
	dimensions.padding = { padding, padding, padding, padding };
}

#undef ALIGN_LABEL
