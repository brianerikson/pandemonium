#pragma once
#include "UiWidget.h"
#include "UiLabel.h"

class UiTextBox : public UiWidget {
public:
	UiTextBox(int charAmt, EFont font, int pt, std::string defaultText);
	UiTextBox();

	virtual void draw(UiRenderer *renderer) override;
	virtual void processInput(std::queue<std::shared_ptr<KeyEvent>> &keyEvents) override;
	virtual UiWidget* delegateFocus() override;
	virtual void propagateVisibilty(bool visible) override;
	virtual void setPosition(float x, float y) override;
	
	std::function<void(std::string)> onSubmit = nullptr;

	void setText(std::string text);
	std::string getText();
	void setWidthToChars(int charAmt);

	glm::vec4 color = { 92.0f/255, 107.0f/255, 119.0f/255, 1.0f }; // #5C6B77
private:
	UiLabel textLabel;

};