#pragma once
#include "ShaderProgram.h"
#include "Actor.h"
#include "Light.h"
#include "Camera.h"

class GameShader : public ShaderProgram {
public:
	void virtual update(Light *light, Camera *camera) = 0;
	void virtual render(const Actor *actor) = 0;
	void virtual render(Mesh &mesh);
protected:
	GameShader(const char *vertPath, const char *fragPath);
};