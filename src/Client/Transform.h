#pragma once
#include "MathInterop.h"
#include "NetCommon/Serializable.h"
#include "NetCommon/Transform.h"

class Transform {
public:
	Matrix4x4 matrix;
    Transform();
    Transform(glm::vec3 pos, glm::vec3 scale, glm::mat4 rotation);
	Transform(aiMatrix4x4 matrix);
    virtual ~Transform();
    
    void setPosition(glm::vec3 pos);
    void setScale(glm::vec3 scale);
	void setRotation(const glm::mat4 &rot);
	void setRotation(const glm::quat &rot);
	void setRotation(float radX, float radY, float radZ);
	void setRotation(float angleDeg, glm::vec3 axis);

    void translate(glm::vec3 dist);

	void rotate(glm::quat rot);
	void rotate(float angleDeg, glm::vec3 axis);

	// Rotate transform by radian angles
	void rotate(float radX, float radY, float radZ);

	const glm::vec3& getPosition() const;
	const glm::vec3& getScale() const;
	const glm::quat& getRotation() const;

    void lookAt(Transform &transform);
    bool update();

	glm::vec3 up();
	glm::vec3 forward();
	glm::vec3 right();

	Netcom::Transform serialize();
	void deserialize(Netcom::Transform t);

private:
	glm::vec3 position{0,0,0};
	glm::vec3 scale{1,1,1};
	glm::quat rotation{1,0,0,0};
	bool needsUpdate = true;
};

