#include "stdafx.h"
#include "Actor.h"
#include "ManagerGame.h"

using namespace std;

Actor::Actor(const SLNet::RakNetGUID &owner, bool isLocal, bool locallyOwned, Netcom::GoType t, 
	World *world, ShaderType shaderType)
	: GameObject(t, owner, isLocal, locallyOwned), world(world), model(world->getModel(t)), 
	  shaderType(shaderType), type(t) {}

Actor::~Actor() {
	if (this->physics) {
		delete physics->rigidBody;
	}
	delete physics;
}

void Actor::enablePhysics(const btScalar &mass, const btScalar &force, 
	const float &maxSpeed, const float &turnSpeed, int group, int mask) {

	physics = new Physics{MotionState(transform), nullptr};
	physics->rigidBody = new btRigidBody(mass, &physics->motionState, world->getCollisionShape(type));
	physics->userData = {Netcom::GoType::ACTOR, this};
	physics->group = group;
	physics->mask = mask;
	physics->mass = mass;
	physics->force = force;
	physics->maxSpeed = maxSpeed;
	physics->turnSpeed = turnSpeed;
	physics->rigidBody->setUserPointer(&this->physics->userData);
}

void MotionState::getWorldTransform(btTransform& worldTrans) const {
	auto &m = this->t.matrix;
	const glm::vec3 &pos = t.getPosition();

	worldTrans.setBasis(m);
	worldTrans.setOrigin(btVector3{ pos.x, pos.y, pos.z });
}

void MotionState::setWorldTransform(const btTransform& worldTrans) {
	float pitch; // x
	float yaw; // y
	float roll; // z

	worldTrans.getRotation().getEulerZYX(roll, yaw, pitch);
	const btVector3 &origin = worldTrans.getOrigin();
	t.setRotation(pitch, yaw, roll);
	t.setPosition({ origin.x(), origin.y(), origin.z() });
}