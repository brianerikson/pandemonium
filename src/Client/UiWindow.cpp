#include "stdafx.h"
#include "UiWindow.h"
#include "UiRenderer.h"

#define ALIGN_CONTAINER(c) c->setPosition( \
	pos.x + dimensions.padding.left, \
	pos.y + dimensions.padding.top \
)

using namespace glm;

UiWindow::UiWindow() : UiWidget(WINDOW) {}

void UiWindow::draw(UiRenderer *renderer) {
	renderer->drawBox(pos.x, pos.y, 
		getInnerWidth(),
		getInnerHeight(),
		color
	);
	container->draw(renderer);
}

UiWidget* UiWindow::delegateFocus() {
	if (this->containsMouse()) {
		UiWidget *focusedWidget = container->delegateFocus();
		if (focusedWidget) {
			return focusedWidget;
		}
		return this;
	}

	return nullptr;
}

void UiWindow::resizeToContent() {
	container->resizeToContent();

	// TODO: Adjust this when adding title bar
	dimensions.content = container->dimensions.content;
}

void UiWindow::propagateVisibilty(bool visible) {
	UiWidget::propagateVisibilty(visible);
	container->propagateVisibilty(visible);
}

void UiWindow::setContainer(UiContainer *container) {
	ALIGN_CONTAINER(container);
	container->dimensions.content.width = dimensions.content.width 
						- (dimensions.padding.left + dimensions.padding.right);
	container->dimensions.content.height = dimensions.content.height
						- (dimensions.padding.bottom + dimensions.padding.top);
	this->container = container;
}

void UiWindow::restore() {
	setVisible(true);
}

void UiWindow::minimize() {
	setVisible(false);
}

void UiWindow::setPosition(float x, float y) {
	UiWidget::setPosition(x, y);
	if (container) { ALIGN_CONTAINER(container); }
}

#undef ALIGN_CONTAINER
