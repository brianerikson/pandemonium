#include "stdafx.h"
#include "FPSCamControls.h"

using namespace glm;

FPSCamControls::FPSCamControls(FPSCamera *camera) : InputController() {
    this->camera = camera;

    // init keystate array
    for (int i = 0; i < KEYSTATE_SIZE; i++) {
        keyState[i] = GLFW_RELEASE;
    }
}


FPSCamControls::~FPSCamControls() {
}

void FPSCamControls::inputCallback(int key, int scancode, int action, int mods) {
    if (action == GLFW_PRESS) {
        keyState[key] = GLFW_PRESS;
    }
    else if (action == GLFW_RELEASE) {
        keyState[key] = GLFW_RELEASE;
    }
}

void FPSCamControls::update(double deltaTime) {
    for (int i = 0; i < KEYSTATE_SIZE; i++) {
        if (keyState[i] == GLFW_PRESS) {
            processKeyEvent(deltaTime, i);
        }
    }
}

void FPSCamControls::processKeyEvent(double deltaTime, int key) {
	speedMul = 1;
	if (key == GLFW_KEY_LEFT_SHIFT) {
		speedMul = 2;
	}

    // LATERAL CAMERA MOVEMENT
    vec3 posDelta = vec3(0.0f);
    // FORWARD AXIS
    if (key == GLFW_KEY_W) {
        posDelta += MOVE_SPEED_SEC * speedMul * camera->forward();
    }
    else if (key == GLFW_KEY_S) {
        posDelta -= MOVE_SPEED_SEC * speedMul * camera->forward();
    }

    // SIDE AXIS
    if (key == GLFW_KEY_A) {
        posDelta -= MOVE_SPEED_SEC * speedMul * camera->right();
    }
    else if (key == GLFW_KEY_D) {
        posDelta += MOVE_SPEED_SEC * speedMul * camera->right();
    }

	// UP AXIS
	if (key == GLFW_KEY_R) {
		posDelta += MOVE_SPEED_SEC * speedMul * camera->up();

	}
	else if (key == GLFW_KEY_F) {
		posDelta -= MOVE_SPEED_SEC * speedMul * camera->up();
	}

    posDelta *= deltaTime;
    camera->translate(posDelta.x, posDelta.y, posDelta.z);

    // ROTATIONAL CAMERA MOVEMENT
    float pitch = 0.0f;
    float yaw = 0.0f;
    // YAW
    if (key == GLFW_KEY_Q) {
        yaw += DEGREE_SEC;
    }
    else if (key == GLFW_KEY_E) {
        yaw -= DEGREE_SEC;
    }

    // PITCH
    if (key == GLFW_KEY_T) {
        pitch -= DEGREE_SEC;

    }
    else if (key == GLFW_KEY_G) {
        pitch += DEGREE_SEC;
    }

    if (pitch != 0.0f || yaw != 0.0f) {
        vec3 aps = vec3(pitch, yaw, 0.0f) * (float)deltaTime;
        camera->rotate(aps.x, aps.y, 0.0f);
    }
}
