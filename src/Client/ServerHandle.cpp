#include "stdafx.h"
#include "ServerHandle.h"

using namespace std;

bool ServerHandle::start(StartArgs args) {
	if (isRunning()) { return true; }

#ifdef __linux__
	// TODO: All of it, honestly :/
	pid_t pid;
	fflush(NULL);
	pid = fork();
	if (!pid) {
		execl("./Server.exe", nullptr);
		return true;
	}
#elif _WIN32
	STARTUPINFO startInfo;
	char buf[260]; //  260 is max file path length in Windows

	ZeroMemory(&startInfo, sizeof(startInfo));
	ZeroMemory(&procInfo, sizeof(procInfo));

	GetModuleFileName(nullptr, buf, 260);
	string path = buf;
	path.erase(path.size() - 10, 10);
	stringstream ss;
	ss << path << "Server.exe -p " << args.numPlayers;
	memcpy(buf, ss.str().c_str(), ss.str().size());
	LPTSTR str = _tcsdup(buf);
	if (CreateProcess(nullptr, str, nullptr, nullptr, true, CREATE_NEW_CONSOLE, nullptr,
		nullptr, &startInfo, &procInfo)) {
		running = true;
		return true;
	}
	return false;
#endif
}

void ServerHandle::stop() {
	if (!isRunning()) { return; }

#ifdef __linux__

#elif _WIN32
	TerminateProcess(procInfo.hProcess, 0);
	CloseHandle(procInfo.hProcess);
	CloseHandle(procInfo.hThread);
#endif
	running = false;
}

bool ServerHandle::isRunning() {
	return running;
}
