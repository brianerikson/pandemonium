#include "stdafx.h"
#include "Spaceship.h"
#include "ManagerGame.h"

Spaceship::Spaceship(World *world, const SLNet::RakNetGUID &owner, bool locallyOwned) 
	: Actor(owner, false, locallyOwned, Netcom::GoType::SPACESHIP, world, ShaderType::DEFAULT) {

	enablePhysics(1, 5, 10.f, 4.f, World::ColMask::SHIP, World::ColMask::ALL);
}

Spaceship::Spaceship(World *world, Netcom::Spaceship *ship, bool locallyOwned) 
	: Spaceship(world, ship->owner, locallyOwned) {

	transform.deserialize(ship->transform);
	physics->rigidBody->getWorldTransform().setOrigin((Vector3)ship->transform.position);
	physics->rigidBody->getWorldTransform().setRotation((Quat)ship->transform.rotation);
	guid = ship->guid;
	shield = ship->shield;
	hull = ship->hull;
}

Spaceship::~Spaceship() = default;

void Spaceship::clearMovementActions() {
	for (MovementAction* action : actions) {
		delete action;
	}
	actions.clear();
}

void Spaceship::addMovementAction(MovementAction *action) {
	action->setActor(this);
	actions.push_back(action);
}

const MovementAction* Spaceship::getCurrentAction() const {
	if (actions.empty()) {
		return nullptr;
	}
	return actions.front();
}

void Spaceship::update(const float &deltaTime) {
	Actor::update(deltaTime);
	if (!actions.empty()) {
		physics->rigidBody->setDamping(0, 0);
		if (actions.front()->update(deltaTime)) {
			delete actions.front();
			actions.pop_front();
		}
	}
	else {
		physics->rigidBody->setDamping(1, 1);
	}
}

void MovementAction::setActor(Actor *actor) {
	if (!actor->physics) {
		throw std::runtime_error("Actor doesn't have physics!");
	}

	this->actor = actor;
}

void MovementAction::seek(const Vector3 &pos, const float &deltaTime) {
	btRigidBody *body = actor->physics->rigidBody;
	body->activate(true);
	glm::vec3 curPos = actor->transform.getPosition();
	glm::vec3 targetOffset = pos - curPos;
	glm::vec3 curVelocity = (Vector3) body->getLinearVelocity();
	glm::vec3 desiredVelocity = glm::normalize(targetOffset) * actor->physics->maxSpeed;
	glm::vec3 steering = glm::normalize(desiredVelocity - curVelocity);
	steering = glm::normalize(steering + avoidCollision(steering));
	Vector3 accelRate = steering * (actor->physics->force / actor->physics->mass);

	orientAt(glm::normalize(targetOffset), actor->transform.up(), deltaTime);

	body->applyCentralForce(accelRate);
}

void MovementAction::lookAt(const glm::vec3 &dir) {
	auto temp = actor->transform.up();
	glm::quat quat = glm::quatLookAtLH((glm::vec3)dir, actor->transform.up());
	actor->physics->rigidBody->getWorldTransform()
		.setRotation(btQuaternion(quat.x, quat.y, quat.z, quat.w));
}

glm::vec3 MovementAction::avoidCollision(const Vector3 &dir) {
	btRigidBody *body = actor->physics->rigidBody;
	btVector3 from = body->getCenterOfMassPosition();
	glm::vec3 checkDir = glm::normalize((Vector3)body->getLinearVelocity().normalized() + dir);
	btVector3 to = (Vector3) ((Vector3)from + (checkDir * actor->physics->maxSpeed));
	btCollisionWorld::ClosestRayResultCallback rc = {from, to};
	actor->world->dynamicsWorld.rayTest(from, to, rc);
	if (rc.hasHit()) {
		glm::vec3 normal = glm::normalize((glm::vec3)(Vector3) rc.m_hitNormalWorld);
		if (abs(glm::dot(normal, checkDir)) > 0.5f) {
			return glm::cross(normal, checkDir);
		}
		return normal;
	}
	return {0,0,0};
}

bool MovementAction::orientAt(const glm::vec3 &dir, const glm::vec3 &up, const float &deltaTime) {
	glm::quat curRot = (Quat) actor->physics->rigidBody->getWorldTransform().getRotation();
	glm::quat t = glm::quatLookAtLH(dir, up);
	glm::quat rot = glm::slerp(curRot, t, deltaTime * actor->physics->turnSpeed);
	actor->physics->rigidBody->getWorldTransform().setRotation(btQuaternion(rot.x, rot.y, rot.z, rot.w));

	float matching = glm::dot(rot, t);
	if (abs(matching - 1.f) < 0.001f) {
		return true;
	}

	return false;
}

FollowPathAction::FollowPathAction(const std::list<Vector3> &path)
: MovementAction(MovementAction::FOLLOW_PATH), spine(path), tubeRadius(5.f) {}

bool FollowPathAction::update(const float &deltaTime) {
	if (!actor) { throw std::runtime_error("Must set actor before updating!"); }
	if (spine.empty()) { return true; }
	btRigidBody *body = actor->physics->rigidBody;
	glm::vec3 pos = actor->transform.getPosition();
	glm::vec3 curVelocity = (Vector3) body->getLinearVelocity();
	glm::vec3 futurePos = pos + curVelocity;

	// trim spine to the closest segment
	float lastDist = std::numeric_limits<float>::max();
	float dist = glm::distance(futurePos, spine.front());
	if (dist < tubeRadius && spine.size() == 1) {
		return true;
	}
	else if (dist < tubeRadius) { spine.pop_front(); }

	seek(spine.front(), deltaTime);

	return false;
}

bool GoToAction::update(const float &deltaTime) {
	if (!actor) { throw std::runtime_error("Must set actor before updating!"); }
	btRigidBody *body = actor->physics->rigidBody;
	glm::vec3 curPos = actor->transform.getPosition();
	glm::vec3 targetOffset = pos - curPos;
	float dist = glm::distance(curPos, pos);
	Vector3 desiredVelocity = glm::normalize(targetOffset);

	if (dist < 1.f) {
		return true;
	}
	else if (dist < slowDist) {
		desiredVelocity = desiredVelocity * actor->physics->maxSpeed * (dist / slowDist);
	}
	else {
		desiredVelocity = desiredVelocity * actor->physics->maxSpeed;
	}

	glm::vec3 dir = glm::normalize(targetOffset);
	glm::vec3 curVelocity = (Vector3) body->getLinearVelocity();
	Vector3 accelRate = actor->transform.forward() * actor->physics->force;
	glm::vec3 forceVec = accelRate * actor->physics->mass;
	glm::vec3 steering = glm::normalize(desiredVelocity - curVelocity);
	steering = glm::normalize(steering + avoidCollision(steering));

	orientAt(dir, Direction::UP, deltaTime);
	body->applyCentralForce((Vector3) (steering * actor->physics->force));
	return false;
}

bool StopAndOrientAction::update(const float &deltaTime) {
	if (!actor) { throw std::runtime_error("Must set actor before updating!"); }
	btRigidBody *body = actor->physics->rigidBody;
	body->setDamping(1, 0);
	if (orientAt(orientDir, Direction::UP, deltaTime) && body->getLinearVelocity().length() < 0.1f) {
		body->setDamping(0, 0);
		body->setLinearVelocity({ 0,0,0 });
		body->forceActivationState(WANTS_DEACTIVATION);
		return true;
	}
	return false;
}
