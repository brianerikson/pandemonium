#include "stdafx.h"
#include "World.h"
#include "Actor.h"
#include "Spaceship.h"

World::World(const btVector3 gravity, Camera *camera) : camera(camera),
	dispatcher(&collisionConfig), dynamicsWorld(&dispatcher, &broadphase, &solver, &collisionConfig) {
	dynamicsWorld.setGravity(gravity);
	planeData.type = Netcom::GoType::XZ_PLANE;
	plane.setUserPointer(&planeData);
	dynamicsWorld.addRigidBody(&plane, (int)ColMask::PLANE, (int)ColMask::RAY);

#ifdef G_PHYS_DBG
	dynamicsWorld.setDebugDrawer(&debugRenderer);
#endif
}

World::~World() {
	for (auto actor : actors) { delete actor; }
	for (auto model : models) { delete model.second; }
	for (auto shape : shapes) { delete shape.second; }
}

void World::stepSimulation(float deltaTime) {
	dynamicsWorld.stepSimulation(deltaTime, 7);
}

void World::update(const float &deltaTime) {
	for (auto &actor : actors) {
		actor->update(deltaTime);
	}
}

void World::addActor(Actor *actor) {
	actors.push_back(actor);
	actorMap[actor->guid] = actor;
	auto &physics = actor->physics;
	if (physics) {
		dynamicsWorld.addRigidBody(physics->rigidBody, physics->group, physics->mask);
		dynamicsWorld.synchronizeSingleMotionState(physics->rigidBody);
	}
}

void World::deleteActor(Actor *actor) {
	actors.erase(std::remove(actors.begin(), actors.end(), actor), actors.end());
	actorMap.erase(actor->guid);
	if (actor->physics) { 
		dynamicsWorld.removeRigidBody(actor->physics->rigidBody); 
	}
	delete actor;
}

Actor* World::getActor(const xg::Guid &guid) {
	auto find = actorMap.find(guid);
	if (find != actorMap.end()) { return find->second; }

	return nullptr;
}

const std::vector<Actor*>& World::getActors() const {
	return actors;
}

Camera* World::getCamera() const {
	return camera;
}

#ifdef G_PHYS_DBG
void World::debugDraw() {
	debugRenderer.start();
	debugRenderer.update(camera);
	dynamicsWorld.debugDrawWorld();
	debugRenderer.drawRays();
	debugRenderer.end();
}
#endif

Model* World::getModel(Netcom::GoType t) {
	auto find = models.find(t);
	if (find != models.end()) { return find->second; }

	return loadModel(t);
}

btCollisionShape* World::getCollisionShape(Netcom::GoType t) {
	glm::vec3 size = getModel(t)->bBox.getSize();
	switch (t) {
	case Netcom::GoType::PLANET:
		if (!planetShape) {
			planetShape = new btSphereShape(size.x / 2.f);
		}
		return planetShape;
		break;
	case Netcom::GoType::SPACESHIP:
	{
		if (!spaceshipShape) {
			spaceshipShape = new btBoxShape({ size.x / 2.f, size.y / 2.f, size.z / 2.f });
		}
		return spaceshipShape;
		break;
	}
	default:
		throw std::runtime_error("Shape not implemented for requested type!");
		break;
	}
}

void World::setSelectedActors(std::vector<Actor*> &actors) {
	selectedActors = { actors };
}

const std::vector<Actor*>& World::getSelectedActors() const {
	return selectedActors;
}

Model* World::loadModel(Netcom::GoType t) {
	Model *model = nullptr;
	switch (t) {
	case Netcom::GoType::PLANET:
		model = new Model("res/models/default_planet/default_planet.dae", t);
		break;
	case Netcom::GoType::SPACESHIP:
		model = new Model("res/models/spaceship/spaceship.dae", t);
		break;
	case Netcom::GoType::SKYBOX:
		model = new Model("res/models/skybox/skybox.dae", t);
		break;
	}

	if (model != nullptr) {
		models[t] = model;
		return model;
	}
	else {
		throw std::runtime_error("Could not load requested model");
	}
}

// ===========================
// START DebugPhysWorld
#define CHECK_RUNNING() if (!running) { return; }

DebugRenderer::DebugRenderer() 
	: ShaderProgram("res/shaders/debug.vert", "res/shaders/debug.frag") {
	viewLoc = glGetUniformLocation(programId, "view");
	projLoc = glGetUniformLocation(programId, "projection");
	colorLoc = glGetUniformLocation(programId, "color");
	
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(bufferData), &bufferData, GL_STREAM_DRAW);
	// Vertex Positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindVertexArray(0);

	this->defaultsLoaded = true;
}

void DebugRenderer::update(Camera *camera) {
	CHECK_RUNNING();
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, value_ptr((glm::mat4)camera->view()));
	glUniformMatrix4fv(projLoc, 1, GL_FALSE, value_ptr(camera->projection));
}

void DebugRenderer::drawLine(const btVector3& from, const btVector3& to, const btVector3& color) {
	CHECK_RUNNING();
	glUniform4fv(colorLoc, 1, glm::value_ptr(glm::vec4(color.x(), color.y(), color.z(), 1.f)));
	glLineWidth(10.f);
	bufferData[0] = from.x();
	bufferData[1] = from.y();
	bufferData[2] = from.z();
	bufferData[3] = to.x();
	bufferData[4] = to.y();
	bufferData[5] = to.z();

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(bufferData), &bufferData, GL_STREAM_DRAW);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);
}

void DebugRenderer::drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color) {}

void DebugRenderer::reportErrorWarning(const char* warningString) {
	std::printf(warningString);
}

void DebugRenderer::draw3dText(const btVector3& location, const char* textString) {}

void DebugRenderer::setDebugMode(int debugMode) {
	std::printf("Debug mode set attempt to %i\n", debugMode);
}

int DebugRenderer::getDebugMode() const {
	return DebugDrawModes::DBG_MAX_DEBUG_DRAW_MODE;
}

void DebugRenderer::drawRays() {
	for (Ray &ray : rays) {
		drawLine(ray.from, ray.to, { 0.f, (float)ray.life / ray.lifetime, 0.f });
		ray.life--;
	}

	rays.erase(std::remove_if(rays.begin(), rays.end(), [](Ray ray) {
		return ray.life < 0;
	}), rays.end());
}

void DebugRenderer::addRay(btVector3 from, btVector3 to, int lifetime) {
	rays.push_back({ from, to, lifetime, lifetime });
}

void DebugRenderer::addWireBox(const btVector3 &ftl, const btVector3 &ftr, const btVector3 &fbr, const btVector3 &fbl, float depth, int lifetime) {
	btVector3 btl = ftl; btl.setZ(depth + btl.z());
	btVector3 btr = ftr; btr.setZ(depth + btr.z());
	btVector3 bbr = fbr; bbr.setZ(depth + bbr.z());
	btVector3 bbl = fbl; bbl.setZ(depth + bbl.z());

	addWireBox(lifetime, ftl, ftr, fbr, fbl, btl, btr, bbr, bbl);
}

void DebugRenderer::addWireBox(int lifetime, const btVector3 &ftl, const btVector3 &ftr, const btVector3 &fbr, 
	const btVector3 &fbl, const btVector3 &btl, const btVector3 &btr, const btVector3 &bbr, const btVector3 &bbl) {

	// front face
	rays.push_back({ ftl, ftr, lifetime, lifetime });
	rays.push_back({ ftr, fbr, lifetime, lifetime });
	rays.push_back({ fbr, fbl, lifetime, lifetime });
	rays.push_back({ fbl, ftl, lifetime, lifetime });

	// back face
	rays.push_back({ btl, btr, lifetime, lifetime });
	rays.push_back({ btr, bbr, lifetime, lifetime });
	rays.push_back({ bbr, bbl, lifetime, lifetime });
	rays.push_back({ bbl, btl, lifetime, lifetime });

	// connectors
	rays.push_back({ ftl, btl, lifetime, lifetime });
	rays.push_back({ ftr, btr, lifetime, lifetime });
	rays.push_back({ fbr, bbr, lifetime, lifetime });
	rays.push_back({ fbl, bbl, lifetime, lifetime });
}

#undef CHECK_RUNNING