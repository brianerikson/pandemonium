#include "stdafx.h"
#include "SceneMainMenu.h"
#include "NetCommon/net_common.h"

using namespace std;

void launchServer() {
}

SceneMainMenu::SceneMainMenu(GContext &gameCtx) : Scene(gameCtx, MAIN_MENU) {
	// GUI
	glm::vec2 screenSize = uiRoot->getScreenSize();

	nameWindow.setCenter(screenSize.x / 2, screenSize.y / 2);
	nameWindow.nameTextBox.setText("Mr. Generic");
	nameWindow.nameTextBox.onSubmit = [=](std::string str) {
		if (!str.empty()) {
			nameWindow.minimize();
			choiceWindow.restore();
		}
	};
	nameWindow.okButton.onPress = [=]() { 
		nameWindow.nameTextBox.onSubmit(nameWindow.nameTextBox.getText()); 
	};

	choiceWindow.setCenter(screenSize.x / 2, screenSize.y / 2);
	choiceWindow.joinButton.onPress = [=]() {
		choiceWindow.minimize();
		joinWindow.restore();
	};
	choiceWindow.hostButton.onPress = [=]() {
		choiceWindow.minimize();
		playerNumWindow.restore();
	};

	auto connectFunc = [=](std::string str) {
		cout << "Submitted:" << endl;
		cout << "Username: " << nameWindow.nameTextBox.getText() << endl;
		cout << "Server: " << joinWindow.serverTextBox.getText() << endl;
		client->requestConnect(joinWindow.serverTextBox.getText().c_str(), 
			SERVER_UDP_PORT, nameWindow.nameTextBox.getText());
	};

	joinWindow.setCenter(screenSize.x / 2, screenSize.y / 2);
	joinWindow.serverTextBox.onSubmit = connectFunc;
	joinWindow.connectButton.onPress = [=]() { 
		connectFunc(joinWindow.serverTextBox.getText());
	};

	playerNumWindow.setCenter(screenSize.x / 2, screenSize.y / 2);
	// playerNumWindow.hostButton/textbox.onPress impl in ManagerGame()

	nameWindow.restore();
	choiceWindow.minimize();
	playerNumWindow.minimize();
	joinWindow.minimize();

	uiRoot->addWidget(&nameWindow);
	uiRoot->addWidget(&choiceWindow);
	uiRoot->addWidget(&joinWindow);
	uiRoot->addWidget(&playerNumWindow);
}

SceneMainMenu::~SceneMainMenu() = default;

void SceneMainMenu::update(float deltaTime) {
	uiRoot->processInput();
	uiRoot->update(deltaTime);
}

void SceneMainMenu::render() {
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	uiRoot->draw();
}

void SceneMainMenu::keyCallback(int key, int scancode, int action, int mods) {

}

void SceneMainMenu::mbCallback(int button, int action, int mods) {

}

void SceneMainMenu::mPosCallback(double x, double y) {

}

void SceneMainMenu::processInputEvents(double deltaTime) {

}

std::string SceneMainMenu::getPlayerName() {
	return nameWindow.nameTextBox.getText();
}

NameWindow::NameWindow() {
	dimensions.padding.left = 5.0f;
	dimensions.padding.top = 5.0f;
	dimensions.padding.right = 5.0f;
	dimensions.padding.bottom = 5.0f;
	setContainer(&hbox);

	nameLabel.pt = 16;
	nameLabel.setText("Player Name:");
	container->addChild(&nameLabel);

	nameTextBox.setText("");
	nameTextBox.setWidthToChars(MAX_PLAYER_NAME_LEN);
	container->addChild(&nameTextBox);

	okButton.setText("OK");
	container->addChild(&okButton);

	resizeToContent();
}

JoinWindow::JoinWindow() {
	dimensions.padding.left = 5.0f;
	dimensions.padding.top = 5.0f;
	dimensions.padding.right = 5.0f;
	dimensions.padding.bottom = 5.0f;
	setContainer(&hbox);

	serverLabel.pt = 16;
	serverLabel.setText("Server IP Address:");
	container->addChild(&serverLabel);

	serverTextBox.setText("127.0.0.1");
	serverTextBox.setWidthToChars(15);
	container->addChild(&serverTextBox);

	connectButton.setText("Connect");
	container->addChild(&connectButton);

	resizeToContent();
}

ChoiceWindow::ChoiceWindow() {
	dimensions.padding.left = 5.0f;
	dimensions.padding.top = 5.0f;
	dimensions.padding.right = 5.0f;
	dimensions.padding.bottom = 5.0f;
	setContainer(&hbox);

	gameLabel.pt = 20;
	gameLabel.setText("Pandemonium");
	container->addChild(&gameLabel);

	joinButton.setText("Join");
	container->addChild(&joinButton);

	hostButton.setText("Host");
	container->addChild(&hostButton);

	resizeToContent();
}

PlayerNumWindow::PlayerNumWindow() {
	dimensions.padding.left = 5.0f;
	dimensions.padding.top = 5.0f;
	dimensions.padding.right = 5.0f;
	dimensions.padding.bottom = 5.0f;
	setContainer(&hbox);

	playerNumLabel.pt = 16;
	playerNumLabel.setText("Number of Players:");
	container->addChild(&playerNumLabel);

	numTextBox.setText("1");
	container->addChild(&numTextBox);

	okButton.setText("OK");
	container->addChild(&okButton);

	resizeToContent();
}
