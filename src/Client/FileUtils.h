#pragma once

namespace FileUtils {
    std::unique_ptr<std::string> readResource(const char *path);
    GLuint textureFromFile(const char *path);
}
