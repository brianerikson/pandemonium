#pragma once

struct Vertex {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 texCoords;
};

struct Texture {
    GLuint id;
    std::string type;
    aiString path;
};

namespace Direction {
    static const glm::vec3 RIGHT   (1.0f, 0.0f, 0.0f);
    static const glm::vec3 FORWARD (0.0f, 0.0f, 1.0f);
    static const glm::vec3 UP      (0.0f, 1.0f, 0.0f);
};