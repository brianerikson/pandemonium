#include "stdafx.h"
#include "ManagerGame.h"

#define SCREEN_WIDTH 1920
#define SCREEN_HEIGHT 1080
#define GLM_ENABLE_EXPERIMENTAL 1

// Preprocessor Definitions
// TEST_LAUNCH: Skip all the menus and get right into the game for testing
// G_DBG: General debugging information
// G_PHYS_DBG: Show physics interations output and render physics wireframe

using namespace std;

GLFWwindow *window;
ManagerGame *gameManager;


void onGLFWError(int error, const char* description) {
	throw runtime_error(description);
}

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods) {
	gameManager->keyCallback(key, scancode, action, mods);
}


// UTF-32
void char_callback(GLFWwindow* window, unsigned int codepoint) {
	gameManager->charCallback(codepoint);
}

void mb_callback(GLFWwindow *window, int button, int action, int mods) {
	gameManager->mbCallback(button, action, mods);
}

void mpos_callback(GLFWwindow *window, double x, double y) {
	gameManager->mPosCallback(x, y);
}

void onShutdown(int signum) {
	if (signum == SIGTERM || signum == SIGBREAK) {
		// do something
		exit(0);
	}
}

int main() {
	aiLogStream stream;

#ifdef _WIN32
	CoInitializeEx(nullptr, COINIT_APARTMENTTHREADED);
#endif

    // INIT
    glfwSetErrorCallback(onGLFWError);
    if (!glfwInit()) {
        throw runtime_error("glfwInit Failed.");
    }

    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Pandemonium", NULL, NULL);
    if (!window) {
        throw runtime_error("glfwCreateWindow Failed. Is your hardware OpenGL 3.3 compatible?");
    }
    glfwMakeContextCurrent(window);

    if (glewInit() != GLEW_OK) {
        throw runtime_error("glewInit Failed.");
    }
    if (!GLEW_VERSION_3_3) {
        throw runtime_error("OpenGL 3.3 API is not available.");
    }

	glfwSwapInterval(true);

	gameManager = &ManagerGame::Instance();
	gameManager->initialize(SCREEN_WIDTH, SCREEN_HEIGHT);

#ifdef G_DBG
	// redirect assimp output to STDOUT
	stream = aiGetPredefinedLogStream(aiDefaultLogStream_STDOUT, NULL);
	aiAttachLogStream(&stream);
#endif

	glfwSetKeyCallback(window, key_callback);
	glfwSetMouseButtonCallback(window, mb_callback);
	glfwSetCursorPosCallback(window, mpos_callback);
	glfwSetCharCallback(window, char_callback);

	double lastTime = glfwGetTime();
	while (!glfwWindowShouldClose(window)) {
		double thisTime = glfwGetTime();
		double deltaTime = thisTime - lastTime;
		lastTime = thisTime;

		// INPUT
		glfwPollEvents();

		gameManager->process((float)deltaTime);

		glfwSwapBuffers(window);
	}


#ifdef _WIN32
	CoUninitialize();
#endif
    return 0;
}