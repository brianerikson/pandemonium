#include "stdafx.h"
#include "UiRoot.h"
#include "UiWindow.h"
#include "UiLabel.h"

#define ASSERT_FOCUS(focus) if (focus == nullptr) { return; }

using namespace std;

UiRoot::UiRoot(unsigned int screenWidth, unsigned int screenHeight) 
	: screenWidth(screenWidth), screenHeight(screenHeight),
	  uiRenderer(UiRenderer::Instance()) {}

UiRoot::~UiRoot() {
	this->clear();
	fWidget = nullptr;
}

void UiRoot::clear() {
	fWidget = nullptr;
	widgets.clear();
}

void UiRoot::addWidget(UiWidget *widget) {
	widgets.push_back(widget);
}

void UiRoot::rawKeyInput(int key, int scancode, int action, int mods) {
	ASSERT_FOCUS(fWidget);

	if (action == GLFW_PRESS || action == GLFW_REPEAT) {
		switch (key) {
		case GLFW_KEY_ENTER:
			keyEvents.push(make_shared<EnterEvent>(EnterEvent()));
			break;
		case GLFW_KEY_BACKSPACE:
			keyEvents.push(make_shared<BackspaceEvent>(BackspaceEvent()));
			break;
		default:
			break;
		}
	}
}

void UiRoot::charInput(unsigned int codepoint) {
	ASSERT_FOCUS(fWidget);
	keyEvents.push(
		make_shared<CharAddedEvent>(
			CharAddedEvent(charConverter.to_bytes(codepoint))
		)
	);
}

void UiRoot::mbInput(int button, int action, int mods) {
	using Trigger = MouseEvent::Trigger;
	if (action == GLFW_PRESS) {
		switch (button) {
		case GLFW_MOUSE_BUTTON_LEFT:
			lmbState = PRESSED;
			keyEvents.push(make_shared<MouseEvent>(MouseEvent(Trigger::LMB_PRESSED)));
			break;
		case GLFW_MOUSE_BUTTON_MIDDLE:
			keyEvents.push(make_shared<MouseEvent>(MouseEvent(Trigger::MMB_PRESSED)));
			break;
		case GLFW_MOUSE_BUTTON_RIGHT:
			keyEvents.push(make_shared<MouseEvent>(MouseEvent(Trigger::RMB_PRESSED)));
			break;
		default:
			break;
		}
	}
	else if (action == GLFW_RELEASE) {
		switch (button) {
		case GLFW_MOUSE_BUTTON_LEFT:
			lmbState = RELEASED;
			keyEvents.push(make_shared<MouseEvent>(MouseEvent(Trigger::LMB_RELEASED)));
			break;
		case GLFW_MOUSE_BUTTON_MIDDLE:
			keyEvents.push(make_shared<MouseEvent>(MouseEvent(Trigger::MMB_RELEASED)));
			break;
		case GLFW_MOUSE_BUTTON_RIGHT:
			keyEvents.push(make_shared<MouseEvent>(MouseEvent(Trigger::RMB_RELEASED)));
			break;
		default:
			break;
		}
	}
}

extern GLFWwindow *window;
void UiRoot::processInput(const UiInputHandler &inputHandler) {
	// process input may consume lmbState
	processInput();

	switch (lmbState) {
	case UiRoot::INACTIVE:
		break;
	case UiRoot::PRESSED:
	{
		if (dragging) { break; }

		// consume the click action if return true
		if (inputHandler.onMousePress()) {
			lmbState = INACTIVE;
			break;
		}

		// If not, drag a rectangle until lmb released
		dragStart = getMousePos();
		dragging = true;
		inputHandler.onMouseDragStart();
		break;
	}
	case UiRoot::RELEASED:
	{
		// TODO: onMouseRelease never called because drag is assumed on 
		// press if not consumed
		if (!dragging) {
			inputHandler.onMouseRelease();
			break;
		}

		dragEnd = getMousePos();
		dragging = false;
		updateDragBox();
		inputHandler.onMouseDragEnd();
		break;
	}
	default:
		break;
	}
}

void UiRoot::processInput() {
	switch (lmbState) {
	case UiRoot::PRESSED:
	{
		UiWidget * last = fWidget;
		if (last) { last->focused = false; }
		for (UiWidget *widget : widgets) {
			if (!widget->isVisibile()) { continue; }

			fWidget = widget->delegateFocus();
			if (fWidget) {
				fWidget->focused = true;
				// Clear buffer to allow for clean input to new focus
				if (fWidget != last) { keyEvents = {}; }
				lmbState = INACTIVE;
				break;
			}
		}
		break;
	}
	case UiRoot::RELEASED:
		break;
	default:
		break;
	}

	if (fWidget != nullptr) {
		fWidget->processInput(keyEvents);
	}
}

void UiRoot::update(float deltaTime) {
	if (fWidget && fWidget->isVisibile() == false) {
		fWidget = nullptr; 
	}

	for (UiWidget *widget : widgets) {
		widget->update(deltaTime);
	}
}

void UiRoot::draw() {
	uiRenderer.start();

	if (dragging) {
		dragEnd = getMousePos();
		updateDragBox();
		uiRenderer.drawLine(dragBox.tl, dragBox.tr, 5.f, focusColor);
		uiRenderer.drawLine(dragBox.tr, dragBox.br, 5.f, focusColor);
		uiRenderer.drawLine(dragBox.br, dragBox.bl, 5.f, focusColor);
		uiRenderer.drawLine(dragBox.bl, dragBox.tl, 5.f, focusColor);
	}

	for (UiWidget *widget : widgets) {
		if (!widget->isVisibile()) { continue; }
		widget->draw(&uiRenderer);
	}

	if (fWidget != nullptr) {
		glm::vec2 pos = fWidget->getPosition();
		uiRenderer.drawBorder(pos.x, pos.y,
			fWidget->getInnerWidth(),
			fWidget->getInnerHeight(),
			3.0f,
			focusColor
		);
	}

	uiRenderer.end();
}

glm::vec2 UiRoot::getMousePos() {
	double xp, yp;
	glfwGetCursorPos(window, &xp, &yp);
	return { (float)abs(xp), (float)abs(yp) };
}

glm::vec2 UiRoot::getScreenSize() {
	return { screenWidth, screenHeight };
}

UiRoot::DragBox UiRoot::getDragBox() const {
	return dragBox;
}

void UiRoot::updateDragBox() {
	float xdiff = dragStart.x - dragEnd.x;
	float yDiff = dragStart.y - dragEnd.y;
	glm::vec2 tl;
	glm::vec2 tr;
	glm::vec2 bl;
	glm::vec2 br;

	// line going to the left
	if (dragStart.x > dragEnd.x) {
		// line going up
		if (dragStart.y > dragEnd.y) {
			tl = dragEnd;
			tr = { dragStart.x, dragEnd.y };
			bl = { dragEnd.x, dragStart.y };
			br = dragStart;
		}
		// line going down
		else {
			tl = { dragEnd.x, dragStart.y };
			tr = dragStart;
			bl = dragEnd;
			br = { dragStart.x, dragEnd.y };
		}
	}
	// line going to the right
	else {
		// up
		if (dragStart.y > dragEnd.y) {
			tl = { dragStart.x, dragEnd.y };
			tr = dragEnd;
			bl = dragStart;
			br = { dragEnd.x, dragStart.y };
		}
		// down
		else {
			tl = dragStart;
			tr = { dragEnd.x, dragStart.y };
			bl = { dragStart.x, dragEnd.y };
			br = dragEnd;
		}
	}

	dragBox.tl = tl;
	dragBox.tr = tr;
	dragBox.bl = bl;
	dragBox.br = br;
}

#undef ASSERT_FOCUS