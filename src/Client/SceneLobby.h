#pragma once
#include "Scene.h"
#include "UiWindow.h"
#include "UiContainer.h"
#include "UiLabel.h"
#include "UiButton.h"

class SceneLobby : public Scene {
public:
	SceneLobby(unsigned int maxPlayers, std::vector<Netcom::SPL_LobbyState::Player> &players, GContext &gameCtx);
	virtual ~SceneLobby();

	void updateLobby(unsigned int maxPlayers, std::vector<Netcom::SPL_LobbyState::Player> &players);
	void enableAdminControls();

	virtual void update(float deltaTime) override;
	virtual void render() override;
	virtual void keyCallback(int key, int scancode, int action, int mods) override;
	virtual void mbCallback(int button, int action, int mods) override;
	virtual void mPosCallback(double x, double y) override;
	virtual void processInputEvents(double deltaTime) override;


	glm::vec4 readyTextColor = { 1.0f, 1.0f, 1.0f, 1.0f }; // #FFFFFF
	glm::vec4 textColor = { 157.0f/255, 161.0f/255, 165.0f/255, 1.0f }; // #9DA1A5
private:
	UiWindow window;
	HBox hbox;
	std::vector<UiLabel*> playerLabels;
	UiButton readyButton;
	UiLabel *lpLabel;
	bool isReady = false;

	// ADMIN CONTROLS
	UiButton launchButton;
};
