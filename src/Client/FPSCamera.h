#pragma once
#include "Camera.h"
#include "InputController.h"
#include "MathInterop.h"

class FPSCamera : public Camera {
public:
    FPSCamera();
    FPSCamera(aiCamera *camera, aiMatrix4x4 &transform);
    virtual ~FPSCamera();
	glm::vec3 position;

    void update() override;

    void translate(float x, float y, float z) override;
	void setPosition(float x, float y, float z) override;
	Vector3 virtual unproject(const glm::vec2 &screenPos, bool farPlane) const override;

	Vector3 getPosition() override;

    void rotate(float pitch, float yaw, float roll) override;
    void virtual rotate(float angle, glm::vec3 dir);
    void setRotation(float pitch, float yaw, float roll) override;
	void virtual setRotation(float angle, glm::vec3 dir);
	glm::mat4 virtual getOrientation();
	Vector3 getRotation() override;

	glm::vec3 virtual forward();
	glm::vec3 virtual up();
	glm::vec3 virtual right();
	Matrix4x4 view() const override;

private:
	glm::mat4 transform = glm::mat4(1.0f);
    bool dirty = true;
	glm::mat4 rotTransform = glm::mat4(1.0f);
	glm::mat4 orientation = glm::mat4(1.0f);
	glm::mat4 lhOrient = glm::mat4(1.0f);
    float pitch = 0.0f;
    float yaw = 0.0f;
};

