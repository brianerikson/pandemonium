#pragma once

class InputController {
public:
    InputController();
    virtual ~InputController();

    void virtual inputCallback(int key, int scancode, int action, int mods) = 0;
    void virtual update(double deltaTime) = 0;
};

