#pragma once

enum EFont {
	ROBOTO_REGULAR = 0,
	ROBOTO_BOLD,
	ROBOTO_LIGHT
};

const std::unordered_map<EFont, const char*> FONT_PATHS = {
	{ ROBOTO_REGULAR, "res/fonts/Roboto-Regular.ttf" },
	{ ROBOTO_BOLD, "res/fonts/Roboto-Bold.ttf" },
	{ ROBOTO_LIGHT, "res/fonts/Roboto-Light.ttf" }
};

class Font {
public:
	EFont fontType;
	int pt; // the size of the font
	GLuint texture; // the assigned texture slot in OpenGL
	FT_Face face; // the assigned face slot in FreeType

	Font(EFont fontType, int pt, GLuint texture, FT_Face face);
	~Font();
};