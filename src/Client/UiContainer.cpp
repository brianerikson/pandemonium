#include "stdafx.h"
#include "UiContainer.h"

void UiContainer::draw(UiRenderer *renderer) {}

UiContainer::UiContainer(LayoutType type) : UiWidget(CONTAINER), type(type) {}

// START HBox

HBox::HBox(std::initializer_list<UiWidget*> widgets) : UiContainer(HBOX) {
	children.insert(children.end(), widgets.begin(), widgets.end());
	this->pack();
}

HBox::HBox() : UiContainer(HBOX) {

}

HBox::~HBox() {
	this->clear();
}

bool HBox::addChild(UiWidget *child) {
	children.push_back(child);
	isValid = false;
	return true;
}

void HBox::pack() {
	float yOffset = 0;
	for (UiWidget *widget : children) {
		yOffset += widget->dimensions.margin.top;
		widget->setPosition(
			dimensions.padding.left + pos.x,
			dimensions.padding.top + pos.y + yOffset
		);
		yOffset += widget->dimensions.margin.bottom +
			widget->dimensions.content.height +
			widget->dimensions.padding.top +
			widget->dimensions.padding.bottom;
	}

	isValid = true;
}

void HBox::clear() {
	children.clear();
}

void HBox::draw(UiRenderer *renderer) {
	if (!isValid) { this->pack(); }

	for (UiWidget *widget : children) {
		if (!widget->isVisibile()) { continue; }
		widget->draw(renderer);
	}
}

UiWidget* HBox::delegateFocus() {
	if (this->containsMouse()) {
		for (UiWidget *widget : children) {
			if (!widget->isVisibile()) { continue; }

			UiWidget *focused = widget->delegateFocus();
			if (focused) {
				return focused;
			}
		}

		return this;
	}

	return nullptr;
}

void HBox::setPosition(float x, float y) {
	UiWidget::setPosition(x, y);
	pack();
}

void HBox::resizeToContent() {
	pack();

	float maxWidth = 0;
	float contentHeight = 0;
	for (UiWidget *widget : children) {
		widget->resizeToContent();

		contentHeight += widget->getHeight();
		float width = widget->getWidth();
		if (width > maxWidth) { maxWidth = width; }
	}

	dimensions.content.width = maxWidth;
	dimensions.content.height = contentHeight;
}

void HBox::propagateVisibilty(bool visible) {
	UiWidget::propagateVisibilty(visible);
	for (UiWidget *widget : children) {
		widget->propagateVisibilty(visible);
	}
}
