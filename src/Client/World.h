#pragma once
#include "Model.h"
#include "Camera.h"
#include "ShaderProgram.h"

class Actor;

struct PhysUserData {
	Netcom::GoType type;
	Actor *actor;
};

class DebugRenderer : public btIDebugDraw, public ShaderProgram {
public:
	struct Ray {
		btVector3 from;
		btVector3 to;

		//! How many frames should the ray be rendered for
		int lifetime;

		//! How many frames the ray has left
		int life;
	};

	DebugRenderer();
	void update(Camera *camera);
	virtual void drawLine(const btVector3& from, const btVector3& to, const btVector3& color) override;
	virtual void drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color) override;
	virtual void reportErrorWarning(const char* warningString) override;
	virtual void draw3dText(const btVector3& location, const char* textString) override;
	virtual void setDebugMode(int debugMode) override;
	virtual int getDebugMode() const override;

	void drawRays();
	void addRay(btVector3 from, btVector3 to, int lifetime);
	void addWireBox(const btVector3 &ftl, const btVector3 &ftr, const btVector3 &fbr, const btVector3 &fbl, float depth, int lifetime);
	void addWireBox(int lifetime,
		const btVector3 &ftl, const btVector3 &ftr, const btVector3 &fbr, const btVector3 &fbl,
		const btVector3 &btl, const btVector3 &btr, const btVector3 &bbr, const btVector3 &bbl
	);
	std::vector<Ray> rays;
private:
	GLfloat bufferData[6] = {};
	GLuint vao, vbo, viewLoc, projLoc, colorLoc;
};

class World {
public:
#define BIT(x) (1<<(x))
	enum ColMask : short {
		NONE = 0,
		PLANE = BIT(0),
		RAY = BIT(1),
		SHIP = BIT(2),
		PLANET = BIT(3),
		ALL = 0xFF
	};
#undef BIT

	World(const btVector3 gravity, Camera *camera);
	~World();

	void stepSimulation(float deltaTime);
	void update(const float &deltaTime);


	void addActor(Actor *actor);
	void deleteActor(Actor *actor);
	Actor* getActor(const xg::Guid &guid);
	const std::vector<Actor*>& getActors() const;
	Camera* getCamera() const;

	Model* loadModel(Netcom::GoType t);
	Model* getModel(Netcom::GoType t);
	btCollisionShape* getCollisionShape(Netcom::GoType t);

	// Will swap with empty array
	void setSelectedActors(std::vector<Actor*> &actors);
	const std::vector<Actor*>& getSelectedActors() const;

	btDiscreteDynamicsWorld dynamicsWorld;

#ifdef G_PHYS_DBG
	void debugDraw();
	DebugRenderer debugRenderer;
#endif
private:
	Camera *camera;
	std::unordered_map<Netcom::GoType, Model*> models;
	std::unordered_map<Netcom::GoType, btCollisionShape*> shapes;

	//! synchronized version of actors for faster access to a specific element
	std::unordered_map<xg::Guid, Actor*> actorMap;

	//! main actor array, optimized for iteration
	std::vector<Actor*> actors;

	//! Sublist of actors
	std::vector<Actor*> selectedActors;

	// Collision Shapes
	btBoxShape* spaceshipShape = nullptr;
	btSphereShape* planetShape = nullptr;

	btDbvtBroadphase broadphase;
	btDefaultCollisionConfiguration collisionConfig;
	btCollisionDispatcher dispatcher;
	btSequentialImpulseConstraintSolver solver;


	btStaticPlaneShape planeShape = btStaticPlaneShape(btVector3(0, 1, 0), 0);
	btDefaultMotionState planeMs = btDefaultMotionState();
	btRigidBody plane = btRigidBody(0, &planeMs, &planeShape);
	PhysUserData planeData;
};