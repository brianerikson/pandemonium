#pragma once
#include "GeometryCommon.h"

class Material {
public:
	Material();
	~Material();

	Texture diffuse;
	Texture specular;
	glm::vec3 diffuse_color;
	glm::vec3 specular_color;
	int enableLighting;
};