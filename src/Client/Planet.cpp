#include "stdafx.h"
#include "Planet.h"
#include "ManagerGame.h"

using namespace std;
using namespace glm;

Planet::Planet(World *world, Netcom::Planet *planet)
	: Actor(planet->owner, false, false, Netcom::GoType::PLANET, world, ShaderType::DEFAULT), 
	  worth(planet->worth) {

	guid = planet->guid;
	enablePhysics(0, 0, 0, 0.1f, World::ColMask::PLANET, World::ColMask::ALL);
	transform.setPosition(planet->transform.position);
	physics->rigidBody->getWorldTransform().setOrigin((Vector3) planet->transform.position);
}