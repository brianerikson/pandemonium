#include "stdafx.h"
#include "Font.h"

using namespace std;

Font::Font(EFont fontType, int pt, GLuint texture, FT_Face face) 
	: fontType(fontType), pt(pt), texture(texture), face(face) {}

Font::~Font() {
	FT_Done_Face(face);
	glDeleteTextures(1, &texture);
}
