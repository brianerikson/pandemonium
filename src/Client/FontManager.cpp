#include "stdafx.h"
#include "FontManager.h"

#define TO_PIXELS(val) (val >> 6)
#define FROM_PIXELS(val) (val << 6)

using namespace std;
using namespace glm;

FontManager::~FontManager() {
	for (Font *f : fonts) { delete f; }
	FT_Done_FreeType(library);
}

void FontManager::initialize(int screenWidth, int screenHeight, GLint texLocation) {
	if (initialized) { return; }

	FT_Error error;
	if ((error = FT_Init_FreeType(&library)) != FT_Err_Ok) {
		throw runtime_error("an error occurred during FreeType library initialization");
	}

	this->texLocation = texLocation;

	GLFWmonitor* primary = glfwGetPrimaryMonitor();
	int widthMM, heightMM;
	glfwGetMonitorPhysicalSize(primary, &widthMM, &heightMM);
	wDPI = (unsigned int) screenWidth / (widthMM / 25.4);
	hDPI = (unsigned int) screenHeight / (heightMM / 25.4);
	initialized = true;
}

Font* FontManager::getFont(EFont fontType, int pt) {
	for (int i = 0; i < fonts.size(); i++) {
		Font *font = fonts[i];
		if (font->fontType == fontType) {
			if (font->pt == pt) {
				return font;
			}
		}
	}

	return nullptr;
}

void FontManager::loadFont(EFont fontType, int pt) {
	if (getFont(fontType, pt)) { return; }
	FT_Error error;
	FT_Face face;

	switch ((error = FT_New_Face(library, FONT_PATHS.at(fontType), 0, &face))) {
	case FT_Err_Unknown_File_Format:
		throw runtime_error("Unsupported Font file format.");
	case FT_Err_Ok:
		break;
	default:
		throw runtime_error("The font file could not be opened or read, is broken, or too many fonts have been loaded.");
	}

	FT_Set_Char_Size(face, 0, FROM_PIXELS(pt), wDPI, hDPI);
	FT_Select_Charmap(face, ft_encoding_unicode);

	GLuint tex;
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	glUniform1i(texLocation, 0);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	fonts.push_back(new Font(fontType, pt, tex, face));
}

void FontManager::unloadFont(EFont fontType, int pt) {
	fonts.erase(
		remove_if(fonts.begin(), fonts.end(), 
			[fontType, pt](Font *font) { 
		if (font->fontType == fontType && font->pt == pt) {
			delete font;
			return true;
		}
		return false;
	}),
		fonts.end()
	);
}

void FontManager::render(EFont fontType, int pt, float x, float y, const std::string &text) {
	if (text.empty()) { return; }
	Font *font = getFont(fontType, pt);
	if (!font) { 
		loadFont(fontType, pt); 
		font = getFont(fontType, pt);
		if (!font) { throw runtime_error("Could not load font on request."); }
	}

	FT_Load_Char(font->face, 'T', FT_LOAD_RENDER);
	FT_GlyphSlot &glyph = font->face->glyph;
	float maxAscent = glyph->bitmap_top;

	glActiveTexture(GL_TEXTURE0);

	FT_Bitmap &bmp = font->face->glyph->bitmap;
	glBindTexture(GL_TEXTURE_2D, font->texture);

	for (char ch : text) {
		if (FT_Load_Char(font->face, ch, FT_LOAD_RENDER))
			continue;

		// Ref: https://stackoverflow.com/a/27476203
		float ascentOffset = maxAscent - glyph->bitmap_top;
		float descentOffset = TO_PIXELS(glyph->metrics.height) - glyph->bitmap_top;
		float x2 = x + glyph->bitmap_left;
		float y2 = -y - glyph->bitmap_top - descentOffset - ascentOffset;
		float w = (float) glyph->bitmap.width;
		float h = (float) glyph->bitmap.rows;

		GLfloat glyphVerts[]{
			x2,		-y2, 0.0f,		0.0f, 1.0f, // bl
			x2 + w, -y2, 0.0f,		1.0f, 1.0f, // br
			x2 + w, -y2 - h, 0.0f,	1.0f, 0.0f, // tr
			x2,		-y2 - h, 0.0f,	0.0f, 0.0f  // tl
		};

		glBufferData(GL_ARRAY_BUFFER, 20 * sizeof(GLfloat), glyphVerts, GL_STREAM_DRAW);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, bmp.width, bmp.rows, 0, GL_RED, GL_UNSIGNED_BYTE, bmp.buffer);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);

		x += (float) TO_PIXELS(glyph->advance.x);
		y += (float) TO_PIXELS(glyph->advance.y);
	}
}

glm::vec2 FontManager::findBounds(EFont fontType, int pt, const std::string &text) {
	Font *font = getFont(fontType, pt);
	if (!font) {
		loadFont(fontType, pt);
		font = getFont(fontType, pt);
		if (!font) { throw runtime_error("Could not load font on request."); }
	}

	float width = 0;
	float height = 0;
	for (char ch : text) {
		FT_Load_Char(font->face, ch, FT_LOAD_DEFAULT);
		FT_GlyphSlot &glyph = font->face->glyph;
		width += TO_PIXELS(glyph->metrics.horiAdvance);
		float curVertAdv = TO_PIXELS(glyph->metrics.vertAdvance - (glyph->metrics.vertBearingY * 2));
		if (curVertAdv > height) { height = curVertAdv; }
	}

	return {width, height};
}

#undef TO_PIXELS
#undef FROM_PIXELS