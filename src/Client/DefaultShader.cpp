#include "stdafx.h"
#include "DefaultShader.h"

using namespace glm;

DefaultShader::DefaultShader() : GameShader("res/shaders/default.vert", "res/shaders/default.frag") {
    modelLoc = glGetUniformLocation(programId, "model");
    viewLoc = glGetUniformLocation(programId, "view");
    projLoc = glGetUniformLocation(programId, "projection");
    viewPosLoc = glGetUniformLocation(programId, "viewPos");

    // light uniforms
    lpLoc = glGetUniformLocation(programId, "lightPosition");
    lAmbLoc = glGetUniformLocation(programId, "light.ambient");
    lcLoc = glGetUniformLocation(programId, "light.color");
    lBrightLoc = glGetUniformLocation(programId, "light.brightness");
	enableLightsLoc = glGetUniformLocation(programId, "enableLighting");
    // material uniforms
	// TODO: Change material struct to not contain Sampler2D as this is a compatibility issue
    matShinLoc = glGetUniformLocation(programId, "material.shininess");
	dcLoc = glGetUniformLocation(programId, "material.diffuse1_color");
	hasTexLoc = glGetUniformLocation(programId, "hasTexture");
	this->defaultsLoaded = true;
}


DefaultShader::~DefaultShader() {
}

void DefaultShader::update(Light *light, Camera *camera) {
    // camera
    vec3 camPos = camera->getPosition();
    glUniform3f(viewPosLoc, camPos.x, camPos.y, camPos.z);
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, value_ptr((glm::mat4)camera->view()));
    glUniformMatrix4fv(projLoc, 1, GL_FALSE, value_ptr(camera->projection));

    // lighting
    vec3 lightPos = light->transform.getPosition();
    glUniform3f(lpLoc, lightPos.x, lightPos.y, lightPos.z);
    glUniform3f(lcLoc, light->color.diffuse.x, light->color.diffuse.y, light->color.diffuse.z);
    glUniform1f(lAmbLoc, 0.2f);
    glUniform1f(lBrightLoc, 1.0f);

    // material
    glUniform1f(matShinLoc, 8.0f);
}

void DefaultShader::render(const Actor *actor) {
    for (unsigned int i = 0; i < actor->model->meshes.size(); i++) {
		Mesh mesh = actor->model->meshes[i];
		if (strcmp(mesh.materials[0].diffuse.path.C_Str(), "") == 0) { glUniform1i(hasTexLoc, 0); }
		else { glUniform1i(hasTexLoc, 1); }
		glUniform1i(enableLightsLoc, mesh.materials[0].enableLighting);
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, value_ptr(actor->transform.matrix * mesh.transform.matrix));
		glUniform3f(dcLoc, mesh.materials[0].diffuse_color.x, mesh.materials[0].diffuse_color.y, mesh.materials[0].diffuse_color.z);
        GameShader::render(mesh);
    }
}