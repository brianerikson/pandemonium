#include "stdafx.h"
#include "ShaderProgram.h"
#include "FileUtils.h"

using namespace std;

GLuint buildShader(const char *path, GLenum shaderType) {
    unique_ptr<string> shaderStr(FileUtils::readResource(path));
    GLuint shader = glCreateShader(shaderType);
    const char *shaderStrRef = shaderStr.get()->c_str();
    glShaderSource(shader, 1, &shaderStrRef, 0);
    glCompileShader(shader);

    int isCompiled;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
    if (isCompiled == false) {
        int maxLength = 0;

        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
        char *infoLog = (char*)malloc(maxLength);

        glGetShaderInfoLog(shader, maxLength, &maxLength, infoLog);
        cout << infoLog << endl;
        throw runtime_error(infoLog);
    }
    return shader;
}

ShaderProgram::ShaderProgram(const char *vertPath, const char *fragPath) {
    programId = glCreateProgram();

    GLuint vertexShader(buildShader(vertPath, GL_VERTEX_SHADER));
    GLuint fragmentShader(buildShader(fragPath, GL_FRAGMENT_SHADER));

    glAttachShader(id, vertexShader);
    glAttachShader(id, fragmentShader);

    glLinkProgram(id); // Compile and upload data to GPU

    int isLinked = 0;
    glGetProgramiv(id, GL_LINK_STATUS, &isLinked);
    if (isLinked == false) {
        int maxLength = 0;

        glGetProgramiv(id, GL_INFO_LOG_LENGTH, &maxLength);
        char *infoLog = (char*)malloc(maxLength);

        glGetProgramInfoLog(id, maxLength, &maxLength, infoLog);
        cout << infoLog << endl;
        throw runtime_error(infoLog);
    }

    // Delete shader refs as they are unused after successful program linking
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

}

ShaderProgram::~ShaderProgram() {
    glDeleteProgram(id);
}

void ShaderProgram::start() {
    if (!defaultsLoaded)
        throw std::runtime_error("Defaults haven't been loaded. Load defaults before using.");
    glUseProgram(programId);
	running = true;
}

void ShaderProgram::end() {
    glUseProgram(0);
	running = false;
}