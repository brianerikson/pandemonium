#pragma once
#include "UiRoot.h"
#include "Client.h"

struct GContext {
	UiRoot *uiRoot;
	Client *net;
};

enum SceneType {
	MAIN_MENU,
	LOBBY,
	GAME
};

class Scene {
public:
	// lifetime of widgets should end when this Scene ends
	virtual ~Scene() {
		uiRoot->clear(); 
	}

	virtual void update(float deltaTime) = 0;
	virtual void render() = 0;
	virtual void keyCallback(int key, int scancode, int action, int mods) = 0;
	virtual void mbCallback(int button, int action, int mods) = 0;
	virtual void mPosCallback(double x, double y) = 0;
	virtual void processInputEvents(double deltaTime) = 0;

	SceneType type;
protected:
	Scene::Scene(GContext &gameCtx, SceneType type) 
		: uiRoot(gameCtx.uiRoot), client(gameCtx.net), type(type) {}
	UiRoot *uiRoot;
	Client *client;
};