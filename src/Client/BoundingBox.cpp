#include "stdafx.h"
#include "BoundingBox.h"
#include "Mesh.h"

using namespace glm;

BoundingBox::BoundingBox(const std::vector<Vertex> &vertices) {
	const vec3 &vpos = vertices[0].position;
	float maxX = vpos.x;
	float minX = vpos.x;
	float maxY = vpos.y;
	float minY = vpos.y;
	float maxZ = vpos.z;
	float minZ = vpos.z;

	for (const Vertex &vert : vertices) {
		const vec3 &vp = vert.position;
		if (vp.x > maxX) {
			maxX = vp.x;
		}
		else if (vp.x < minX) {
			minX = vp.x;
		}

		if (vp.y > maxY) {
			maxY = vp.y;
		}
		else if (vp.y < minY) {
			minY = vp.y;
		}

		if (vp.z > maxZ) {
			maxZ = vp.z;
		}
		else if (vp.z < minZ) {
			minZ = vp.z;
		}
	}

	create(minX, maxX, minY, maxY, minZ, maxZ);
}

BoundingBox::BoundingBox(const std::vector<Mesh> &meshes) {
	const BoundingBox &bb = meshes[0].bBox;
	float maxX = bb.fbr.x;
	float minX = bb.fbl.x;
	float maxY = bb.ftl.y;
	float minY = bb.fbl.y;
	float maxZ = bb.bbl.z;
	float minZ = bb.fbl.z;

	for (const Mesh &mesh : meshes) {
		const BoundingBox &bBox = mesh.bBox;
		if (bBox.fbr.x > maxX) {
			maxX = bBox.fbr.x;
		}
		
		if (bBox.fbl.x < minX) {
			minX = bBox.fbl.x;
		}

		if (bBox.ftl.y > maxY) {
			maxY = bBox.ftl.y;
		}
		
		if (bBox.fbl.y < minY) {
			minY = bBox.fbl.y;
		}

		if (bBox.bbl.z > maxZ) {
			maxZ = bBox.bbl.z;
		}
		
		if (bBox.fbl.z < minZ) {
			minZ = bBox.fbl.z;
		}
	}

	create(minX, maxX, minY, maxY, minZ, maxZ);
}

BoundingBox::BoundingBox() {
	create(0, 1, 0, 1, 0, 1);
}

glm::vec3 BoundingBox::getSize() const {
	return { 
		abs(fbr.x - fbl.x),
		abs(ftl.y - fbl.y),
		abs(bbl.z - fbl.z)
	};
}

void BoundingBox::create(float minX, float maxX, float minY, float maxY, float minZ, float maxZ) {
	fbl = { minX, minY, minZ };
	ftl = { minX, maxY, minZ };
	fbr = { maxX, minY, minZ };
	ftr = { maxX, maxY, minZ };

	bbl = { minX, minY, maxZ };
	btl = { minX, maxY, maxZ };
	bbr = { maxX, minY, maxZ };
	btr = { maxX, maxY, maxZ };
}
