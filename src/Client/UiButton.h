#pragma once
#include "UiWidget.h"
#include "UiLabel.h"

class UiButton : public UiWidget {
public:
	UiButton();
	virtual void update(float deltaTime) override;
	void draw(UiRenderer *renderer) override;
	virtual void processInput(std::queue<std::shared_ptr<KeyEvent>> &keyEvents);
	virtual UiWidget* delegateFocus() override;
	virtual void propagateVisibilty(bool visible) override;
	virtual void setPosition(float x, float y) override;
	void setWidthToChars(int charAmt);
	void setText(std::string text);
	std::string getText();
	std::function<void()> onPress = nullptr;

	glm::vec4 color = { 53.0f / 255, 61.0f / 255, 68.0f / 255, 1.0f }; // #353D44
	glm::vec4 hoverColor = { 47.0f / 255, 54.0f / 255, 60.0f / 255, 1.0f }; // #2F363C
	glm::vec4 depressedColor = { 41.0f / 255, 47.0f / 255, 53.0f / 255, 1.0f }; // #292F35

private:
	bool pressed;
	UiLabel label;
	glm::vec4 *activeColor = nullptr;
};