#include "stdafx.h"
#include "UiButton.h"
#include "FontManager.h"


UiButton::UiButton() : UiWidget(BUTTON), activeColor(&color), label(ROBOTO_BOLD, 14, "") {

}

void UiButton::draw(UiRenderer *renderer) {
	renderer->drawBox(
		pos.x, pos.y,
		getInnerWidth(),
		getInnerHeight(),
		*activeColor
	);
	label.draw(renderer);
}

void UiButton::update(float deltaTime) {
	// TODO: Hovercolor doesn't seem to work
	if (activeColor != &depressedColor) {
		if (this->containsMouse()) { activeColor = &hoverColor; }
		else { activeColor = &color; }
	}
}

void UiButton::processInput(std::queue<std::shared_ptr<KeyEvent>> &keyEvents) {
	using Trigger = MouseEvent::Trigger;
	while (!keyEvents.empty()) {
		auto ke = keyEvents.front();
		if (ke->action == KeyEvent::Action::MOUSE) {
			auto me = std::static_pointer_cast<MouseEvent>(ke);
			if (me->trigger == Trigger::LMB_PRESSED) {
				activeColor = &depressedColor; 
			}
			else if (me->trigger == Trigger::LMB_RELEASED) {
				if (this->containsMouse()) { 
					activeColor = &hoverColor; 
					if (onPress) { onPress(); }
				}
				else { activeColor = &color; }
			}
		}
		keyEvents.pop();
	}
}

UiWidget* UiButton::delegateFocus() {
	if (this->containsMouse()) {
		return this;
	}

	return nullptr;
}

void UiButton::setPosition(float x, float y) {
	UiWidget::setPosition(x, y);
	label.setPosition(
		pos.x + dimensions.padding.left,
		pos.y + dimensions.padding.top
	);
}

void UiButton::setWidthToChars(int charAmt) {
	glm::vec2 charSize = FontManager::Instance().findBounds(label.font, label.pt, "A");
	dimensions.content.width = charSize.x * charAmt;
	dimensions.content.height = charSize.y;
	float padding = charSize.y * 0.3f;
	dimensions.padding = { padding, padding, padding, padding };
}

void UiButton::setText(std::string text) {
	label.setText(text);
	setWidthToChars(text.size());
}

std::string UiButton::getText() {
	return label.getText();
}

void UiButton::propagateVisibilty(bool visible) {
	UiWidget::propagateVisibilty(visible);
	label.propagateVisibilty(visible);
}
