#include "stdafx.h"
#include "GameShader.h"

using namespace std;

GameShader::GameShader(const char *vertPath, const char *fragPath) : ShaderProgram(vertPath, fragPath) {}

void GameShader::render(Mesh &mesh) {
	GLuint diffuseNum = 1;
	GLuint specularNum = 1;

	for (GLuint i = 0; i < mesh.materials.size(); i++) {
		Material material = mesh.materials[i];
		if (material.diffuse.id < 0) { continue; }
		glActiveTexture(GL_TEXTURE0 + i);
		stringstream ss;
		string number;
		string name = material.diffuse.type;
		if (name == "texture_diffuse") {
			ss << diffuseNum++;
		}
		else if (name == "texture_specular") {
			ss << specularNum++;
		}
		number = ss.str();

		glUniform1f(glGetUniformLocation(programId, ("material." + name + number).c_str()), (GLfloat)i);
		glBindTexture(GL_TEXTURE_2D, material.diffuse.id);
	}
	glActiveTexture(GL_TEXTURE0);

	glBindVertexArray(mesh.vao);
	glDrawElements(GL_TRIANGLES, (GLsizei)mesh.indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}
