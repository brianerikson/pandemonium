#include "stdafx.h"
#include "Light.h"

using namespace glm;

Light::Light(const SLNet::RakNetGUID &owner) : GameObject(Netcom::GoType::GAME_OBJECT, owner, true, true) {
}

Light::Light(aiLight *light, aiMatrix4x4 &transform, 
	const SLNet::RakNetGUID &owner) : Light(owner) {

    this->angleInnerCone = light->mAngleInnerCone;
    this->angleOuterCone = light->mAngleOuterCone;
    this->attenuationConstant = light->mAttenuationConstant;
    this->attenuationLinear = light->mAttenuationLinear;
    this->attenuationQuadratic = light->mAttenuationQuadratic;
    this->color = Color{
        vec3(light->mColorAmbient.r, light->mColorAmbient.g, light->mColorAmbient.b),
        vec3(light->mColorDiffuse.r, light->mColorDiffuse.g, light->mColorDiffuse.b),
        vec3(light->mColorSpecular.r, light->mColorSpecular.g, light->mColorSpecular.b)
    };


    aiVector3D t, s;
    aiQuaternion r;
    transform.Decompose(s, r, t);
    auto rm = r.GetMatrix();
    mat4 rotator = mat4(
        rm.a1, rm.a2, rm.a3, 0.0,
        rm.b1, rm.b2, rm.b3, 0.0,
        rm.c1, rm.c2, rm.c3, 0.0,
        0.0, 0.0, 0.0, 1.0
    );
    this->transform.setPosition(vec3(t.x, t.y, t.z));
    this->transform.setScale(vec3(s.x, s.y, s.z));
    this->transform.setRotation(rotator);
}