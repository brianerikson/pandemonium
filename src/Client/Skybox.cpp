#include "stdafx.h"
#include "Skybox.h"

Skybox::Skybox(World *gameWorld, const SLNet::RakNetGUID &owner) 
	: Actor(owner, true, true, Netcom::GoType::SKYBOX, gameWorld, ShaderType::DEFAULT) {
	model->meshes[0].materials[0].enableLighting = 0;
}
