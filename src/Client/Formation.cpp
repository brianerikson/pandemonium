#include "stdafx.h"
#include "Formation.h"



Formation::Formation(unsigned int slots) 
	: guids(slots) {}

Formation::Formation(const std::vector<Actor*> &actors) {
	guids.reserve(actors.size());
	for (Actor *actor : actors) {
		guids.push_back(actor->guid);
	}

	finalize();
}

Formation::~Formation() {
	if (pathMesh) {
		delete pathMesh;
	}
}

void Formation::add(xg::Guid guid) {
	guids.push_back(guid);
}

void Formation::finalize() {
	if (guids.empty()) { throw std::runtime_error("Formation is empty!"); }
	// wall should be twice as long as tall
	// 
	// 10 units = 2 rows of 5
	// 20 units = 3 rows of 6.6 // OVERFLOW 3 rows of 7 (21 slots total, one empty)
	// 30 units = 4 rows of 7.5 // OVERFLOW 4 rows of 8 (32 total, two empty)
	// 0,0 is center
	unsigned int objs = (unsigned int)guids.size();

	// TODO: need to abstract formations into different types. In this case
	// Slices is == 1 because it's a wall formation
	depth = 1;
	
	// find rows to columns
	float colsToRow = 2.f;
	float bestDiff = std::numeric_limits<float>::max();
	float lastDiff = bestDiff;
	for (unsigned int row = 1; row <= objs; row++) {
		unsigned int col = (unsigned int)std::ceil((float) objs / (float)row);
		float ratio = (float)col / row;
		float diff = abs(colsToRow - ratio);
		if (lastDiff < diff) {
			break;
		}
		else if ( diff < bestDiff) {
			bestDiff = diff;
			height = row;
			width = col;
		}
		lastDiff = diff;
	}

	unsigned int freeSlots = (height * width) - objs;

	// TODO: Adjust positions based on free slots to center-align formation
	unsigned int curRow = 0;
	for (unsigned int x = 0; x < width; x++) {
		for (unsigned int y = 0; y < height; y++) {
			unsigned int idx = x + (width * y);
			if (idx > objs) { break; }
			xg::Guid &guid = guids[idx];
			positions[guid] = glm::vec3{x, y, 0.f};
		}
	}

	pathMesh = new PathNav(width, height, 1, 2, padding);
}

std::list<Vector3> Formation::getPath(const xg::Guid &guid, const Vector3 &from, const Vector3 &origin) {
	Vector3 localDest;
	auto find = positions.find(guid);
	if (find != positions.end()) { 
		localDest = find->second;
	}
	else { return std::list<Vector3>(); }

	auto path = pathMesh->getPath(from, localDest, origin);
	if (path.empty()) {
		// TODO: instead of enforcing path on impossible destination, use collision avoidance
		PathNav altMesh = PathNav(width, height, 1, 1, padding);
		path = altMesh.getPath(from, localDest, origin);
		DBG_ASSERT(!path.empty());
	}

	return path;
}

Vector3 Formation::getPosition(const xg::Guid &guid, const Vector3 &origin) {
	if (positions.find(guid) != positions.end()) {
		return positions[guid] + origin;
	}

	return Vector3();
}