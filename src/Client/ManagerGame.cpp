#include "stdafx.h"
#include "ManagerGame.h"
#include "SceneMainMenu.h"
#include "SceneWorld.h"
#include "SceneLobby.h"
#include "Planet.h"

using namespace std;

ManagerGame::~ManagerGame() {
	fmodMasterBank->unload();
	fmod->release();
	delete gameCtx.net;
	delete gameCtx.uiRoot;
	if (serverHandle.isRunning()) { serverHandle.stop(); }
}

void ManagerGame::initialize(unsigned int screenWidth, unsigned int screenHeight) {
	//// FMOD init
	FMOD_CHECK(FMOD::Studio::System::create(&fmod), "FMOD create failure"); // Create the Studio System object.

	// Initialize FMOD Studio, which will also initialize FMOD Low Level
	FMOD_CHECK(fmod->initialize(512, FMOD_STUDIO_INIT_NORMAL, FMOD_INIT_NORMAL, 0), "FMOD init failure");


	FMOD_CHECK(fmod->loadBankFile("res/sound/Master Bank.bank",
		FMOD_STUDIO_LOAD_BANK_NORMAL, &fmodMasterBank), "Could not load FMOD bank file.");
	FMOD_CHECK(fmod->loadBankFile("res/sound/Master Bank.strings.bank",
		FMOD_STUDIO_LOAD_BANK_NORMAL, &fmodMasterBank), "Could not load FMOD bank file.");
	////

	UiRenderer &uiRenderer = UiRenderer::Instance(); // start lazy-iniitalizion
	uiRenderer.initialize(screenWidth, screenHeight);

	gameCtx.net = new Client();
	gameCtx.uiRoot = new UiRoot(screenWidth, screenHeight);

	SceneMainMenu *mmScene = new SceneMainMenu(gameCtx);
	scene = (Scene*) mmScene;
#ifdef TEST_LAUNCH
	if (serverHandle.start({ 1 })) {
		gameCtx.net->requestConnect("127.0.0.1", SERVER_UDP_PORT, mmScene->getPlayerName());
}
	else {
		printf("ERROR: Could not start the server!\n");
	}
#else
	mmScene->playerNumWindow.numTextBox.onSubmit = [=](std::string numStr) {
		int num = 0;
		try { num = stoi(numStr); }
		catch (exception e) {
			printf("ERROR: Invalid input for number of players.\n");
			printf("	   Must be between a number between 0 and %i.\n",
				MAX_PLAYERS);
			return;
		}

		if (num < 1 || num > MAX_PLAYERS) {
			printf("ERROR: Invalid input for number of players.\n");
			printf("	   Must be between a number between 0 and %i.\n",
				MAX_PLAYERS);
			return;
		}

		if (serverHandle.start({num})) {
			gameCtx.net->requestConnect("127.0.0.1", SERVER_UDP_PORT, mmScene->getPlayerName());
		}
		else {
			printf("ERROR: Could not start the server!\n");
		}
	};
	mmScene->playerNumWindow.okButton.onPress = [=]() {
		mmScene->playerNumWindow.numTextBox.onSubmit(
			mmScene->playerNumWindow.numTextBox.getText()
		);
	};
#endif
}

void ManagerGame::process(float deltaTime) {
	scene->processInputEvents(deltaTime);

	processNetwork();

	scene->update(deltaTime);
	scene->render();

	fmod->update();
}

ManagerGame::ManagerGame() {}

void ManagerGame::processNetwork() {
	using Type = Netcom::Payload::Type;

	auto &payloads = gameCtx.net->poll();
	for (Netcom::Payload *payload : payloads) {
		switch (payload->type) {
		case Type::SVR_JOIN_RESPONSE:
		{
			if (scene->type != MAIN_MENU) { break; }
			localPlayerName = ((SceneMainMenu*) scene)->getPlayerName().substr(0, MAX_PLAYER_NAME_LEN);
			Netcom::SPL_JoinResponse *jr = (Netcom::SPL_JoinResponse*) payload;
			localPlayerGuid = jr->playerId;
			break;
		}
		case Type::SVR_LEVEL_LOAD:
		{
			delete scene;

			Netcom::SPL_LevelLoad *level = (Netcom::SPL_LevelLoad*) payload;
			SceneWorld *gameScene = new SceneWorld(gameCtx, localPlayerGuid);
			scene = (Scene*) gameScene;

			for (auto &planet : level->system.planets) {
				gameScene->spawnActor(&planet);
			}
			gameScene->camera.setRotation(0.0f, 0.0f, 0.0f);
			gameScene->camera.setPosition(0.0f, 50.0f, 0.0f);
			break;
		}
		case Type::SVR_LOBBY_STATE:
		{
			Netcom::SPL_LobbyState *state = (Netcom::SPL_LobbyState*) payload;
			if (scene->type != LOBBY) { loadLobby(state); }
			else {
				SceneLobby *lobby = (SceneLobby*) scene;
				lobby->updateLobby(state->maxPlayers, state->players);
			}
			break;
		}
		case Type::SVR_AUTHORITY_LEVEL:
		{
			using SPL_Auth = Netcom::SPL_AuthorityLevel;
			SPL_Auth *authLevel = (SPL_Auth*) payload;
			if (scene->type == LOBBY && authLevel->level == SPL_Auth::ADMIN) {
				SceneLobby *lobby = (SceneLobby*) scene;
				lobby->enableAdminControls();
			}
			this->serverAuthLevel = authLevel->level;
			break;
		}
		case Type::SVR_SPAWN_RESPONSE:
		{
			DBG_ASSERT(scene->type == GAME);
			using SPL_Spawn = Netcom::SPL_SpawnResponse;
			SPL_Spawn *spawnRes = (SPL_Spawn*) payload;
			SceneWorld *gs = (SceneWorld*) scene;
			if (spawnRes->approved) {
				gs->spawnActor(spawnRes->gameObject);
			}
			break;
		}
		case Type::SVR_STATE_TICK:
		{
			DBG_ASSERT(scene->type == GAME);
			using SPL_State = Netcom::SPL_StateTick;
			SPL_State *state = (SPL_State*) payload;
			SceneWorld *gs = (SceneWorld*) scene;
			gs->applyServerTick(*state->updates);
			break;
		}
		default:
			break;
		}
	}

	while (!payloads.empty()) {
		delete payloads.back();
		payloads.erase(payloads.end() - 1);
	}
}

void ManagerGame::loadLobby(Netcom::SPL_LobbyState *state) {
	delete scene;
	SceneLobby *lobbyScene = new SceneLobby(state->maxPlayers, state->players, gameCtx);
	scene = (Scene*) lobbyScene;
}

std::string ManagerGame::getLocalPlayerName() {
	return localPlayerName;
}

void ManagerGame::keyCallback(int key, int scancode, int action, int mods) {
	scene->keyCallback(key, scancode, action, mods);
	gameCtx.uiRoot->rawKeyInput(key, scancode, action, mods);
}

void ManagerGame::mbCallback(int button, int action, int mods) {
	scene->mbCallback(button, action, mods);
	gameCtx.uiRoot->mbInput(button, action, mods);
}

void ManagerGame::mPosCallback(double x, double y) {
	scene->mPosCallback(x, y);
}

void ManagerGame::charCallback(unsigned int codepoint) {
	gameCtx.uiRoot->charInput(codepoint);
}