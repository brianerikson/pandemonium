#include "stdafx.h"
#include "FPSCamera.h"
#include "FPSCamControls.h"

using namespace glm;

extern GLFWwindow *window;
FPSCamera::FPSCamera() : Camera(window, new FPSCamControls(this)) {
	lhOrient = glm::rotate(lhOrient, radians(180.0f), Direction::RIGHT) *
		glm::rotate(lhOrient, radians(180.0f), Direction::FORWARD);
	update();
}

FPSCamera::FPSCamera(aiCamera * camera, aiMatrix4x4 & transform) : Camera(camera, new FPSCamControls(this)) {
    aiVector3D t, s;
    aiQuaternion r;
    transform.Decompose(s, r, t);
    this->position = vec3(t.x, t.y, t.z);
    vec3 angles = degrees(glm::eulerAngles(quat(r.w, r.x, r.y, r.z))); // pitch roll yaw from blender
    setRotation(angles.x, angles.z, angles.y);

	lhOrient = glm::rotate(lhOrient, radians(180.0f), Direction::RIGHT) * glm::rotate(lhOrient, radians(180.0f), Direction::FORWARD);
    update();
}


FPSCamera::~FPSCamera() {
}

void FPSCamera::update() {
    if (!dirty) 
        return;

	rotTransform = orientation * lhOrient;
    transform = glm::translate(mat4(1.0f), position) * rotTransform;
    dirty = false;
}

void FPSCamera::translate(float x, float y, float z) {
    dirty = true;
    position += vec3(x, y, z);
}

void FPSCamera::setPosition(float x, float y, float z) {
    dirty = true;
    position = vec3(x, y, z);
}

Vector3 FPSCamera::getPosition()
{
    return position;
}

// In radians
mat4 yawPitch(float yaw, float pitch) {
    mat4 rot = glm::rotate(mat4(1.0f), yaw, Direction::UP);
    return glm::rotate(rot, pitch, Direction::RIGHT);
}

void FPSCamera::rotate(float pitch, float yaw, float roll) {
    dirty = true;

    this->pitch += radians(pitch);
    this->yaw += radians(yaw);
    orientation = yawPitch(this->yaw, this->pitch);
}

void FPSCamera::rotate(float angle, vec3 dir) {
    throw std::runtime_error("This method is unsupported");
}

void FPSCamera::setRotation(float pitch, float yaw, float roll) {
    dirty = true;

    this->pitch = radians(pitch);
    this->yaw = radians(yaw);
    orientation = yawPitch(this->yaw, this->pitch);
}

void FPSCamera::setRotation(float angle, vec3 dir) {
    throw std::runtime_error("This method is unsupported");
}

mat4 FPSCamera::getOrientation() {
    return rotTransform;
}

Vector3 FPSCamera::getRotation()
{
    return degrees(eulerAngles(toQuat(rotTransform)));
}

vec3 FPSCamera::forward() {
	return vec3(rotTransform * vec4{ 0.0f, 0.0f, -1.0f, 1.0f });
}

vec3 FPSCamera::up() {
    return vec3(rotTransform * vec4(Direction::UP, 1.0f));
}

vec3 FPSCamera::right() {
    return vec3(rotTransform * vec4(Direction::RIGHT, 1.0f));
}

Matrix4x4 FPSCamera::view() const
{
    return glm::inverse(transform);
}

Vector3 FPSCamera::unproject(const glm::vec2 &screenPos, bool farPlane) const {
	int w, h;
	glfwGetWindowSize(window, &w, &h);
	float zWin = farPlane ? 1.f : 0.f;
	return glm::unProject(
		glm::vec3{ screenPos.x, h - screenPos.y, zWin },
		view(), 
		projection, 
		glm::vec4{0.f, 0.f, w, h}
	);
}
