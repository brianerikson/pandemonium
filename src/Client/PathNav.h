#pragma once
#include "MathInterop.h"

// http://theory.stanford.edu/~amitp/GameProgramming/
class PathNav {
public:

	//************************************
	// Parameter: unsigned int gridWidth
	// Parameter: unsigned int gridHeight
	// Parameter: unsigned int gridDepth
	// Parameter: unsigned int thickness
	// Parameter: float cellPadding
	//************************************
	PathNav(unsigned int gridWidth, unsigned int gridHeight, 
		unsigned int gridDepth, unsigned int thickness, float cellPadding);
	~PathNav();

	
	//************************************
	// Returns:   std::list<Vector3> path
	// Parameter: const glm::vec3 & from 
	//		Start of the path in world coordiantes
	// Parameter: const glm::vec3 & to 
	//		Target pos in grid coordinates
	// Parameter: const Vector3 & worldOrigin
	//		origin of the grid
	//************************************
	std::list<Vector3> getPath(const glm::vec3 &from, const glm::vec3 &to, const Vector3 &worldOrigin);

private:
	struct Cell {
		Cell *parent = nullptr;
		glm::vec3 pos;
		float g;
		float h; // manhattan distance

		bool operator==(const Cell &other);
		float getScore() { return g + h; }
	};

	void findAdjacentCells(Cell &cell, const glm::vec3 &gridDest);
	Cell generateCell(const glm::vec3 &gridPos, const glm::vec3 &gridDest, Cell *parent = nullptr);
	glm::vec3 translateVec(const glm::vec3 &vec);
	void checkAdd(const glm::vec3 &node, const glm::vec3 &gridDest, Cell &cell);
	float manhattanDist(const glm::vec3 &from, const glm::vec3 &to);

	// Used to adjust distance between each ship
	float padding;
	// How thick the pathing grid is that encapsulates the formation grid
	unsigned int thickness;
	unsigned int gridWidth, gridHeight, gridDepth;
	bool ***pathGrid = nullptr;
	std::list<Cell> open;
	std::list<Cell> closed;
};