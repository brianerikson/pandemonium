#pragma once

using Vector2 = glm::vec2;

class Vector3 : public glm::vec3 {
public:
	Vector3() = default;
	Vector3(glm::vec3 vec);
	Vector3(btVector3 vec);

	operator btVector3() const;
};

class Vector4 : public glm::vec4 {
public:
	Vector4() = default;
	Vector4(glm::vec4 vec);
	Vector4(btVector4 vec);

	operator btVector4() const;
};

class Matrix4x4 : public glm::mat4 {
public:
	Matrix4x4() = default;
	Matrix4x4(const glm::mat4 &mat);
	Matrix4x4(const btMatrix3x3 &mat);

	operator btMatrix3x3() const;
};

class Quat : public glm::quat {
public:
	Quat() = default;
	Quat(const glm::quat &quat);
	Quat(const btQuaternion &quat);

	operator btQuaternion() const;
};