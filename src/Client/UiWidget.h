#pragma once
#include "UiRenderer.h"
#include "KeyEvent.h"

class UiWidget {
public:
	enum Type {
		WINDOW,
		CONTAINER,
		LABEL,
		TEXTBOX,
		BUTTON
	};

	struct Dimensions {
		struct Dim {
			float left = 0;
			float right = 0;
			float top = 0;
			float bottom = 0;
		};
		struct Size {
			float width = 0;
			float height = 0;
		};
		// Content width and height
		// typically set by the widget itself, unless it's a UiWindow/Container
		// In Pixels
		Size content{};

		// Space between border and other outer elements 
		// Unset by default
		// In Pixels
		Dim margin{};

		// Space between border and element content
		// Unset by default
		// In Pixels
		Dim padding{};
	};

	virtual ~UiWidget();

	virtual void update(float deltaTime);
	virtual void draw(UiRenderer *renderer) = 0;

	// Widget must consume all events
	virtual void processInput(std::queue<std::shared_ptr<KeyEvent>> &keyEvents);

	//! Called when mouse has been clicked. Return self if contains mouse or delegate to children
	virtual UiWidget* delegateFocus() = 0;

	//! Internally used to ensure visibilty is reflected in children
	virtual void propagateVisibilty(bool visible);

	//! Resizes widget to fit only contained content. Does not adjust padding
	virtual void resizeToContent();

	virtual glm::vec2 getPosition();
	virtual void setPosition(float x, float y);

	//! Centers the widget on a 2D coordinate point
	virtual void setCenter(float x, float y);

	//! Calculates total width of widget including margins
	virtual float getWidth();

	//! Calculates total height of widget including margins
	virtual float getHeight();

	//! Padding + Content
	virtual float getInnerWidth();

	//! Padding + Content
	virtual float getInnerHeight();

	//! Determines if the Widget (including specified padding) contains the mouse pointer
	bool containsMouse();

	bool isVisibile();

	//! Sets the visibility of the widget and all of it's children
	void setVisible(bool visibility);

	Dimensions dimensions{};
	bool focused = false;
	Type type;
protected:
	glm::vec2 pos{};
	UiWidget(Type widgetType);

private:
	bool visible = false;
};