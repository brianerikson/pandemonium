#pragma once
#include "GameShader.h"
#include "Light.h"
#include "Camera.h"
#include "Actor.h"

class DefaultShader : public GameShader {
public:
    DefaultShader();
    virtual ~DefaultShader();

    void update(Light *light, Camera *camera) override;
    void render(const Actor *actor) override;
private:
    GLuint viewLoc, projLoc, modelLoc, lpLoc, lcLoc, dcLoc, lAmbLoc, lBrightLoc, viewPosLoc, matShinLoc, hasTexLoc, enableLightsLoc;
};

