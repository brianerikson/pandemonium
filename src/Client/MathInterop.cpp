#include "stdafx.h"
#include "MathInterop.h"

Vector3::Vector3(glm::vec3 vec) {
	x = vec.x;
	y = vec.y;
	z = vec.z;
}

Vector3::Vector3(btVector3 vec) {
	x = vec.x();
	y = vec.y();
	z = vec.z();
}

Vector3::operator btVector3() const {
	return btVector3{ x, y, z };
}
Vector4::Vector4(glm::vec4 vec) {
	x = vec.x;
	y = vec.y;
	z = vec.z;
	w = vec.w;
}

Vector4::Vector4(btVector4 vec) {
	x = vec.x();
	y = vec.y();
	z = vec.z();
	w = vec.w();
}

Vector4::operator btVector4() const {
	return btVector4{ x, y, z, w };
}

#define m (*this)
Matrix4x4::Matrix4x4(const btMatrix3x3 &mat) {
	for (int c = 0; c < 3; c++) {
		auto &vec = mat.getColumn(c);
		m[c][0] = vec.x();
		m[c][1] = vec.y();
		m[c][2] = vec.z();
		m[c][3] = 0.f;
	}
	m[3][0] = 0.f;
	m[3][1] = 0.f;
	m[3][2] = 0.f;
	m[3][3] = 1.f;
}

Matrix4x4::Matrix4x4(const glm::mat4 &mat) {
	for (int c = 0; c < 4; c++) {
		for (int r = 0; r < 4; r++) {
			m[c][r] = mat[c][r];
		}
	}
}

Matrix4x4::operator btMatrix3x3() const {
	// Convert from column-major to row-major
	return btMatrix3x3 (
		m[0][0], m[1][0], m[2][0],
		m[0][1], m[1][1], m[2][1],
		m[0][2], m[1][2], m[2][2]
	);
}
#undef m

Quat::Quat(const glm::quat &quat) {
	x = quat.x;
	y = quat.y;
	z = quat.z;
	w = quat.w;
}

Quat::Quat(const btQuaternion &quat) {
	x = quat.x();
	y = quat.y();
	z = quat.z();
	w = quat.w();
}

Quat::operator btQuaternion() const {
	return btQuaternion(x, y, z, w);
}
