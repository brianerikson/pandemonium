#pragma once
#include "NetCommon/net_common.h"

class ServerHandle {
public:
	struct StartArgs {
		int numPlayers = MAX_PLAYERS;
	};

	//! Start the server. Will return true if it's already running a server.
	bool start(StartArgs args);

	//! Stop the server. Repeated calls will do nothing.
	void stop();

	bool isRunning();
private:
	bool running = false;

#ifdef __linux__

#elif _WIN32
	PROCESS_INFORMATION procInfo;
#endif
};