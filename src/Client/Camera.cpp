#include "stdafx.h"
#include "Camera.h"

using namespace std;
using namespace glm;

Camera::Camera(float aspect, float clipNear, float clipFar, float fov) {
    this->aspect = aspect;
    this->clipPlaneNear = clipNear;
    this->clipPlaneFar = clipFar;
    this->fov = fov;
    updateFrustum();
}

Camera::Camera(GLFWwindow *window, InputController *ic) {
    int w, h;
    glfwGetWindowSize(window, &w, &h);
    this->inputController = unique_ptr<InputController>(ic);
    this->aspect = (float) w / (float) h;
    updateFrustum();
}

Camera::Camera(aiCamera *camera, InputController *ic) {
    this->aspect = camera->mAspect;
    this->clipPlaneFar = camera->mClipPlaneFar;
    this->clipPlaneNear = camera->mClipPlaneNear;
    this->fov = degrees(camera->mHorizontalFOV);
    this->inputController = unique_ptr<InputController>(ic);


    updateFrustum();
}

Camera::~Camera() {
}

void Camera::updateFrustum() {
    projection = perspective(radians(this->fov), this->aspect, this->clipPlaneNear, this->clipPlaneFar);
}