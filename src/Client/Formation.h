#pragma once
#include "MathInterop.h"
#include "Actor.h"
#include "PathNav.h"

class Formation {
public:
	//! Unoptimized init
	Formation() = default;

	//! Optimized for adding guids one at a time
	Formation(unsigned int slots);

	//! Generate formation for all actors in array
	Formation(const std::vector<Actor*> &actors);
	virtual ~Formation();

	//! Call finalize when finished
	void add(xg::Guid guid);

	//! Generates positions for each guid
	void finalize();

	// call after formation is finalized
	std::list<Vector3> getPath(const xg::Guid &guid, const Vector3 &from, const Vector3 &origin);

	//! Ensure finalize was called before retrieving positions
	// Will return vec{0} if guid is not in formation
	Vector3 getPosition(const xg::Guid &guid, const Vector3 &origin);
private:
	// Used to adjust distance between each ship
	float padding = 20.f;
	unsigned int height, width, depth;
	std::vector<xg::Guid> guids;
	std::unordered_map<xg::Guid, Vector3> positions;
	PathNav *pathMesh;
};