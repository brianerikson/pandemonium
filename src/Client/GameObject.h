#pragma once
#include "Transform.h"
#include "NetCommon/GoTypes.h"
#include "Client.h"

class GameObject {
public:
    GameObject(Netcom::GoType t, const SLNet::RakNetGUID &owner, bool isLocal, bool locallyOwned);
    virtual ~GameObject() = default;
    
	void virtual update(const float &deltaTime);
	

	SLNet::RakNetGUID owner;
	const bool isLocal;
	bool locallyOwned;
	bool updateNet;
	float lastNetUpdateDt = 0.f;
	Client *client;

	xg::Guid guid;
	Transform transform;
	const Netcom::GoType type;
};

