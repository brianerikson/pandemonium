#pragma once

#include "NetCommon/net_common.h"

class Client {
public:
	Client();
	~Client();
	void requestConnect(const char *ip, int port, const std::string &name);

	//! Emits consumable payloads when received from server
	//  Caller must delete each payload and clear the vector when finished
	std::vector<Netcom::Payload*>& poll();
	void send(const SLNet::BitStream *stream, PacketPriority priority, 
		PacketReliability reliability, char orderingChannel);
private:
	SLNet::RakPeerInterface *client;
	SLNet::SystemAddress endPoint;
	std::vector<Netcom::Payload*> stash;
	bool connected;
	std::string clientName;
};