#pragma once

#include "NetCommon/Planet.h"
#include "Actor.h"

class Planet : public Actor {
public:
	Planet(World *world, Netcom::Planet *planet);
	virtual ~Planet() = default;

private:
	unsigned short worth; 
};