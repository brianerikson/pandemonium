#pragma once
#include "Font.h"

class FontManager {
public:
	static FontManager& Instance() {
		static FontManager fm;
		return fm;
	}

	FontManager(FontManager const&) = delete;
	void operator=(FontManager const&) = delete;
	~FontManager();

	// Initial FontManager Setup. Subsequent calls will be ignored.
	// Intialized by UiRenderer.
	void initialize(int screenWidth, int screenHeight, GLint texLocation);

	// must call shader start, glBindVertexArray, and glBindBuffer before calling
	// 0,0 top-left
	void render(EFont fontType, int pt, float x, float y, const std::string &text);

	//! Returns the width and height of the requested string as pixels
	glm::vec2 findBounds(EFont fontType, int pt, const std::string &text);

private:
	FontManager() = default;

	void loadFont(EFont fontType, int pt);
	void unloadFont(EFont fontType, int pt);

	// Returns nullptr if not found
	Font* getFont(EFont fontType, int pt);

	FT_Library library;
	std::vector<Font*> fonts;
	unsigned int wDPI, hDPI;
	GLint texLocation;
	bool initialized = false;
};