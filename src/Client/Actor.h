#pragma once
#include "GameObject.h"
#include "Model.h"
#include "NetCommon/GoTypes.h"
#include "World.h"

 // Here to avoid circular dependencies
enum class ShaderType : int {
	DEFAULT
};

class MotionState : public btMotionState {
public:
	MotionState(Transform &t) : t(t) {}
	virtual void getWorldTransform(btTransform& worldTrans) const override;
	virtual void setWorldTransform(const btTransform& worldTrans) override;
	Transform &t;
};

class Actor : public GameObject {
public:
	struct Physics {
		MotionState motionState;
		btRigidBody *rigidBody = nullptr;
		PhysUserData userData;
		btScalar mass;
		btScalar force;
		float maxSpeed;
		float turnSpeed;
		int group, mask;
	} *physics = nullptr;

	// Owned by GameWorld; do not delete
	Model* model;
	ShaderType shaderType;
	const Netcom::GoType type;

	Actor(const SLNet::RakNetGUID &owner, bool isLocal, bool locallyOwned, Netcom::GoType t, World *world, ShaderType shaderType);
	virtual ~Actor();

	//! Enable physics on this actor. group and mask are of type ColMask
	void enablePhysics(const btScalar &mass, const btScalar &force, 
		const float &maxSpeed, const float &turnSpeed, int group, int mask);

	World *world;
};