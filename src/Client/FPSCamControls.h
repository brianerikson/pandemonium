#pragma once
#include "InputController.h"
#include "FPSCamera.h"

class FPSCamControls : public InputController {
public:
    FPSCamControls(FPSCamera *camera);
    virtual ~FPSCamControls();

    void inputCallback(int key, int scancode, int action, int mods);
    void update(double deltaTime);

private:
    FPSCamera *camera; // don't worry about dealloc as this class is a member of FPSCamera
    static const int KEYSTATE_SIZE = 348;
    int keyState[KEYSTATE_SIZE];
    const float MOVE_SPEED_SEC = 10.0f;
    const float DEGREE_SEC = 45.0f;
	float speedMul = 1;

    void processKeyEvent(double deltaTime, int key);
};

