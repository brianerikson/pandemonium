#include "stdafx.h"
#include "PathNav.h"

PathNav::PathNav(unsigned int gridWidth, unsigned int gridHeight, 
	unsigned int gridDepth, unsigned int thickness, float cellPadding)
	: gridWidth(gridWidth + (thickness * 2)), 
	  gridHeight(gridHeight + (thickness * 2)), 
	  gridDepth(gridDepth + (thickness * 2)), 
	  padding(cellPadding), thickness(thickness) {

	// Initialize pathing grid
	pathGrid = new bool**[this->gridWidth];
	for (unsigned int x = 0; x < this->gridWidth; x++) {
		pathGrid[x] = new bool*[this->gridHeight];
		for (unsigned int y = 0; y < this->gridHeight; y++) {
			pathGrid[x][y] = new bool[this->gridDepth];
			for (unsigned int z = 0; z < this->gridDepth; z++) {
				pathGrid[x][y][z] = false;
			}
		}
	}

	for (unsigned int x = thickness; x < this->gridWidth - thickness; x++) {
		for (unsigned int y = thickness; y < this->gridHeight - thickness; y++) {
			for (unsigned int z = thickness; z < this->gridDepth - thickness; z++) {
				pathGrid[x][y][z] = true;
			}
		}
	}
}

PathNav::~PathNav() {
	for (unsigned int x = 0; x < gridWidth; x++) {
		for (unsigned int y = 0; y < gridHeight; y++) {
			delete[] pathGrid[x][y];
		}
		delete[] pathGrid[x];
	}
	delete[] pathGrid;
}

std::list<Vector3> PathNav::getPath(const glm::vec3 &from, const glm::vec3 &to, const Vector3 &worldOrigin) {
	Vector3 localDest = to + glm::vec3{ thickness, thickness, thickness };
	DBG_ASSERT(open.empty() && closed.empty());

	// get closest available point on the grid
	float closestDist = std::numeric_limits<float>::max();
	glm::vec3 closestNode;
	for (unsigned int x = 0; x < gridWidth; x++) {
		for (unsigned int y = 0; y < gridHeight; y++) {
			for (unsigned int z = 0; z < gridDepth; z++) {
				glm::vec3 node = { x, y, z };
				glm::vec3 point = translateVec(node) + worldOrigin;

				float dist = glm::distance(from, point);
				if (dist < closestDist && !pathGrid[x][y][z]) {
					closestDist = dist;
					closestNode = node;
				}
			}
		}
	}
	//pathGrid[(int) closestNode.x][(int) closestNode.y][(int) closestNode.z] = true;
	closed.push_back(Cell{ nullptr, closestNode, 0, manhattanDist(closestNode, localDest) });

	// get adjacent positions on the grid
	Cell *lastCell = nullptr;
	while (closed.back().h != 0.f) {
		if (lastCell == &closed.back()) { break; }
		lastCell = &closed.back();
		findAdjacentCells(closed.back(), localDest);
		if (open.empty()) { break; }

		Cell *bestCell = &open.back();
		for (Cell &cell : open) {
			if (cell.getScore() <= bestCell->getScore()) { bestCell = &cell; }
		}
		open.remove(*bestCell);
		closed.push_back(*bestCell);
	}

	std::list<Vector3> path;
	if (closed.back().h == 0.f) {
		// compile path
		Cell *curParent = &closed.back();
		do {
			pathGrid[(int) curParent->pos.x][(int) curParent->pos.y][(int) curParent->pos.z] = true;
			path.push_front(translateVec(curParent->pos) + worldOrigin);
			curParent = curParent->parent;
		} while (curParent != nullptr);
	}

	open.clear();
	closed.clear();

	return path;
}

glm::vec3 PathNav::translateVec(const glm::vec3 &vec) {
	float qHeight = gridHeight * 0.25f;
	float qWidth = gridWidth * 0.25f;
	float qDepth = gridDepth * 0.25f;
	return {
		(vec.x - qWidth) * padding,
		(vec.y - qHeight) * padding,
		(vec.z - qDepth) * padding
	};
}

void PathNav::checkAdd(const glm::vec3 &node, const glm::vec3 &gridDest, Cell &cell) {
	for (Cell &cl : closed) {
		if (cl.pos == node) {
			return;
		}
	}

	for (Cell &op : open) {
		if (op.pos == node) {
			float h = manhattanDist(node, gridDest);
			float g = cell.g + 1;

			if (h + g < op.getScore()) {
				op.h = h;
				op.g = g;
				op.parent = &cell;
			}
			return;
		}
	}

	bool occupied = pathGrid[(int) node.x][(int) node.y][(int) node.z];
	if (occupied) {
		if (node == gridDest) {
			open.push_back(generateCell(node, gridDest, &cell));
		}
		// else do nothing because it's occupied
	}
	else {
		open.push_back(generateCell(node, gridDest, &cell));
	}
}

float PathNav::manhattanDist(const glm::vec3 &from, const glm::vec3 &to) {
	return abs(from.x - to.x) + abs(from.y - to.y) + abs(from.z - to.z);
}

void PathNav::findAdjacentCells(Cell &cell, const glm::vec3 &gridDest) {
	DBG_ASSERT(cell.pos.x >= 0 && cell.pos.y >= 0 && cell.pos.z >= 0);

	if (cell.pos.x < gridWidth - 1) {
		glm::vec3 node = cell.pos;
		node.x++;
		checkAdd(node, gridDest, cell);
	}
	if (cell.pos.x > 0 && cell.pos.x < gridWidth) {
		glm::vec3 node = cell.pos;
		node.x--;
		checkAdd(node, gridDest, cell);
	}

	if (cell.pos.y < gridHeight - 1) {
		glm::vec3 node = cell.pos;
		node.y++;
		checkAdd(node, gridDest, cell);
	}
	if (cell.pos.y > 0 && cell.pos.y < gridHeight) {
		glm::vec3 node = cell.pos;
		node.y--;
		checkAdd(node, gridDest, cell);
	}

	if (cell.pos.z < gridDepth - 1) {
		glm::vec3 node = cell.pos;
		node.z++;
		checkAdd(node, gridDest, cell);
	}
	if (cell.pos.z > 0 && cell.pos.z < gridDepth) {
		glm::vec3 node = cell.pos;
		node.z--;
		checkAdd(node, gridDest, cell);
	}
}

PathNav::Cell PathNav::generateCell(const glm::vec3 &gridPos, const glm::vec3 &gridDest, Cell *parent) {
	float g = 1;
	if (parent) {
		g += parent->g;
	}
	float h = manhattanDist(gridPos, gridDest);
	return Cell{ parent, gridPos, g, h };
}

bool PathNav::Cell::operator==(const Cell &other) {
	return h == other.h && g == other.g && parent == other.parent && pos == other.pos;
}