#pragma once
#include "UiWidget.h"
#include "UiRenderer.h"
#include "KeyEvent.h"

class UiInputHandler {
public:
	virtual ~UiInputHandler() = default;
	virtual bool onMousePress() const { return false; };
	virtual bool onMouseRelease() const { return false; };
	virtual bool onMouseDragStart() const { return false; };
	virtual bool onMouseDragEnd() const { return false; };
protected:
	UiInputHandler() = default;
};

class UiRoot {
public:
	struct DragBox {
		// Top-left of box
		glm::vec2 tl, tr, br, bl;
	} dragBox;

	UiRoot(unsigned int screenWidth, unsigned int screenHeight);
	~UiRoot();

	// Clears list of Widgets
	// Does not delete or free
	void clear();

	// Caller must manage Widget lifetimes
	void addWidget(UiWidget *widget);

	void rawKeyInput(int key, int scancode, int action, int mods);
	void charInput(unsigned int codepoint);
	void mbInput(int button, int action, int mods);

	void processInput();
	void processInput(const UiInputHandler &inputHandler);
	void update(float deltaTime);
	void draw();

	glm::vec2 getMousePos();
	glm::vec2 getScreenSize();
	DragBox getDragBox() const;

	glm::vec4 focusColor = { 38.0f/255, 116.0f/255, 180.0f/255, 1.0f }; // #2674B4
private:
	void updateDragBox();

	unsigned int wDPI = 0, hDPI = 0, screenWidth = 0, screenHeight = 0;

	// State should be consumed and set to Inactive after triggering an event
	enum LmbState {
		INACTIVE,
		PRESSED,
		RELEASED
	} lmbState = INACTIVE;
	

	bool dragging = false;
	glm::vec2 dragStart{};
	glm::vec2 dragEnd{};

	UiWidget *fWidget = nullptr;
	UiRenderer &uiRenderer;

	// A list of events. 
	std::queue<std::shared_ptr<KeyEvent>> keyEvents;
	std::wstring_convert<std::codecvt_utf8<uint32_t>, uint32_t> charConverter;

	std::vector<UiWidget*> widgets;
};