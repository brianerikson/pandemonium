#pragma once
#include "GeometryCommon.h"
#include "Transform.h"
#include "Material.h"
#include "BoundingBox.h"

class Mesh {
public:
	const BoundingBox bBox;
    std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<Material> materials;
    GLuint vao, vbo, ebo;
	Transform transform;

    Mesh(std::vector<Vertex> &vertices, std::vector<GLuint> &indices, std::vector<Material> &materials);
    virtual ~Mesh();
};

