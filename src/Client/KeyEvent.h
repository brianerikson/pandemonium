#pragma once

class KeyEvent {
public:
	enum Action {
		CHAR_ADDED,
		BACKSPACE,
		ENTER,
		MOUSE
	};
	Action action;

	virtual ~KeyEvent() = default;
protected:
	KeyEvent(Action action) : action(action) {}
};

class BackspaceEvent : public KeyEvent {
public:
	BackspaceEvent() : KeyEvent(BACKSPACE) {}
};

class EnterEvent : public KeyEvent {
public:
	EnterEvent() : KeyEvent(ENTER) {}
};

class CharAddedEvent : public KeyEvent {
public:
	std::string ch;
	CharAddedEvent(std::string ch) : KeyEvent(CHAR_ADDED), ch(ch) {};
};

class MouseEvent : public KeyEvent {
public:
	enum Trigger {
		LMB_PRESSED,
		LMB_RELEASED,
		MMB_PRESSED,
		MMB_RELEASED,
		RMB_PRESSED,
		RMB_RELEASED
	} trigger;
	MouseEvent(Trigger trigger) 
		: KeyEvent(MOUSE), trigger(trigger) {};
};