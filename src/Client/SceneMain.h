#pragma once
#include "Scene.h"
#include "UiWindow.h"
#include "UiContainer.h"
#include "UiLabel.h"
#include "UiTextBox.h"

class SceneMain : public Scene {
public:
	SceneMain(GContext &gameCtx);
	virtual ~SceneMain();

	virtual void update() override;
	virtual void render() override;
	virtual void keyCallback(int key, int scancode, int action, int mods) override;
	virtual void mbCallback(int button, int action, int mods) override;
	virtual void mPosCallback(double x, double y) override;
	virtual void processInputEvents(double deltaTime) override;

private:
	UiWindow window;
	HBox windowContainer;
	UiLabel label;
	UiTextBox textBox;
};