#include "stdafx.h"
#include "Transform.h"
#include "GeometryCommon.h"

using namespace glm;

Transform::Transform() {
    matrix = mat4(1.0f);
    update();
}

Transform::Transform(vec3 pos, vec3 scale, mat4 rotation) {
    this->position = pos;
    this->scale = scale;
	this->rotation = glm::quat_cast(rotation);
    update();
}

Transform::Transform(aiMatrix4x4 matrix) {
	aiVector3D t, s;
	aiQuaternion r;
	matrix.Decompose(s, r, t);
	auto rm = r.GetMatrix();
	mat4 rotator = mat4(
		rm.a1, rm.a2, rm.a3, 0.0,
		rm.b1, rm.b2, rm.b3, 0.0,
		rm.c1, rm.c2, rm.c3, 0.0,
		0.0, 0.0, 0.0, 1.0
	);
	this->setPosition(vec3(t.x, t.y, t.z));
	this->setScale(vec3(s.x, s.y, s.z));
	this->setRotation(rotator);
	this->update();
}


Transform::~Transform() {
}

void Transform::setPosition(vec3 pos) {
    this->position = pos;
	needsUpdate = true;
}

void Transform::setScale(vec3 scale) {
	this->scale = scale;
	needsUpdate = true;
}

void Transform::setRotation(const glm::mat4 &rot) {
	this->rotation = glm::quat_cast(rot);
	needsUpdate = true;
}

void Transform::setRotation(const glm::quat &rot) {
	this->rotation = rot;
	needsUpdate = true;
}

void Transform::setRotation(float radX, float radY, float radZ) {
	rotation = glm::quat({ radX, radY, radZ });
	needsUpdate = true;
}

void Transform::setRotation(float angleDeg, glm::vec3 axis) {
	rotation = glm::rotate(glm::quat(), angleDeg, axis);
	needsUpdate = true;
}

void Transform::translate(vec3 dist) {
	this->position += dist;
	needsUpdate = true;
}

void Transform::rotate(glm::quat rot) {
	rotation = rotation * rot;
	needsUpdate = true;
}

void Transform::rotate(float radX, float radY, float radZ) {
	rotation = rotation * glm::quat({ radX, radY, radZ });
	needsUpdate = true;
}

void Transform::rotate(float angleDeg, glm::vec3 axis) {
	rotation = glm::rotate(rotation, angleDeg, axis);
	needsUpdate = true;
}

const vec3& Transform::getPosition() const {
    return position;
}

const vec3& Transform::getScale() const {
    return scale;
}

const glm::quat& Transform::getRotation() const {
	return rotation;
}

void Transform::lookAt(Transform &transform) {
	setRotation(glm::lookAt(this->getPosition(), transform.getPosition(), vec3(0, 1, 0)));
}

bool Transform::update() {
	if (needsUpdate) {
		matrix = glm::translate(mat4(1.0f), position) * glm::toMat4(rotation) * glm::scale(mat4(1.0f), scale);
		needsUpdate = false;
		return true;
	}
	return false;
}

glm::vec3 Transform::up() {
	return rotation * Direction::UP;
}

glm::vec3 Transform::forward() {
	return rotation * Direction::FORWARD;
}

glm::vec3 Transform::right() {
	return rotation * Direction::RIGHT;
}

Netcom::Transform Transform::serialize() {
	return Netcom::Transform(position, rotation);
}

void Transform::deserialize(Netcom::Transform t) {
	setPosition(t.position);
	setRotation(t.rotation);
}
