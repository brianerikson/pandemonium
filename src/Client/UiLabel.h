#pragma once
#include "UiWidget.h"
#include "Font.h"

class UiLabel : public UiWidget {
public:
	// Will load requested font+pt into memory if it doesn't exist
	UiLabel(EFont font, int pt, std::string text);
	UiLabel();
	virtual void draw(UiRenderer *renderer) override;
	virtual UiWidget* delegateFocus() override;
	virtual void resizeToContent() override;

	std::string getText();
	virtual void setText(std::string text);

	int pt = 12;
	glm::vec4 color = { 235.0f / 255, 236.0f / 255, 237.0f / 255, 1.0f }; // #EBECED
	EFont font = EFont::ROBOTO_REGULAR;

protected:
	// constrains the content size to the actual bounding box of the bitmap text string
	void pack();

	std::string text = "NULL";
};