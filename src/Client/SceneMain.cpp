#include "stdafx.h"
#include "SceneMain.h"

using namespace std;

SceneMain::SceneMain(GContext &gameCtx) : Scene(gameCtx) {
	// GUI
	glm::vec2 screenSize = uiRoot->getScreenSize();

	// Window dimensions must be configured before setting the container
	window.dimensions.content = { screenSize.x / 2, screenSize.y / 2 };
	window.dimensions.padding.left = 5.0f;
	window.dimensions.padding.top = 5.0f;
	window.dimensions.padding.right = 5.0f;
	window.dimensions.padding.bottom = 5.0f;
	window.setPosition(screenSize.x / 2, screenSize.y / 2);

	// Container must be set before adding children to the container
	window.setContainer(&windowContainer);

	label.pt = 16;
	label.setText("Server IP Address:");
	windowContainer.addChild(&label);

	textBox.setText("127.0.0.1");
	textBox.setWidthToChars(15);
	textBox.onSubmit = [=](std::string str) { 
		cout << "Submitted:" << str << endl; 
		client->requestConnect(str.c_str(), SERVER_UDP_PORT);
	};
	windowContainer.addChild(&textBox);

	windowContainer.pack();
	window.resizeToContent();

	uiRoot->addWidget(&window);
}

SceneMain::~SceneMain() {

}

void SceneMain::update() {
	uiRoot->processInput();
}

void SceneMain::render() {
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	uiRoot->draw();
}

void SceneMain::keyCallback(int key, int scancode, int action, int mods) {

}

void SceneMain::mbCallback(int button, int action, int mods) {

}

void SceneMain::mPosCallback(double x, double y) {

}

void SceneMain::processInputEvents(double deltaTime) {

}
