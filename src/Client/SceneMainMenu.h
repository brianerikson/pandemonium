#pragma once
#include "Scene.h"
#include "UiWindow.h"
#include "UiContainer.h"
#include "UiLabel.h"
#include "UiTextBox.h"
#include "UiButton.h"

class JoinWindow : public UiWindow {
public:
	JoinWindow();

	HBox hbox;
	UiLabel serverLabel;
	UiTextBox serverTextBox;
	UiButton connectButton;
};

class ChoiceWindow : public UiWindow {
public:
	ChoiceWindow();

	HBox hbox;
	UiLabel gameLabel;
	UiButton joinButton;
	UiButton hostButton;
};

class NameWindow : public UiWindow {
public:
	NameWindow();

	HBox hbox;
	UiLabel nameLabel;
	UiTextBox nameTextBox;
	UiButton okButton;
};

class PlayerNumWindow : public UiWindow {
public:
	PlayerNumWindow();

	HBox hbox;
	UiLabel playerNumLabel;
	UiTextBox numTextBox;
	UiButton okButton;
};

class SceneMainMenu : public Scene {
public:
	SceneMainMenu(GContext &gameCtx);
	virtual ~SceneMainMenu();

	virtual void update(float deltaTime) override;
	virtual void render() override;
	virtual void keyCallback(int key, int scancode, int action, int mods) override;
	virtual void mbCallback(int button, int action, int mods) override;
	virtual void mPosCallback(double x, double y) override;
	virtual void processInputEvents(double deltaTime) override;
	std::string getPlayerName();

	PlayerNumWindow playerNumWindow;
private:
	JoinWindow joinWindow;
	NameWindow nameWindow;
	ChoiceWindow choiceWindow;
};