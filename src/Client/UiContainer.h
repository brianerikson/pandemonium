#pragma once
#include "UiWidget.h"
#include "UiRenderer.h"

enum LayoutType {
	HBOX,
	VBOX
};

class UiContainer : public UiWidget {
public:
	virtual bool addChild(UiWidget *child) = 0;
	virtual void clear() = 0;
	virtual void draw(UiRenderer *renderer) override;
	virtual UiWidget* delegateFocus() override = 0;

	LayoutType type;

protected:
	UiContainer(LayoutType type);
};

class HBox : public UiContainer {
public:


	// Caller must manage Widget lifetimes
	HBox(std::initializer_list<UiWidget*> widgets);
	HBox();
	~HBox();

	// This will invalidate the structure contents. Call pack() to re-format.
	// if pack is not called, re-packing will be done on the next draw call.
	// Caller must manage Widget lifetimes
	bool addChild(UiWidget *child) override;

	// Reconfigures spacing of children 
	// Use after changing or setting the dimensions of this widget
	void pack();

	void clear() override;

	virtual void draw(UiRenderer *renderer) override;
	virtual UiWidget* delegateFocus() override;
	virtual void setPosition(float x, float y) override;
	virtual void resizeToContent() override;
	virtual void propagateVisibilty(bool visible) override;

private:
	bool isValid = false;
	std::vector<UiWidget*> children;
};