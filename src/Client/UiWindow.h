#pragma once
#include "UiWidget.h"
#include "UiContainer.h"

class UiWindow : public UiWidget {
public:
	UiWindow();
	virtual void draw(UiRenderer *renderer) override; 
	virtual UiWidget* delegateFocus() override;
	virtual void resizeToContent() override;
	virtual void propagateVisibilty(bool visible) override;

	void setContainer(UiContainer *container);

	//! Renders the window visible
	void restore();

	//! Renders the window invisible
	void minimize();

	virtual void setPosition(float x, float y) override;

	glm::vec4 color = { 56.0f / 255, 68.0f / 255, 76.0f / 255, 1.0f }; // #3B444C

protected:

	// TODO: Find a better way to add widgets to the window's container without
	//       having a reference
	UiContainer *container = nullptr;
};