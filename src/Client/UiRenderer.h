#pragma once
#include "ShaderProgram.h"
#include "Font.h"

// assumes a 2D left-hand coordinate system
class UVQuad {
public:
	constexpr static GLfloat DATA[]{
		-1.0f, -1.0f, 0.0f,		0.0f, 0.0f, // bl
		1.0f, -1.0f, 0.0f,		1.0f, 0.0f, // br
		1.0f, 1.0f, 0.0f,		1.0f, 1.0f, // tr
		-1.0f, 1.0f, 0.0f,		0.0f, 1.0f  // tl
	};

	constexpr static GLuint INDICES[]{
		0, 1, 3, 2, 3, 1
	};

	UVQuad();

	void set(float x, float y, float width, float height);
	void flipUV();
	void reset();

	GLfloat rectData[20];
};

class UiRenderer : public ShaderProgram {
public:
	static UiRenderer& Instance() {
		static UiRenderer ur;
		return ur;
	}

	UiRenderer(UiRenderer const&) = delete;
	void operator=(UiRenderer const&) = delete;

	// Initial UiRenderer Setup. Subsequent calls will be ignored.
	// Should be initalized right after GLFW setup completed
	void initialize(unsigned int screenWidth, unsigned int screenHeight);

	void start() override;
	// Primitives
	void drawBox(float x, float y, float width, float height, glm::vec4 color);
	void drawText(EFont font, int pt, float x, float y, glm::vec4 color, const char *fmt, ...);
	void drawLine(glm::vec2 start, glm::vec2 end, float thickness, glm::vec4 color);
	void drawBorder(float x, float y, float width, float height, float thickness, glm::vec4 color);
	//TODO void drawImage();

	void end() override;
private:
	UiRenderer();

	// SHADER DEFAULTS (Reset to default after usage is done!)
	// hasTex = false
	// isText = false
	GLint hasTexLoc, isTextLoc, texLoc, colorLoc, coordLoc, projLoc;

	GLuint vao, vbo, ebo, lvao, lvbo;
	GLfloat lineData[4] = {};
	UVQuad quad; // Used for rendering each box or line
	bool initialized = false;
};