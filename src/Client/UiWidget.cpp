#include "stdafx.h"
#include "UiWidget.h"

UiWidget::UiWidget(Type widgetType) : type(widgetType), visible(true) {}
UiWidget::~UiWidget() = default;

void UiWidget::update(float deltaTime) {}
void UiWidget::processInput(std::queue<std::shared_ptr<KeyEvent>> &keyEvents) {}

extern GLFWwindow *window;

void UiWidget::propagateVisibilty(bool visible) {
	this->visible = visible;
}

void UiWidget::resizeToContent() {}

glm::vec2 UiWidget::getPosition() {
	return pos;
}

void UiWidget::setPosition(float x, float y) {
	pos = { x, y };
}

void UiWidget::setCenter(float x, float y) {
	float hw = dimensions.content.width / 2.0f;
	float hh = dimensions.content.height / 2.0f;
	this->setPosition(x - hw, y - hh);
}

float UiWidget::getWidth() {
	auto &margins = dimensions.margin;

	return getInnerWidth() + margins.left + margins.right;
}

float UiWidget::getHeight() {
	auto &margins = dimensions.margin;

	return getInnerHeight() + margins.top + margins.bottom;
}

float UiWidget::getInnerWidth() {
	auto &padding = dimensions.padding;
	auto &content = dimensions.content;

	return content.width + padding.left + padding.right;
}

float UiWidget::getInnerHeight() {
	auto &padding = dimensions.padding;
	auto &content = dimensions.content;

	return content.height + padding.top + padding.bottom;
}

bool UiWidget::containsMouse() {
	double xp, yp;
	glfwGetCursorPos(window, &xp, &yp);
	float x = abs(xp);
	float y = abs(yp);

	float padXL = pos.x - dimensions.padding.left;
	float padYT = pos.y - dimensions.padding.top;
	if (x > padXL && y > padYT) {
		float padXR = pos.x + dimensions.padding.right + dimensions.content.width;
		float padYB = pos.y + dimensions.padding.bottom + dimensions.content.height;
		if (x < padXR && y < padYB) {
			return true;
		}
	}

	return false;
}

bool UiWidget::isVisibile() {
	return visible;
}

void UiWidget::setVisible(bool visibility) {
	this->visible = visibility;
	this->propagateVisibilty(visibility);
}
