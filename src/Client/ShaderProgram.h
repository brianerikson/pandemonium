#pragma once

class ShaderProgram {
public:
    const GLuint &id = programId;
    virtual ~ShaderProgram();

    virtual void start();
    virtual void end();
protected:
    ShaderProgram(const char *vertPath, const char *fragPath);
    bool defaultsLoaded, running;
    GLuint programId;
};

