#pragma once
#include "Actor.h"
#include "NetCommon/Spaceship.h"

class MovementAction {
public:
	enum Type {
		FOLLOW_PATH,
		GO_TO,
		STOP_AND_ORIENT
	} const type;
	virtual ~MovementAction() = default;

	//! Perform action incrementally
	// Return val is whether or not this action has completed.
	virtual bool update(const float &deltaTime) = 0;

	//! Hook Actor into the movement instance. Actor will need to have physics enabled.
	virtual void setActor(Actor *actor);
protected:
	MovementAction(MovementAction::Type type) : type(type) {}

	// http://www.red3d.com/cwr/steer/gdc99/
	void seek(const Vector3 &pos, const float &deltaTime);

	//! Similar to classic lookAt, but obeys a maximum turning speed
	// Returns true if no more turning is needed 
	bool orientAt(const glm::vec3 &dir, const glm::vec3 &up, const float &deltaTime);
	void lookAt(const glm::vec3 &dir);

	//! Casts a ray in dir + curVelocity 
	// returns the normal if hit within maxSpeed range, zero vec otherwise
	glm::vec3 avoidCollision(const Vector3 &dir);

	Actor *actor = nullptr;
};

class FollowPathAction : public MovementAction {
public:
	FollowPathAction(const std::list<Vector3> &path);
	virtual bool update(const float &deltaTime) override;
	std::list<Vector3> spine;
private:
	float tubeRadius;
};

class GoToAction : public MovementAction {
public:
	GoToAction(const Vector3 &pos, const float &slowDist) 
		: MovementAction(MovementAction::GO_TO), pos(pos), slowDist(slowDist) {}
	virtual bool update(const float &deltaTime) override;

	Vector3 pos;
private:
	float slowDist;
};

class StopAndOrientAction : public MovementAction {
public:
	StopAndOrientAction(const Vector3 &orientDir) 
	: MovementAction(MovementAction::STOP_AND_ORIENT), orientDir(orientDir) {}
	virtual bool update(const float &deltaTime) override;
private:
	glm::vec3 orientDir;
};

class Spaceship : public Actor {
public:
	Spaceship(World *world, Netcom::Spaceship *ship, bool locallyOwned);
	Spaceship(World *world, const SLNet::RakNetGUID &owner, bool locallyOwned);
	virtual ~Spaceship();

	void clearMovementActions();
	void addMovementAction(MovementAction *action);
	const MovementAction* getCurrentAction() const;

	virtual void update(const float &deltaTime) override;

	float hull = 100;
	float shield = 100;
private:
	std::list<MovementAction*> actions;

	//=======================
	// Arrival
	// Distance at which arrive() slows the ship down for arrival 
	//const float slowDist = maxSpeed*2.3f;
	// Distance away from target to be considered "arrived"
	//const float stopDist = 2.f;
	//float turnSpeed = 1.f; // between 0 and 1
	//======================
};