#pragma once
#include "Scene.h"
#include "Actor.h"
#include "Light.h"
#include "FPSCamera.h"
#include "DefaultShader.h"
#include "Transform.h"
#include "Skybox.h"
#include "NetCommon/net_common.h"
#include "Spaceship.h"
#include "GameRenderer.h"

class GameUiInputHandler : public UiInputHandler {
public:
	GameUiInputHandler(const SLNet::RakNetGUID &owner, UiRoot *uiRoot, FPSCamera *camera, World *gameWorld);
	virtual bool onMousePress() const override;
	virtual bool onMouseDragEnd() const override;

	SLNet::RakNetGUID localPlayerGuid;
private:
	UiRoot * uiRoot;
	FPSCamera *camera;
	World *gameWorld;
};

extern GLFWwindow *window;
class SceneWorld : public Scene {
public:
    std::vector<std::unique_ptr<Light>> lights;
	FPSCamera camera;

	SceneWorld(GContext &gameCtx, const SLNet::RakNetGUID &localPlayer);
    virtual ~SceneWorld();

	void spawnActor(Netcom::GameObject *object);
	void applyServerTick(const std::vector<Netcom::UdtObjectPart*> &updates);
    virtual void update(float deltaTime) override;
    virtual void render() override;
    virtual void keyCallback(int key, int scancode, int action, int mods) override;
	virtual void mbCallback(int button, int action, int mods) override;
	virtual void mPosCallback(double x, double y) override;
	virtual void processInputEvents(double deltaTime) override;

	World *gameWorld;
private:
    const char *importPath;
	std::map<ShaderType, GameShader*> shaders;
	glm::vec4 gravityWellColor = { 177.f / 255, 180.f / 255, 174.f / 255, 0.5f }; // #B1B4AE
	glm::vec4 ownBorderColor = { 38.0f / 255, 116.0f / 255, 180.0f / 255, 1.0f }; // #2674B4

	// managed by gameWorld
	Skybox *skybox;

	GameRenderer gameRenderer;

	FMOD::Studio::System *fmod;
	FMOD::Studio::Bank *weaponSndBank;
	FMOD_3D_ATTRIBUTES fmodCam;

	GameUiInputHandler inputHandler;
	SLNet::RakNetGUID localPlayerGuid;
};

