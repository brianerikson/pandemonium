#pragma once
#include "GameObject.h"

class Light : public GameObject {
public:
    struct Color {
		glm::vec3 ambient;
		glm::vec3 diffuse;
		glm::vec3 specular;
    };
    enum LightType { DIRECTIONAL, POINT, SPOT };

	// UNUSED
	// SEE DefaultShader.cpp for usage
    float angleInnerCone = 0;
    float angleOuterCone = 0;
    float attenuationConstant = 0;
    float attenuationLinear = 0;
    float attenuationQuadratic = 0;
	//

	// Only diffuse is hooked up atm
	Color color = { {}, {252.0f/255, 212.0f/255, 64.0f/255}, {} }; // #FCD440

	// Doesn't matter atm
    LightType type = POINT;

    Light(const SLNet::RakNetGUID &owner);
    Light(aiLight *light, aiMatrix4x4 &transform, const SLNet::RakNetGUID &owner);
    virtual ~Light() = default;
};

