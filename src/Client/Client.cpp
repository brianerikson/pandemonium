#include "stdafx.h"
#include "Client.h"

using namespace SLNet;
using namespace std;
using namespace Netcom;

Client::Client() {
	client = RakPeerInterface::GetInstance();
	StartupResult startupResult;
	if ((startupResult = client->Startup(1, &SocketDescriptor(), 1)) != 0) {
		printf("StartupResult: %i\n", startupResult);
	}
}

Client::~Client() {
	RakPeerInterface::DestroyInstance(client);
}

void Client::requestConnect(const char *ip, int port, const std::string &name) {
	ConnectionAttemptResult caResult;
	if ((caResult = client->Connect(ip, port, 0, 0)) != 0) {
		printf("ConnectionAttemptResult: %i", caResult);
	}
	clientName = name.substr(0, MAX_PLAYER_NAME_LEN);
}

vector<Payload*>& Client::poll() {
	using Type = Payload::Type;

	Packet* packet;
	for (packet = client->Receive(); packet; client->DeallocatePacket(packet), packet = client->Receive()) {
		switch (GetPacketIdentifier(packet)) {
		case ID_CONNECTION_REQUEST_ACCEPTED:
		{
			printf("Our connection request has been accepted.\n");
			endPoint = packet->systemAddress;
			connected = true;
			Netcom::CPL_JoinRequest playerInfo = Netcom::CPL_JoinRequest(clientName);
			SLNet::BitStream stream;
			playerInfo.serialize(stream);
			send(&stream, HIGH_PRIORITY, RELIABLE_ORDERED, 0);
			break;
		}
		case ID_NEW_INCOMING_CONNECTION:
			printf("A connection is incoming.\n");
			break;
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			printf("The server is full.\n");
			connected = false;
			break;
		case ID_DISCONNECTION_NOTIFICATION:
			printf("We have been disconnected.\n");
			connected = false;
			break;
		case ID_CONNECTION_LOST:
			printf("Connection lost.\n");
			connected = false;
			break;
		case ID_CONNECTION_ATTEMPT_FAILED:
			printf("Connection attempt failed.\n");
			connected = false;
			break;
		case Type::SVR_JOIN_RESPONSE:
		{
			printf("Joined server.\n");
			SPL_JoinResponse *jr = new SPL_JoinResponse;
			BitStream stream(packet->data, packet->length, true);
			jr->deserialize(stream);
			stash.push_back((Payload*) jr);
			break;
		}
		case Type::SVR_LOBBY_STATE:
		{
			printf("Received lobby update.\n");
			SPL_LobbyState *lobby = new SPL_LobbyState;
			BitStream stream(packet->data, packet->length, true);
			lobby->deserialize(stream);
			stash.push_back((Payload*) lobby);
			break;
		}
		case Type::SVR_LEVEL_LOAD:
		{
			printf("Loading level.\n");
			SPL_LevelLoad *level = new SPL_LevelLoad;
			BitStream stream(packet->data, packet->length, true);
			level->deserialize(stream);
			stash.push_back((Payload*) level);
			break;
		}
		case Type::SVR_AUTHORITY_LEVEL:
		{
			SPL_AuthorityLevel *authLevel = new SPL_AuthorityLevel;
			BitStream stream(packet->data, packet->length, true);
			authLevel->deserialize(stream);
			stash.push_back((Payload*) authLevel);
			printf("Received authority level of %i", authLevel->level);
			break;
		}
		case Type::SVR_SPAWN_RESPONSE:
		{
			SPL_SpawnResponse *spawnRes = new SPL_SpawnResponse;
			BitStream stream(packet->data, packet->length, true);
			spawnRes->deserialize(stream);
			stash.push_back((Payload*) spawnRes);
			break;
		}
		case Type::SVR_STATE_TICK:
		{
			SPL_StateTick * state = new SPL_StateTick(nullptr);
			BitStream stream(packet->data, packet->length, true);
			state->deserialize(stream);
			stash.push_back((Payload*) state);
			break;
		}
		default:
			printf("Message with identifier %i has arrived.\n", packet->data[0]);
			break;
		}
	}

	return stash;
}

void Client::send(const SLNet::BitStream *stream, PacketPriority priority, 
					PacketReliability reliability, char orderingChannel) {
	// TODO: Handle disconnection gracefully
	if (!connected) { throw std::runtime_error("Cannot send packet. Not connected to endpoint."); }
	client->Send(stream, priority, reliability, orderingChannel, endPoint, false);
}
