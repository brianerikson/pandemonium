#pragma once
#include "Model.h"
#include "Scene.h"
#include "ServerHandle.h"

// FMOD Error checking
static FMOD_RESULT FMOD_CHECK(FMOD_RESULT result, const char *errStr) {
	if (result != FMOD_OK) {
		printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
		throw std::runtime_error(errStr);
	}
	return result;
}


class ManagerGame {
public:

	static ManagerGame& Instance() {
		static ManagerGame mg;
		return mg;
	}

	~ManagerGame();

	void initialize(unsigned int screenWidth, unsigned int screenHeight);
	std::string getLocalPlayerName();

	void process(float deltaTime);

	void keyCallback(int key, int scancode, int action, int mods);
	void mbCallback(int button, int action, int mods);
	void mPosCallback(double x, double y);
	void charCallback(unsigned int codepoint);

	ManagerGame(ManagerGame const&) = delete;
	void operator=(ManagerGame const&) = delete;

	FMOD::Studio::System *fmod = nullptr;
	FMOD::Studio::Bank *fmodMasterBank;

	std::string localPlayerName;
	SLNet::RakNetGUID localPlayerGuid;
	GContext gameCtx;
private:
	ManagerGame();
	void processNetwork();
	void loadLobby(Netcom::SPL_LobbyState *state);

	Scene *scene = nullptr;

	//! Used by hosting functionality to manage local server state
	ServerHandle serverHandle;

	//! The current authority level over the server
	//  Variable is accurate once user enters the lobby
	Netcom::SPL_AuthorityLevel::Level serverAuthLevel = Netcom::SPL_AuthorityLevel::PLAYER;

};