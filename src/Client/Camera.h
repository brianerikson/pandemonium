#pragma once
#include "GameObject.h"
#include "Transform.h"
#include "GeometryCommon.h"
#include "InputController.h"
#include "MathInterop.h"

class Camera {
public:
    float aspect;
    float clipPlaneNear = 0.01f;
    float clipPlaneFar = 10000.0f;
    float fov = 75.0f;
	glm::mat4 projection = glm::mat4(1.0f);
	std::unique_ptr<InputController> inputController;

    virtual ~Camera();

    void updateFrustum();
    void virtual update() = 0;

    void virtual translate(float x, float y, float z) = 0;
    void virtual setPosition(float x, float y, float z) = 0;
    Vector3 virtual getPosition() = 0;

    void virtual rotate(float pitch, float roll, float yaw) = 0;
    void virtual rotate(float angle, glm::vec3 dir) = 0;
    void virtual setRotation(float pitch, float roll, float yaw) = 0;
    void virtual setRotation(float angle, glm::vec3 dir) = 0;
	glm::mat4 virtual getOrientation() = 0;

	//! Returns euler angles, pitch as x, yaw as y, roll as z.
	// The result is expressed in degrees.
	Vector3 virtual getRotation() = 0;

	glm::vec3 virtual forward() = 0;
	glm::vec3 virtual up() = 0;
	glm::vec3 virtual right() = 0;
	Matrix4x4 virtual view() const = 0;
	Vector3 virtual unproject(const glm::vec2 &screenPos, bool farPlane) const = 0;

protected:
    Camera(float aspect, float clipNear, float clipFar, float fov);
    Camera(GLFWwindow *window, InputController *ic);
    Camera(aiCamera *camera, InputController *ic);
};

