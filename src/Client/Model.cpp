#include "stdafx.h"
#include "Model.h"
#include "GeometryCommon.h"
#include "FileUtils.h"

using namespace std;
using namespace glm;

Model::Model(const char *modelPath, Netcom::GoType type) : type(type) {
    Assimp::Importer importer;
    const aiScene *scene = importer.ReadFile(modelPath, aiProcess_FlipWindingOrder | aiProcess_Triangulate | aiProcess_FlipUVs);
    if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
        throw runtime_error(importer.GetErrorString());
    }

    parse(modelPath, scene->mRootNode, scene);
	bBox = BoundingBox(meshes);
}

Model::Model(const char *folder, aiNode *root, const aiScene *scene, Netcom::GoType type) : type(type) {
	parse(folder, root, scene);
	bBox = BoundingBox(meshes);
}


Model::~Model() {
}

void Model::parse(const char * folder, aiNode * root, const aiScene * scene) {
	this->directory = string(folder);
	this->directory = this->directory.substr(0, this->directory.find_last_of('/'));

	this->processNode(root, scene);
	this->matrix = root->mTransformation;
}

void Model::processNode(aiNode * node, const aiScene * scene) {
    for (unsigned int i = 0; i < node->mNumMeshes; i++) {
        aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
		Mesh newMesh = this->processMesh(mesh, scene);
		newMesh.transform = Transform(node->mTransformation);
		newMesh.transform.update();
        this->meshes.push_back(newMesh);
    }

    for (unsigned int i = 0; i < node->mNumChildren; i++) {
        this->processNode(node->mChildren[i], scene);
    }
}

Mesh Model::processMesh(aiMesh * mesh, const aiScene * scene) {
    vector<Vertex> vertices;
    vector<GLuint> indices;
	Material material;

    // Vertices
    for (unsigned int i = 0; i < mesh->mNumVertices; i++) {
        Vertex vertex = Vertex{
            vec3(
                mesh->mVertices[i].x,
                mesh->mVertices[i].y,
                mesh->mVertices[i].z
            ),
            vec3(),
            vec2()
        };

        if (mesh->mNormals != nullptr) {
            vertex.normal.x = mesh->mNormals[i].x;
            vertex.normal.y = mesh->mNormals[i].y;
            vertex.normal.z = mesh->mNormals[i].z;
        }
        
        if (mesh->mTextureCoords != nullptr) {
            vertex.texCoords.x = mesh->mTextureCoords[0][i].x;
            vertex.texCoords.y = mesh->mTextureCoords[0][i].y;
        }

        vertices.push_back(vertex);
    }

    // Indices
    for (unsigned int i = 0; i < mesh->mNumFaces; i++) {
        aiFace face = mesh->mFaces[i];
        for (unsigned int j = 0; j < face.mNumIndices; j++) {
            indices.push_back(face.mIndices[j]);
        }
    }

    // Texture(s)
    if (mesh->mMaterialIndex >= 0) {
        aiMaterial *aiMaterial = scene->mMaterials[mesh->mMaterialIndex];
        
        vector<Texture> diffuseMaps = this->loadMaterialTextures(aiMaterial, aiTextureType_DIFFUSE, "texture_diffuse");
		if (!diffuseMaps.empty())
			material.diffuse = diffuseMaps[0];

        vector<Texture> specularMaps = this->loadMaterialTextures(aiMaterial, aiTextureType_SPECULAR, "texture_specular");
		if (!specularMaps.empty())
			material.specular = specularMaps[0];

		aiColor3D color;
		aiMaterial->Get(AI_MATKEY_COLOR_DIFFUSE, color);
		material.diffuse_color = vec3 {color.r, color.g, color.b};
    }

	return Mesh(vertices, indices, vector<Material> {material});
}

vector<Texture> Model::loadMaterialTextures(aiMaterial * mat, aiTextureType type, const char * typeName) {
    vector<Texture> textures;
    for (unsigned int i = 0; i < mat->GetTextureCount(type); i++) {
        aiString str;
        mat->GetTexture(type, i, &str);

        bool skip = false;
        for (auto &loaded_texture : loaded_textures) {
            if (loaded_texture.path == str) {
                textures.push_back(loaded_texture);
                skip = true;
                break;
            }
        }

        if (skip) {
            continue;
        }

        Texture texture;
        cout << "Texture path is " << (this->directory + "/" + str.C_Str()).c_str() << endl;
        texture.id = FileUtils::textureFromFile((this->directory + "/" + str.C_Str()).c_str());
        texture.type = typeName;
        texture.path = str;
        textures.push_back(texture);
        loaded_textures.push_back(texture);
    }
    
    return textures;
}