#pragma once
#include "Actor.h"

class Skybox : public Actor {
public:
	Skybox(World *gameWorld, const SLNet::RakNetGUID &owner);
	virtual ~Skybox() = default;
};