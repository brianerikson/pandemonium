#include "stdafx.h"
#include "SceneWorld.h"
#include "Model.h"
#include "FPSCamera.h"
#include "Actor.h"
#include "Planet.h"
#include "ManagerGame.h"
#include "Formation.h"
#include "NetCommon/net_common.h"

using namespace std;

const aiString MODEL_ROOT = aiString("Models");

SceneWorld::SceneWorld(GContext &gameCtx, const SLNet::RakNetGUID &localPlayer)
	: Scene(gameCtx, GAME), camera(), gameWorld(new World({ 0,0,0 }, &camera)), 
	  inputHandler(localPlayer, uiRoot, &camera, gameWorld), localPlayerGuid(localPlayer)  {
	fmodCam.velocity = {0,0,0};
	this->shaders.emplace(ShaderType::DEFAULT, (GameShader*)new DefaultShader());

	fmod = ManagerGame::Instance().fmod;
	FMOD_CHECK(fmod->loadBankFile("res/sound/Weapons.bank",
		FMOD_STUDIO_LOAD_BANK_NORMAL, &weaponSndBank), "Could not load FMOD bank file.");
	
	skybox = new Skybox(gameWorld, localPlayerGuid);
	gameWorld->addActor(skybox);

	lights.push_back(std::make_unique<Light>(Light(localPlayerGuid)));
}


SceneWorld::~SceneWorld() {
	weaponSndBank->unload();

	for (auto pair : shaders) { delete pair.second; }
	delete gameWorld;
}

void SceneWorld::spawnActor(Netcom::GameObject *object) {
	bool locallyOwned = (object->owner == localPlayerGuid);
	switch (object->type) {
	case Netcom::GoType::SPACESHIP:
		gameWorld->addActor(new Spaceship(gameWorld, (Netcom::Spaceship*)object, locallyOwned));
		break;
	case Netcom::GoType::PLANET:
		gameWorld->addActor(new Planet(gameWorld, (Netcom::Planet*)object));
		break;
	default:
		break;
	}
}

void SceneWorld::applyServerTick(const std::vector<Netcom::UdtObjectPart*> &updates) {
	using UdtType = Netcom::UdtObjectPart::Type;
	for (auto update : updates) {
		Actor *actor = gameWorld->getActor(update->gameObjectGuid);
		DBG_ASSERT(actor);
		switch (update->type) {
		case UdtType::TRANSFORM:
		{
			// TODO: The rigidbody and the mesh aren't travelling together 
			// when directly set, necessitating the adjustment of both
			// But when using force on the rigidbody, the mesh follows
			Netcom::UdtTransform *t = (Netcom::UdtTransform*) update;
			if (actor->physics) {
				btTransform &bt = actor->physics->rigidBody->getWorldTransform();
				bt.setOrigin((Vector3) t->transform.position);
				bt.setRotation((Quat) t->transform.rotation);
			}
			actor->transform.setPosition(t->transform.position);
			actor->transform.setRotation(t->transform.rotation);
			break;
		}
		case UdtType::SPACESHIP_ATTRS:
		{
			Netcom::UdtSpaceshipAttrs *attrs = (Netcom::UdtSpaceshipAttrs*) update;
			Spaceship *ship = (Spaceship*) actor;
			ship->hull = attrs->hull;
			ship->shield = attrs->shield;
			break;
		}
		default:
			break;
		}
	}
}

void SceneWorld::update(float deltaTime) {
	uiRoot->processInput(inputHandler);
	uiRoot->update(deltaTime);

	gameWorld->update(deltaTime);
	gameWorld->stepSimulation(deltaTime);

	camera.update();

    for (auto &light : lights) {
        light->update(deltaTime);
    }

	glm::vec3 up = camera.up();
	glm::vec3 forward = camera.forward();
	fmodCam.forward = { forward.x, forward.y, forward.z };
	fmodCam.up = { up.x, up.y, up.z };
	fmodCam.position = { camera.position.x, camera.position.y, camera.position.z };
	// TODO fmodCam.velocity = 

	fmod->setListenerAttributes(0, &fmodCam);
}

void SceneWorld::render() {
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // render all actors
	GameShader *lastShader = nullptr;
	auto &actors = gameWorld->getActors();
    for (const Actor *actor : actors) {
        GameShader *gameShader = nullptr;
		auto search = shaders.find(actor->shaderType);
		if (search != shaders.end()) {
			gameShader = search->second;
		}
		else {
			// TODO: handle other shaders
			gameShader = shaders[ShaderType::DEFAULT];
			if (gameShader == nullptr) {
				throw exception("Default shader and requested shader for actor is not available.");
			}
		}

		if (lastShader != gameShader) {
			if (lastShader) { lastShader->end(); }
			lastShader = gameShader;
			gameShader->start();
		}

		gameShader->update(lights[0].get(), &camera);

		if (actor->type == Netcom::GoType::SKYBOX) {
			glDisable(GL_DEPTH_TEST);
			gameShader->render(actor);
			glEnable(GL_DEPTH_TEST);
		}
		else { gameShader->render(actor); };
        
    }
	lastShader->end();

#ifdef G_PHYS_DBG
	gameWorld->debugDraw();
#endif

	gameRenderer.start();
	gameRenderer.update(&camera);

	for (auto &actor : actors) {
		if (actor->type != Netcom::GoType::PLANET) { continue; }
		Planet *planet = (Planet*) actor;
		auto size = planet->model->bBox.getSize();
		gameRenderer.drawCircle(planet->transform.getPosition(), size.x * 1.f, gravityWellColor);
		gameRenderer.drawCircle(planet->transform.getPosition(), size.x * 2.5f, gravityWellColor);
	}

	for (const Actor *actor : gameWorld->getSelectedActors()) {
		if (actor->type == Netcom::GoType::SPACESHIP) {
			auto ship = (Spaceship*) actor;
			const MovementAction *movementAction = ship->getCurrentAction();
			if (movementAction) {
				switch (movementAction->type) {
				case MovementAction::Type::FOLLOW_PATH:
				{
					FollowPathAction *followPath = (FollowPathAction*) movementAction;
					Vector3 pos = ship->transform.getPosition();
					Vector3 *lastPoint = &pos;
					for (Vector3 &point : followPath->spine) {
						gameRenderer.drawLine(*lastPoint, point, gravityWellColor * 1.2f);
						lastPoint = &point;
					}
					break;
				}
				case MovementAction::Type::GO_TO:
				{
					Vector3 endPos = ((const GoToAction*) movementAction)->pos;
					gameRenderer.drawLine(ship->transform.getPosition(), endPos, gravityWellColor * 1.2f);
					break;
				}
				default:
					break;
				}
			}
		}

		if (actor->owner == localPlayerGuid) {
			gameRenderer.drawBoundingBox(actor, ownBorderColor);
		}
		else { gameRenderer.drawBoundingBox(actor, gravityWellColor); }
	}
	gameRenderer.end();

	uiRoot->draw();
}

bool actorSort(Actor *actor, Actor *bActor) {
	return (actor->shaderType < bActor->shaderType);
}

void SceneWorld::keyCallback(int key, int scancode, int action, int mods) {
    // TODO: Add more robust input composition.
    camera.inputController->inputCallback(key, scancode, action, mods);
}

void SceneWorld::mbCallback(int button, int action, int mods) {
	if (action == GLFW_PRESS && button == GLFW_MOUSE_BUTTON_RIGHT) {
		FMOD::Studio::EventDescription *event = nullptr;

		FMOD_CHECK(fmod->getEvent("event:/Ships/Large/cannon", &event), 
			"FMOD error: cannot load event:/Ships/Large/cannon");
		FMOD::Studio::EventInstance *cannonSnd = nullptr;
		FMOD_CHECK(event->createInstance(&cannonSnd), "FMOD error: cannon sound couldn't be created.");
		cannonSnd->set3DAttributes(&fmodCam);
		cannonSnd->start();
		cannonSnd->release();

		// Select point on plane
		glm::vec2 mPos = uiRoot->getMousePos();
		Vector3 nearPos = camera.unproject(mPos, false);
		Vector3 farPos = camera.unproject(mPos, true);
		btCollisionWorld::ClosestRayResultCallback rc(nearPos, farPos);
		rc.m_collisionFilterGroup = World::ColMask::RAY;
		rc.m_collisionFilterMask = World::ColMask::PLANE;
		gameWorld->dynamicsWorld.rayTest(nearPos, farPos, rc);

		// send spawn request
		if (rc.hasHit()) {
			Netcom::CPL_SpawnRequest spawnRequest = Netcom::CPL_SpawnRequest(
				Netcom::GoType::SPACESHIP,
				(Vector3)rc.m_hitPointWorld
			);
			SLNet::BitStream os;
			spawnRequest.serialize(os);
			client->send(&os, PacketPriority::HIGH_PRIORITY, PacketReliability::RELIABLE, 0);
		}

	}
}

void SceneWorld::mPosCallback(double x, double y) {

}

void SceneWorld::processInputEvents(double deltaTime) {
    // TODO: Add more robust input composition.
    camera.inputController->update(deltaTime);
	skybox->transform.setPosition(camera.getPosition());
}

GameUiInputHandler::GameUiInputHandler(const SLNet::RakNetGUID &owner, UiRoot *uiRoot, FPSCamera *camera, World *gameWorld)
	: localPlayerGuid(owner), uiRoot(uiRoot), camera(camera), gameWorld(gameWorld){}

bool GameUiInputHandler::onMousePress() const {
	// Detect if player is clicking on a unit
	glm::vec2 mPos = uiRoot->getMousePos();
	Vector3 nearPos = camera->unproject(mPos, false);
	Vector3 farPos = camera->unproject(mPos, true);
	btCollisionWorld::ClosestRayResultCallback rc(nearPos, farPos);

#ifdef G_PHYS_DBG
	gameWorld->debugRenderer.addRay(nearPos, farPos, 500);
#endif

	rc.m_collisionFilterGroup = World::ColMask::RAY;
	rc.m_collisionFilterMask = World::ColMask::ALL & ~World::ColMask::PLANE;
	gameWorld->dynamicsWorld.rayTest(nearPos, farPos, rc);
	if (rc.hasHit()) {
		PhysUserData *data = (PhysUserData*) rc.m_collisionObject->getUserPointer();
		Actor *actor = data->actor;
#ifdef G_PHYS_DBG
		auto hitPos = actor->transform.getPosition();
		std::printf("Hit at pos %f, %f, %f\n", hitPos.x, hitPos.y, hitPos.z);
#endif

		gameWorld->setSelectedActors(std::vector<Actor*>(1, actor));
		return true;
	}

	// can't control a planet or unowned objects (only one can be selected at once
	// since you can't volume select them either)
	auto &actors = gameWorld->getSelectedActors();
	if (actors.size() == 1 && (actors[0]->type == Netcom::GoType::PLANET ||
		actors[0]->owner != localPlayerGuid)) {
		return false;
	}

	if (!actors.empty()) {
		rc.m_collisionFilterMask = World::ColMask::PLANE;
		gameWorld->dynamicsWorld.rayTest(nearPos, farPos, rc);
		if (rc.hasHit() && !actors.empty()) {
			// navigate ships to point
			Vector3 point = rc.m_hitPointWorld;
			// TODO: this assumes that what selected is spaceships. Bad Brian
			Formation formation = Formation(actors);
			for (auto &actor : actors) {
				Spaceship *ship = (Spaceship*) actor;
				auto path = formation.getPath(
					ship->guid, ship->transform.getPosition(), point
				);
				ship->clearMovementActions();
				ship->addMovementAction(new FollowPathAction(path));
				ship->addMovementAction(new GoToAction(path.back(), 5.f));
				ship->addMovementAction(new StopAndOrientAction(Direction::FORWARD));
			}

			return true;
		}
	}
	return false;
}


struct SelVolumeCallback : public btCollisionWorld::ContactResultCallback {
public:
	std::vector<Actor*> actors;

	virtual btScalar addSingleResult(btManifoldPoint& cp,
		const btCollisionObjectWrapper* colObj0Wrap, int partId0, int index0,
		const btCollisionObjectWrapper* colObj1Wrap, int partId1, int index1) override {

		PhysUserData *dataA = (PhysUserData*) colObj0Wrap->m_collisionObject->getUserPointer();
		PhysUserData *dataB = (PhysUserData*) colObj1Wrap->m_collisionObject->getUserPointer();

		if (dataA->type == Netcom::GoType::VOLUME_SELECTION && dataB->type == Netcom::GoType::ACTOR) {
			actors.push_back(dataB->actor);
		}
		else if (dataB->type == Netcom::GoType::VOLUME_SELECTION && dataA->type == Netcom::GoType::ACTOR) {
			actors.push_back(dataA->actor);
		}

		return 0;
	}

};
bool GameUiInputHandler::onMouseDragEnd() const {
	Vector3 vecs[5];
	Camera *camera = gameWorld->getCamera();
	UiRoot::DragBox dragBox = uiRoot->getDragBox();
	glm::vec2 screenSize = uiRoot->getScreenSize();
	vecs[0] = camera->unproject({ screenSize.x / 2.f, screenSize.y / 2.f }, false);
	vecs[1] = camera->unproject(dragBox.tl, true);
	vecs[2] = camera->unproject(dragBox.bl, true);
	vecs[3] = camera->unproject(dragBox.br, true);
	vecs[4] = camera->unproject(dragBox.tr, true);

	// pyramid shape
	btConvexHullShape projShape = btConvexHullShape();
	for (Vector3 &vec : vecs) { projShape.addPoint(vec); }
	projShape.optimizeConvexHull();
	projShape.initializePolyhedralFeatures();

	btPairCachingGhostObject volume = btPairCachingGhostObject();
	volume.setUserPointer(new PhysUserData{ Netcom::GoType::VOLUME_SELECTION, nullptr });
	volume.setCollisionShape(&projShape);
	volume.setCollisionFlags(volume.getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);

	SelVolumeCallback sv;
	gameWorld->dynamicsWorld.contactTest(&volume, sv);

	sv.actors.erase(std::remove_if(sv.actors.begin(), sv.actors.end(), [&](Actor *actor) {
		return actor->type == Netcom::GoType::PLANET || actor->owner != localPlayerGuid;
	}), sv.actors.end());
#ifdef G_PHYS_DBG
	std::printf("Hit %i actors with volume selection\n", sv.actors.size());
#endif
	gameWorld->setSelectedActors(sv.actors);
	return true;
}
