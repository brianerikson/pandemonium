#include "stdafx.h"
#include "SceneLobby.h"
#include "ManagerGame.h"
#include "NetCommon/net_common.h"

using namespace Netcom;

SceneLobby::SceneLobby(unsigned int maxPlayers, std::vector<SPL_LobbyState::Player> &players, GContext &gameCtx)
	: Scene(gameCtx, LOBBY) {
	// GUI
	glm::vec2 screenSize = uiRoot->getScreenSize();

	// Window dimensions must be configured before setting the container
	window.dimensions.content = { screenSize.x / 2, screenSize.y / 2 };
	window.dimensions.padding.left = 5.0f;
	window.dimensions.padding.top = 5.0f;
	window.dimensions.padding.right = 5.0f;
	window.dimensions.padding.bottom = 5.0f;
	window.setContainer(&hbox);


	// creation of labels could be more effecient but we'll cross that bridge ltr
	for (unsigned int i = 0; i < maxPlayers; i++) {
		UiLabel *label = new UiLabel(ROBOTO_REGULAR, 14, "Not Connected");
		label->color = textColor;
		// TODO: Padding seems to be ignored with labels on the left and right sides
		label->dimensions.padding = { 5, 5, 5, 5 };
		playerLabels.push_back(label);
		hbox.addChild(label);
	}
	updateLobby(maxPlayers, players);

	readyButton.setText("Ready");
	readyButton.onPress = [=]() {
		CPL_ReadyState rdyState;
		SLNet::BitStream stream;
		isReady = !isReady;
		rdyState.ready = isReady;
		rdyState.serialize(stream);
		gameCtx.net->send(&stream, PacketPriority::HIGH_PRIORITY, PacketReliability::RELIABLE, 0);
	};

	hbox.addChild(&readyButton);
	hbox.pack();

	window.setCenter(screenSize.x / 2, screenSize.y / 2);
	uiRoot->addWidget(&window);

	// Request admin controls (determines client authority level)
	SLNet::BitStream stream;
	CPL_RequestAuthLevel reqAuthLevel;
	reqAuthLevel.serialize(stream);
	gameCtx.net->send(&stream, PacketPriority::LOW_PRIORITY, PacketReliability::RELIABLE, 0);
}

SceneLobby::~SceneLobby() {
	for (UiLabel *label : playerLabels) { delete label; }
}

void SceneLobby::updateLobby(unsigned int maxPlayers, std::vector<SPL_LobbyState::Player> &players) {
	ManagerGame &gm = ManagerGame::Instance();
	for (unsigned int i = 0; i < players.size(); i++) {
		UiLabel *label = playerLabels[i];
		auto &player = players[i];
		label->setText(player.name);

		if (player.ready) { label->color = readyTextColor; }
		else { label->color = textColor; }

		if (label->getText() == gm.getLocalPlayerName()) { lpLabel = label; }
	}
}

void SceneLobby::update(float deltaTime) {
	uiRoot->processInput();
	uiRoot->update(deltaTime);
}

void SceneLobby::render() {
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	uiRoot->draw();
}

void SceneLobby::enableAdminControls() {
	launchButton.setText("Launch");
	launchButton.onPress = [=]() {
		Netcom::CPL_LaunchGame launchReq;
		SLNet::BitStream stream;
		launchReq.serialize(stream);
		client->send(&stream, PacketPriority::HIGH_PRIORITY, PacketReliability::RELIABLE, 0);
	};
	hbox.addChild(&launchButton);

#ifdef TEST_LAUNCH
	launchButton.onPress();
#endif
}

void SceneLobby::keyCallback(int key, int scancode, int action, int mods) {

}

void SceneLobby::mbCallback(int button, int action, int mods) {

}

void SceneLobby::mPosCallback(double x, double y) {

}

void SceneLobby::processInputEvents(double deltaTime) {

}
