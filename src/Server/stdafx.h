// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include <stdio.h>
#include <signal.h>
#include <random>
#include <climits>
#include <ctime>
#include <unordered_map>
#include <vector>
#define GLM_ENABLE_EXPERIMENTAL
#include "glm/common.hpp"
#include "glm/gtx/quaternion.hpp"
#include "slikenet/PeerInterface.h"
#include "slikenet/Types.h"
#include "slikenet/MessageIdentifiers.h"
#include "slikenet/BitStream.h"
#include "Guid.hpp"

#ifdef G_DBG
#define DBG_ASSERT(x) assert(x)
#define DBG_PRINTF(x) std::printf(x)
#endif
