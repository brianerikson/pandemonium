#pragma once
#include "NetCommon/net_common.h"

struct Player {
	//! Used for identifying reconnecting players
	//! Not valid for re-establishing a connection. No port is logged. 
	SLNet::SystemAddress addr{};
	SLNet::RakNetGUID guid{};
	std::string name = "";
	bool connected = false;
	bool ready = false;
	Netcom::SPL_AuthorityLevel::Level authorityLevel;
};

enum SessionState {
	LOBBY,
	GAME
};

class SessionManager {
public:
	static SessionManager& Instance() {
		static SessionManager ms;
		return ms;
	}
	SessionManager(SessionManager const&) = delete;
	void operator=(SessionManager const&) = delete;

	void onDisconnect(SLNet::RakNetGUID guid);
	void process(SLNet::Packet *packet);
	void update();

	int maxNumPlayers;
private:
	SessionManager();
	~SessionManager();
	void addPlayer(Player player);

	// Will manage lifetime
	void addGameObject(Netcom::GameObject *gameObject);

	//! Find active player given their SystemAddress (OR/AND) their GUID
	//  The GUID takes precedence in the matching process
	Player* getPlayer(SLNet::SystemAddress *addr, SLNet::RakNetGUID *guid);

	//! Broadcast lobby state to all players in lobby
	void notifyLobby();
	void serializeLobby(SLNet::BitStream &os);

	void launchGame();

	SLNet::RakNetGUID serverGuid;
	std::vector<Player> players;
	SessionState sState;

	//! synchronized version of actors for faster access to a specific element
	std::unordered_map<xg::Guid, Netcom::GameObject*> gameObjMap;

	//! main actor array, optimized for iteration
	std::vector<Netcom::GameObject*> gameObjs;

	//! Contains all the changes since last tick
	std::list<Netcom::UdtObjectPart*> tickChanges;
};