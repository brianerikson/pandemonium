#pragma once
#include "BitStream.h"

class DataObject {
public:
	DataObject() = default;
	virtual ~DataObject() = default;

	virtual void serialize(SLNet::BitStream &stream) = 0;
};