#include "stdafx.h"
#include "SessionManager.h"
#include "NetCommon/Spaceship.h"

using namespace SLNet;
using namespace Netcom;
extern RakPeerInterface* server;

void SessionManager::onDisconnect(SLNet::RakNetGUID guid) {
	for (Player player : players) {
		if (player.guid == guid) {
			player.connected = false;
		}
	}

	// TODO: Cull players that don't reconnect within a certain amt of seconds *when in lobby*
}

void SessionManager::process(SLNet::Packet *packet) {
	using Type = Payload::Type;
	switch (GetPacketIdentifier(packet)) {
	case Type::CLNT_JOIN_REQUEST:
	{
		if (sState != LOBBY || players.size() > maxNumPlayers) {
			// TODO: Handle reconnections for a running game
			server->CloseConnection(packet->guid, true);
			break;
		}
		Netcom::CPL_JoinRequest playerInfo;
		BitStream stream(packet->data, packet->length, true);
		playerInfo.deserialize(stream);
		Player player = {
			packet->systemAddress.ToString(false),
			packet->guid,
			playerInfo.playerName,
			true, 
			SPL_AuthorityLevel::Level::PLAYER
		};

		addPlayer(player);

		stream.Reset();
		SPL_JoinResponse jr;
		jr.playerId = packet->guid;
		jr.serialize(stream);
		std::printf("Recieved Join request. Sending response...\n");
		server->Send(&stream, PacketPriority::IMMEDIATE_PRIORITY, 
			PacketReliability::RELIABLE_ORDERED, 0, packet->guid, false);

		// Notify all in the lobby of the newcomer, including the newcomer
		// If newcomer is not in lobby, client should move to lobby.
		notifyLobby();
		break;
	}
	case Type::CLNT_LOBBY_RDY_STATE:
	{
		if (sState != LOBBY) {
			std::printf("A player readied up but the server isn't in the lobby!\n");
			break;
		}
		CPL_ReadyState rdyState;
		BitStream stream(packet->data, packet->length, true);
		rdyState.deserialize(stream);

		// Safe to assume player is in vector, as each player is added during connection
		int readied = 0;
		for (Player &player : players) {
			if (player.guid == packet->guid) {
				player.ready = rdyState.ready;
				if (player.ready) {
					std::printf("%s readied up.\n", player.name.c_str());
				}
				else {
					std::printf("%s unreadied.\n", player.name.c_str());
				}
			}

			if (player.ready) { readied++; }
		}

		notifyLobby();
		break;
	}
	case Type::CLNT_REQUEST_AUTH_LEVEL:
	{
		Player *player = getPlayer(&packet->systemAddress, &packet->guid);
		if (!player) { 
			printf("A player requested their authority level, but the client isn't a registered player!\n"); 
			break;
		}
		SPL_AuthorityLevel authLevel;
		authLevel.level = player->authorityLevel;
		BitStream stream;
		authLevel.serialize(stream);
		server->Send(&stream, PacketPriority::LOW_PRIORITY, 
			PacketReliability::RELIABLE, 0, packet->guid, false);
		break;
	}
	case Type::CLNT_LAUNCH_GAME: 
	{
		Player * player = getPlayer(&packet->systemAddress, &packet->guid);
		if (!player) {
			printf("A player requested to launch the game, but the client isn't a registered player!\n");
			break;
		}
		else if (player->authorityLevel != SPL_AuthorityLevel::ADMIN) {
			printf("Player %s tried to launch the game, but isn't an admin!\n", player->name.c_str());
			break;
		}

		launchGame();
		break;
	}
	case Type::CLNT_SPAWN_REQUEST:
	{
		CPL_SpawnRequest spawnReq;
		BitStream stream(packet->data, packet->length, true);
		spawnReq.deserialize(stream);
		stream.Reset();
		// TODO confirm player can spawn GameObject based on player attributes and location

		GameObject *gameObj = nullptr;
		switch (spawnReq.type) {
		case GoType::SPACESHIP:
		{
			Spaceship *ship = new Spaceship(packet->guid);
			ship->guid = xg::newGuid();
			ship->transform.position = spawnReq.pos;
			gameObj = ship;
			break;
		}
		default:
			break;
		}

		if (gameObj) {
			printf("Spawning GameObject for %s\n", getPlayer(&packet->systemAddress, &packet->guid)->name.c_str());
			addGameObject(gameObj);
			SPL_SpawnResponse spawnRes = SPL_SpawnResponse(gameObj);
			spawnRes.serialize(stream);
			server->Send(&stream, 
				PacketPriority::HIGH_PRIORITY, PacketReliability::RELIABLE,
				0,
				UNASSIGNED_SYSTEM_ADDRESS,
				true);
		}
		break;
	}
	case Type::CLNT_GAME_OBJ_UPDATE:
	{
		CPL_GameObjectUpdate update;
		BitStream stream(packet->data, packet->length, true);
		update.deserialize(stream);

		tickChanges.push_back(update.part);
		GameObject *obj = gameObjMap[update.part->gameObjectGuid];
		
		DBG_ASSERT(obj != nullptr);
		switch (update.part->type) {
		case UdtObjectPart::Type::TRANSFORM:
		{
			UdtTransform *t = (UdtTransform*) update.part;
			obj->transform = t->transform;
			break;
		}
		case UdtObjectPart::Type::SPACESHIP_ATTRS:
		{
			UdtSpaceshipAttrs *attrs = (UdtSpaceshipAttrs*) update.part;
			Spaceship *ship = (Spaceship*) obj;
			ship->hull = attrs->hull;
			ship->shield = attrs->shield;
			break;
		}
		default:
			throw std::runtime_error("Requested part was not handled");
			break;
		}
		break;
	}
	default:
		printf("Message with identifier %i has arrived.\n", packet->data[0]);
	}
}

void SessionManager::update() {
	// Send out unowned GameObjects that have been updated recently to each player
	if (!tickChanges.empty()) {
		std::unordered_map<uint64_t, size_t> playerUpdates;
		for (auto &part : tickChanges) {
			playerUpdates[gameObjMap[part->gameObjectGuid]->owner.g]++;
		}

		std::vector<UdtObjectPart*> parts;
		SLNet::BitStream stream;
		for (Player &player : players) {
			size_t num = 0;
			for (Player &other : players) {
				if (player.guid == other.guid) { continue; }
				num += playerUpdates[other.guid.g];
			}
			if (num == 0) { continue; }
			parts.reserve(num);

			// iterate through tickChanges and build array with parts that the player doesn't own
			for (auto &change : tickChanges) {
				if (gameObjMap[change->gameObjectGuid]->owner == player.guid) {
					continue;
				}
				parts.push_back(change);
			}

			// send that to the player
			Netcom::SPL_StateTick state(&parts);
			state.serialize(stream);
			server->Send(&stream, PacketPriority::MEDIUM_PRIORITY,
				PacketReliability::UNRELIABLE_SEQUENCED, 0, player.guid, false);

			parts.clear();
			stream.Reset();
		}

		for (auto &change : tickChanges) {
			delete change;
		}
		tickChanges.clear();
	}
}

SessionManager::SessionManager() : sState(LOBBY), 
	serverGuid(server->GetGuidFromSystemAddress(UNASSIGNED_SYSTEM_ADDRESS)) {}

SessionManager::~SessionManager() {
	gameObjMap.clear();
	for (GameObject *obj : gameObjs) {
		delete obj;
	}
	gameObjs.clear();
}

void SessionManager::addPlayer(Player player) {
	if (player.addr.IsLoopback()) {
		// assume loopback player is admin
		player.authorityLevel = SPL_AuthorityLevel::Level::ADMIN;
		printf("Authority connected.\n");
	}

	bool found = false;
	// TODO: Find a way to allow players to reconnect
	// This way doesn't work if making two connections from the same IP
	// Can't use ports because they are randomized each time
// 	for (Player &p : players) {
// 		if (p.addr == player.addr.) {
// 			p.name = player.name;
// 			p.connected = true;
// 			p.guid = player.guid;
// 			found = true;
// 			printf("%s has reconnected to the server.\n", p.name.c_str());
// 			break;
// 		}
// 	}

	if (!found) {
		// add new player
		players.push_back(player);
		printf("%s has joined the server.\n", player.name.c_str());
	}

}

void SessionManager::addGameObject(Netcom::GameObject *gameObject) {
	gameObjs.push_back(gameObject);
	gameObjMap[gameObject->guid] = gameObject;
}

Player* SessionManager::getPlayer(SLNet::SystemAddress *addr, SLNet::RakNetGUID *guid) {
	for (Player &player : players) {
		if (guid && player.guid == *guid) {
			return &player;
		}
		else if (addr && player.addr == *addr) {
			return &player;
		}
	}

	return nullptr;
}

void SessionManager::notifyLobby() {
	BitStream stream;
	serializeLobby(stream);
	server->Send(
		&stream,
		PacketPriority::HIGH_PRIORITY,
		PacketReliability::RELIABLE_ORDERED,
		0,
		UNASSIGNED_SYSTEM_ADDRESS,
		true
	);
}

void SessionManager::serializeLobby(SLNet::BitStream &os) {
	SPL_LobbyState lobbyState = SPL_LobbyState(maxNumPlayers, (int16_t)players.size());
	for (auto player : players) { lobbyState.players.push_back(SPL_LobbyState::Player{player.name, player.ready}); }

	lobbyState.serialize(os);
}

void SessionManager::launchGame() {
	SPL_LevelLoad level;
	level.system.planets.push_back(Planet(serverGuid ,glm::vec2(0, 500)));
	BitStream stream;
	level.serialize(stream);
	server->Send(
		&stream, PacketPriority::HIGH_PRIORITY, 
		PacketReliability::RELIABLE_ORDERED, 
		0, 
		UNASSIGNED_SYSTEM_ADDRESS, 
		true
	);
	sState = GAME;
	printf("Game launched.\n");
}
