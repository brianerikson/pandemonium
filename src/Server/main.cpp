#include "stdafx.h"
#include "NetCommon/net_common.h"
#include "SessionManager.h"
#include "anyoption.h"


using namespace SLNet;

RakPeerInterface* server;

void onShutdown(int signum) {
	if (signum == SIGTERM || signum == SIGBREAK) {
		RakPeerInterface::DestroyInstance(server);
		exit(0);
	}
}

int main(int argc, char* argv[]) {
	signal(SIGINT, onShutdown);
	signal(SIGBREAK, onShutdown);

	AnyOption *opt = new AnyOption();
	opt->setVerbose();
	opt->autoUsagePrint(true);

	opt->addUsage("");
	opt->addUsage("Usage: ");
	opt->addUsage("");
	opt->addUsage(" -h  --help  		Prints this help ");
	opt->addUsage(" -p  --players 4     Number of players that can join the server ");
	opt->addUsage("						NOTE: Cannot be greater than 12");
	opt->addUsage("");
	
	opt->setFlag("help", 'h');
	opt->setOption("players", 'p');

	opt->processCommandArgs(argc, argv);
	if (opt->getFlag("help") || opt->getFlag('h')) { 
		opt->printUsage(); 
		goto CMD_OPT_CLEANUP;
	}

	int numberOfPlayers = 2;
	if (opt->getValue('p') != nullptr || opt->getValue("players") != nullptr) {
		string num = opt->getValue('p');
		try { numberOfPlayers = stoi(num); }
		catch (exception e) { 
			opt->printUsage(); 
			goto CMD_OPT_CLEANUP;
		}

		if (numberOfPlayers < 1 || numberOfPlayers > MAX_PLAYERS) { 
			opt->printUsage(); 
			goto CMD_OPT_CLEANUP; 
		}
	}

	// Listen for connections
	server = RakPeerInterface::GetInstance();
	server->SetMaximumIncomingConnections(numberOfPlayers);
	printf("Starting server on %s:%i with %i player slots\n", "0.0.0.0", SERVER_UDP_PORT, numberOfPlayers);
	server->Startup(numberOfPlayers, &SocketDescriptor(SERVER_UDP_PORT, 0), 1);

	SessionManager *session = &SessionManager::Instance();
	session->maxNumPlayers = numberOfPlayers;

	while (true) {
		Packet *packet;
		for (packet = server->Receive(); packet; server->DeallocatePacket(packet), packet = server->Receive()) {
			switch (Netcom::GetPacketIdentifier(packet)) {
			case ID_REMOTE_DISCONNECTION_NOTIFICATION:
				session->onDisconnect(packet->guid);
				printf("Another client has gracefully disconnected.\n");
				break;
			case ID_REMOTE_CONNECTION_LOST:
				session->onDisconnect(packet->guid);
				printf("Another client has lost the connection.\n");
				break;
			case ID_REMOTE_NEW_INCOMING_CONNECTION:
				printf("Another client has connected.\n");
				break;
			case ID_NO_FREE_INCOMING_CONNECTIONS:
				printf("The server is full.\n");
				break;
			case ID_DISCONNECTION_NOTIFICATION:
				session->onDisconnect(packet->guid);
				printf("A client has disconnected.\n");
				break;
			case ID_CONNECTION_LOST:
				session->onDisconnect(packet->guid);
				printf("A client lost the connection.\n");
				break;
			case ID_CONNECTION_REQUEST_ACCEPTED:
				printf("Our connection request has been accepted.\n");
				break;
			case ID_NEW_INCOMING_CONNECTION:
				printf("A connection is incoming.\n");
				break;
			default:
				session->process(packet);
				break;
			}
		}

		session->update();

		Sleep(NETWORK_TICKRATE_MS);
	}

	RakPeerInterface::DestroyInstance(server);
CMD_OPT_CLEANUP:
	delete opt;
	return 0;
}