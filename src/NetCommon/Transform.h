#pragma once
#include "Serializable.h"

namespace Netcom {
	class Transform : public Serializable {
	public:
		Transform(const glm::vec3 &pos, const glm::quat &rot);
		Transform() = default;
		virtual ~Transform() = default;
		virtual SLNet::BitStream& serialize(SLNet::BitStream &os) override;
		virtual void deserialize(SLNet::BitStream &is) override;

		glm::vec3 position{ 0,0,0 };

		// format W, X, Y, Z for some reason
		glm::quat rotation{ 1,0,0,0 };
	};
}