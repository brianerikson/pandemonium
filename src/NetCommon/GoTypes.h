#pragma once

namespace Netcom {
	enum GoType {
		// Abstracts
		MODEL,
		ACTOR,
		VOLUME_SELECTION,
		XZ_PLANE,

		// Objects
		GAME_OBJECT,
		PLANET,
		SKYBOX,
		SPACESHIP
	};
}