#pragma once
#include "Serializable.h"
#include "GoTypes.h"
#include "Transform.h"

namespace Netcom {
	class GameObject : public Serializable {
	public:
		GameObject(GoType type)
			: type(type) {}
		GameObject(GoType type, const SLNet::RakNetGUID &owner)
			: type(type), owner(owner) {}

		SLNet::BitStream& serialize(SLNet::BitStream &os) override;
		void deserialize(SLNet::BitStream &is) override;

		virtual ~GameObject() = default;

		SLNet::RakNetGUID owner;

		Transform transform{};
		xg::Guid guid;
		GoType type;
	};
}