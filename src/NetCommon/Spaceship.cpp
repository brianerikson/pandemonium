#include "stdafx.h"
#include "Spaceship.h"

using namespace Netcom;

SLNet::BitStream& Spaceship::serialize(SLNet::BitStream &os) {
	GameObject::serialize(os);
	os.Write((char*) &hull, sizeof(float));
	os.Write((char*) &shield, sizeof(float));

	return os;
}

void Netcom::Spaceship::deserialize(SLNet::BitStream &is) {
	GameObject::deserialize(is);
	is.Read((char*) &hull, sizeof(float));
	is.Read((char*) &shield, sizeof(float));
}
