#pragma once
#include "./GameObject.h"

namespace Netcom {
	class Planet : public GameObject {
	public:
		Planet() : GameObject(PLANET) {};
		Planet(const SLNet::RakNetGUID &owner) : GameObject(PLANET, owner) {};
		Planet(const SLNet::RakNetGUID &owner, glm::vec2 pos);
		SLNet::BitStream& serialize(SLNet::BitStream &os) override;
		virtual void deserialize(SLNet::BitStream &is) override;

		unsigned short worth = 0;
	};
}