#include "stdafx.h"
#include "Transform.h"

using namespace Netcom;

SLNet::BitStream& Transform::serialize(SLNet::BitStream &os) {
	os.Write((char*) &position.x, sizeof(position.x));
	os.Write((char*) &position.y, sizeof(position.y));
	os.Write((char*) &position.z, sizeof(position.z));
	os.Write((char*) &rotation.x, sizeof(rotation.x));
	os.Write((char*) &rotation.y, sizeof(rotation.y));
	os.Write((char*) &rotation.z, sizeof(rotation.z));
	os.Write((char*) &rotation.w, sizeof(rotation.w));
	return os;
}

void Transform::deserialize(SLNet::BitStream &is) {
	is.Read((char*) &position.x, sizeof(position.x));
	is.Read((char*) &position.y, sizeof(position.y));
	is.Read((char*) &position.z, sizeof(position.z));
	is.Read((char*) &rotation.x, sizeof(rotation.x));
	is.Read((char*) &rotation.y, sizeof(rotation.y));
	is.Read((char*) &rotation.z, sizeof(rotation.z));
	is.Read((char*) &rotation.w, sizeof(rotation.w));
}

Transform::Transform(const glm::vec3 &pos, const glm::quat &rot)
	: position(pos), rotation(rot) {}
