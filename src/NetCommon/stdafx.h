// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#include <string>
#include <vector>
#include <random>
#include "glm/common.hpp"
#define GLM_ENABLE_EXPERIMENTAL
#include "glm/glm.hpp"
#include "glm/gtx/quaternion.hpp"
#include "slikenet/peerinterface.h"
#include "slikenet/types.h"
#include "slikenet/MessageIdentifiers.h"
#include "slikenet/BitStream.h"
#include "Guid.hpp"

// TODO: reference additional headers your program requires here
