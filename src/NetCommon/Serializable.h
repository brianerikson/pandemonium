#pragma once

namespace Netcom {
	class Serializable {
	public:
		virtual ~Serializable() = default;
	protected:
		Serializable() {}
		virtual SLNet::BitStream& serialize(SLNet::BitStream &os) = 0;
		virtual void deserialize(SLNet::BitStream &is) = 0;
	};
}