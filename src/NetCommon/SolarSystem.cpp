#include "stdafx.h"
#include "SolarSystem.h"



SLNet::BitStream& Netcom::SolarSystem::serialize(SLNet::BitStream &os) {
	size_t size = planets.size();
	os.Write((char*) &size, sizeof(size));
	for (auto &planet : planets) {
		planet.serialize(os);
	}
	return os;
}

void Netcom::SolarSystem::deserialize(SLNet::BitStream &is) {
	size_t planetNum;
	is.Read((char*) &planetNum, sizeof(planetNum));
	for (size_t i = 0; i < planetNum; i++) {
		Planet planet = Planet();
		planet.deserialize(is);
		planets.push_back(planet);
	}
}
