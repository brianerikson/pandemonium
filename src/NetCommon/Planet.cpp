#include "stdafx.h"
#include "Planet.h"

using namespace std;
using namespace glm;
using namespace SLNet;
using namespace Netcom;

Planet::Planet(const SLNet::RakNetGUID &owner, vec2 pos) : Planet(owner) {
	transform.position = {pos.x, 0.f, pos.y};

	std::random_device rd;
	std::mt19937 gen(rd());
	normal_distribution<float> distribution(0.5f, 0.25f); // MEAN, STD DEVIATION
	worth = (unsigned short)(distribution(gen) * USHRT_MAX);
}

SLNet::BitStream& Planet::serialize(SLNet::BitStream &os) {
	GameObject::serialize(os);
	os.Write((char*) &worth, sizeof(worth));
	return os;
}

void Planet::deserialize(SLNet::BitStream &is) {
	GameObject::deserialize(is);
	is.Read((char*) &worth, sizeof(worth));
}