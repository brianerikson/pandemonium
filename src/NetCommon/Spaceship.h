#pragma once
#include "./GameObject.h"

namespace Netcom {
	class Spaceship : public GameObject {
	public:
		Spaceship() : GameObject(SPACESHIP) {}
		Spaceship(const SLNet::RakNetGUID &owner) : GameObject(SPACESHIP, owner) {}
		SLNet::BitStream& serialize(SLNet::BitStream &os) override;
		virtual void deserialize(SLNet::BitStream &is) override;

		float hull;
		float shield;
	};
}