#include "stdafx.h"
#include "net_common.h"
#include "Spaceship.h"

using namespace Netcom;

//////////////////////////////////////////////////////////////////////////
// PAYLOADS
//////////////////////////////////////////////////////////////////////////

SLNet::BitStream& Payload::serialize(SLNet::BitStream &os) {
	os.Write((char*) &type, sizeof(Type));
	return os;
}

void Payload::deserialize(SLNet::BitStream &is) {
	is.Read((char*) &type, sizeof(Type));
}


//========================================================================
// SHARED

SLNet::BitStream& UdtObjectPart::serialize(SLNet::BitStream &os) {
	os.Write(((std::string)gameObjectGuid).c_str(), GUID_LEN);
	os.Write((char*)&type, sizeof(type));
	return os;
}

void UdtObjectPart::deserialize(SLNet::BitStream &is) {
	char id[GUID_LEN];
	is.Read(id, GUID_LEN);
	is.Read((char*) &type, sizeof(type));
	gameObjectGuid = xg::Guid(std::string(id));
}

SLNet::BitStream& Netcom::UdtTransform::serialize(SLNet::BitStream &os) {
	UdtObjectPart::serialize(os);
	transform.serialize(os);
	return os;
}

void Netcom::UdtTransform::deserialize(SLNet::BitStream &is) {
	UdtObjectPart::deserialize(is);
	transform.deserialize(is);
}

SLNet::BitStream& Netcom::UdtSpaceshipAttrs::serialize(SLNet::BitStream &os) {
	UdtObjectPart::serialize(os);
	os.Write((char*) &hull, sizeof(hull));
	os.Write((char*) &shield, sizeof(shield));
	return os;
}

void Netcom::UdtSpaceshipAttrs::deserialize(SLNet::BitStream &is) {
	UdtObjectPart::deserialize(is);
	is.Read((char*) &hull, sizeof(hull));
	is.Read((char*) &shield, sizeof(shield));
}

UdtObjectPart* deserializeInterpretUdtOP(SLNet::BitStream &is, UdtObjectPart &in) {
	auto offset = is.GetReadOffset();
	in.deserialize(is);
	is.SetReadOffset(offset);
	switch (in.type) {
	case UdtObjectPart::Type::TRANSFORM:
	{
		UdtTransform *t = (new UdtTransform());
		t->deserialize(is);
		return (UdtObjectPart*) t;
		break;
	}
	case UdtObjectPart::Type::SPACESHIP_ATTRS:
	{
		UdtSpaceshipAttrs *attr = new UdtSpaceshipAttrs();
		attr->deserialize(is);
		return (UdtObjectPart*) attr;
		break;
	}
	default:
		throw std::runtime_error("Could not deserialize requested type");
		break;
	}
}
//========================================================================

//========================================================================
// SERVER -> CLIENT

SLNet::BitStream& SPL_JoinResponse::serialize(SLNet::BitStream &os) {
	Payload::serialize(os);
	os.Write((char*) &playerId.g, sizeof(playerId.g));
	return os;
}

void SPL_JoinResponse::deserialize(SLNet::BitStream &is) {
	Payload::deserialize(is);
	unsigned long long id;
	is.Read((char*) &id, sizeof(id));

	playerId = SLNet::RakNetGUID(id);
}

SLNet::BitStream& SPL_LevelLoad::serialize(SLNet::BitStream &os) {
	Payload::serialize(os);
	system.serialize(os);
	return os;
}

void SPL_LevelLoad::deserialize(SLNet::BitStream &is) {
	Payload::deserialize(is);
	system.deserialize(is);
}

SLNet::BitStream& SPL_LobbyState::serialize(SLNet::BitStream &os) {
	Payload::serialize(os);
	os.Write((char*) &maxPlayers, sizeof(maxPlayers));
	os.Write((char*) &lobbySize, sizeof(lobbySize));
	for (Player player : players) {
		os.Write(
			player.name.substr(0, MAX_PLAYER_NAME_LEN).c_str(),
			MAX_PLAYER_NAME_LEN
		);
		os.Write((char*) &player.ready, sizeof(player.ready));
	}
	return os;
}

void SPL_LobbyState::deserialize(SLNet::BitStream &is) {
	Payload::deserialize(is);
	is.Read((char*) &maxPlayers, sizeof(int16_t));
	is.Read((char*) &lobbySize, sizeof(int16_t));
	for (int i = 0; i < lobbySize; i++) {
		Player player;
		char chs[MAX_PLAYER_NAME_LEN];
		is.Read(chs, MAX_PLAYER_NAME_LEN);
		player.name = std::string(chs);
		is.Read((char*)&player.ready, sizeof(player.ready));
		players.push_back(player);
	}
}

SLNet::BitStream& SPL_AuthorityLevel::serialize(SLNet::BitStream &os) {
	Payload::serialize(os);
	os.Write((char*) &level, sizeof(SPL_AuthorityLevel::Level));
	return os;
}

void SPL_AuthorityLevel::deserialize(SLNet::BitStream &is) {
	Payload::deserialize(is);
	is.Read((char*) &level, sizeof(SPL_AuthorityLevel::Level));
}

SLNet::BitStream& SPL_SpawnResponse::serialize(SLNet::BitStream &os) {
	Payload::serialize(os);
	os.Write((char*) &approved, sizeof(approved));
	if (approved) { gameObject->serialize(os); }
	return os;
}

void SPL_SpawnResponse::deserialize(SLNet::BitStream &is) {
	Payload::deserialize(is);
	is.Read((char*) &approved, sizeof(approved));
	if (approved) {
		auto offset = is.GetReadOffset();
		GameObject obj = GameObject(GoType::GAME_OBJECT);
		obj.deserialize(is);
		is.SetReadOffset(offset);
		switch (obj.type) {
		case GoType::SPACESHIP:
		{
			Spaceship *ship = new Spaceship();
			ship->deserialize(is);
			gameObject = ship;
			break;
		}
		default:
			throw std::runtime_error("Spawn response recieved but not handled");
			break;
		}
	}
}

SLNet::BitStream& SPL_StateTick::serialize(SLNet::BitStream &os) {
	Payload::serialize(os);
	size_t size = updates->size();
	os.Write((char*) &size, sizeof(size));
	for (UdtObjectPart *part : *updates) {
		part->serialize(os);
	}
	return os;
}

void SPL_StateTick::deserialize(SLNet::BitStream &is) {
	Payload::deserialize(is);
	size_t updateNum = 0;
	is.Read((char*) &updateNum, sizeof(updateNum));
	updates->reserve(updateNum);
	SLNet::BitSize_t offset = 0;
	for (size_t i = 0; i < updateNum; i++) {
		UdtObjectPart in;
		updates->push_back(deserializeInterpretUdtOP(is, in));
	}
}

Netcom::SPL_StateTick::~SPL_StateTick() {
	if (ownsUpdates) { 
		for (auto update : *updates) {
			delete update;
		}
		delete updates; 
	}
}

//========================================================================

//========================================================================
// CLIENT -> SERVER

SLNet::BitStream& CPL_ReadyState::serialize(SLNet::BitStream &os) {
	Payload::serialize(os);
	os.Write((char*) &ready, sizeof(ready));
	return os;
}

void CPL_ReadyState::deserialize(SLNet::BitStream &is) {
	Payload::deserialize(is);
	is.Read((char*) &ready, sizeof(ready));
}

SLNet::BitStream& CPL_SpawnRequest::serialize(SLNet::BitStream &os) {
	Payload::serialize(os);
	os.Write((char*) &type, sizeof(type));
	os.Write((char*) &pos.x, sizeof(pos.x));
	os.Write((char*) &pos.y, sizeof(pos.y));
	os.Write((char*) &pos.z, sizeof(pos.z));
	return os;
}

void CPL_SpawnRequest::deserialize(SLNet::BitStream &is) {
	Payload::deserialize(is);
	is.Read((char*) &type, sizeof(type));
	is.Read((char*) &pos.x, sizeof(pos.x));
	is.Read((char*) &pos.y, sizeof(pos.y));
	is.Read((char*) &pos.z, sizeof(pos.z));
}

SLNet::BitStream& Netcom::CPL_GameObjectUpdate::serialize(SLNet::BitStream &os) {
	Payload::serialize(os);
	part->serialize(os);
	return os;
}

void Netcom::CPL_GameObjectUpdate::deserialize(SLNet::BitStream &is) {
	Payload::deserialize(is);
	UdtObjectPart p;
	part = deserializeInterpretUdtOP(is, p);
}

CPL_JoinRequest::CPL_JoinRequest(std::string playerName) : Payload(Payload::CLNT_JOIN_REQUEST) {
	playerName = playerName.substr(0, MAX_PLAYER_NAME_LEN - 1);
	memcpy(this->playerName, playerName.c_str(), MAX_PLAYER_NAME_LEN);
}

SLNet::BitStream& CPL_JoinRequest::serialize(SLNet::BitStream &os) {
	Payload::serialize(os);
	os.Write(playerName, MAX_PLAYER_NAME_LEN);
	return os;
}

void CPL_JoinRequest::deserialize(SLNet::BitStream &is) {
	Payload::deserialize(is);
	is.Read(playerName, MAX_PLAYER_NAME_LEN);
}