#pragma once
#include "Serializable.h"
#include "SolarSystem.h"

#define SOCKET_BUF_LEN 1024 // Size of the buffer that the sockets use to Read incoming data
#define SERVER_UDP_PORT 7263
#define MAX_PLAYERS 12
#define MAX_PLAYER_NAME_LEN 20 // Same length as steam's

#define GUID_LEN 37

#define MS_PER_S 1000
#define NETWORK_TICKRATE 60
#define NETWORK_TICKRATE_SEC (1/NETWORK_TICKRATE)  // Network tickrate per sec
#define NETWORK_TICKRATE_MS (MS_PER_S/NETWORK_TICKRATE) // Network Tickrate per ms

namespace Netcom {
	static unsigned char GetPacketIdentifier(SLNet::Packet *p) {
		if ((unsigned char) p->data[0] == ID_TIMESTAMP)
			return (unsigned char) p->data[sizeof(SLNet::MessageID) + sizeof(SLNet::Time)];
		else
			return (unsigned char) p->data[0];
	}

	enum class ConnectDenyReason : char {
		LOBBY_FULL
	};

	//////////////////////////////////////////////////////////////////////////
	// PAYLOADS
	//////////////////////////////////////////////////////////////////////////
	class Payload : public Serializable {
	public:
		enum Type {
			// SERVER -> CLIENT
			SVR_LOBBY_STATE = ID_USER_PACKET_ENUM + 1,
			SVR_JOIN_RESPONSE,
			SVR_LEVEL_LOAD,
			SVR_AUTHORITY_LEVEL,
			SVR_SPAWN_RESPONSE,
			SVR_STATE_TICK,

			// CLIENT -> SERVER
			CLNT_JOIN_REQUEST,
			CLNT_LOBBY_RDY_STATE,
			CLNT_REQUEST_AUTH_LEVEL,
			/// Called from a server ADM client which is verified on server
			CLNT_LAUNCH_GAME ,
			CLNT_SPAWN_REQUEST,
			CLNT_GAME_OBJ_UPDATE

		} type;

		Payload(Payload::Type type) : type(type) {}
		virtual ~Payload() = default;
		virtual SLNet::BitStream& serialize(SLNet::BitStream &os) override;
		virtual void deserialize(SLNet::BitStream &is) override;
	};

	//========================================================================
	// SHARED

	class UdtObjectPart : public Serializable {
	public:
		enum class Type : char {
			TRANSFORM,
			SPACESHIP_ATTRS
		} type;
		UdtObjectPart() {}
		UdtObjectPart(const xg::Guid &guid, UdtObjectPart::Type type) 
			: gameObjectGuid(guid), type(type) {}
		virtual SLNet::BitStream& serialize(SLNet::BitStream &os) override;
		virtual void deserialize(SLNet::BitStream &is) override;

		xg::Guid gameObjectGuid;
	};

	class UdtTransform : public UdtObjectPart {
	public:
		UdtTransform() : UdtTransform(xg::newGuid(), Transform{}) {}
		UdtTransform(const xg::Guid &guid, const Transform &t)
			: UdtObjectPart(guid, Type::TRANSFORM), transform(t) {
		}
		virtual SLNet::BitStream& serialize(SLNet::BitStream &os) override;
		virtual void deserialize(SLNet::BitStream &is) override;

		Transform transform;
	};

	class UdtSpaceshipAttrs : public UdtObjectPart {
	public:
		UdtSpaceshipAttrs() : UdtSpaceshipAttrs(xg::newGuid(), 0, 0) {}
		UdtSpaceshipAttrs(const xg::Guid &guid, float hull, float shield)
			: UdtObjectPart(guid, Type::SPACESHIP_ATTRS),
			hull(hull), shield(shield) {
		}
		virtual SLNet::BitStream& serialize(SLNet::BitStream &os) override;
		virtual void deserialize(SLNet::BitStream &is) override;

		float hull, shield;
	};

	//========================================================================
	// SERVER -> CLIENT

	class SPL_JoinResponse : public Payload {
	public:
		SPL_JoinResponse() : Payload(SVR_JOIN_RESPONSE) {}
		SLNet::BitStream& serialize(SLNet::BitStream &os) override;
		virtual void deserialize(SLNet::BitStream &is) override;

		SLNet::RakNetGUID playerId;
	};

	//! Used to notify all players in a lobby to move into the game session
	class SPL_LevelLoad : public Payload {
	public:
		SPL_LevelLoad() : Payload(SVR_LEVEL_LOAD), system() {}
		SPL_LevelLoad(SolarSystem system) : Payload(SVR_LEVEL_LOAD), system(system) {}
		SLNet::BitStream& serialize(SLNet::BitStream &os) override;
		virtual void deserialize(SLNet::BitStream &is) override;

		SolarSystem system;
	};

	//! Used as a broadcast to all players in a lobby
	class SPL_LobbyState : public Payload {
	public:
		struct Player {
			// If larger than MAX_PLAYER_NAME_LEN - 1, name will be altered to fit
			std::string name = "";
			bool ready = false;
		};
		SPL_LobbyState() : Payload(SVR_LOBBY_STATE) {}
		SPL_LobbyState(int16_t maxPlayers, int16_t lobbySize)
			: Payload(SVR_LOBBY_STATE), maxPlayers(maxPlayers), lobbySize(lobbySize) {
		}
		SLNet::BitStream& serialize(SLNet::BitStream &os) override;
		virtual void deserialize(SLNet::BitStream &is) override;

		// Number of players allowed in the lobby
		int16_t maxPlayers = 0;
		int16_t lobbySize = 0;

		// List of players, in order of oldest connections to newest
		std::vector<Player> players;
	};

	//! Notifies specific client of authority level
	class SPL_AuthorityLevel : public Payload {
	public:
		enum Level {
			PLAYER,
			ADMIN
		} level;
		SPL_AuthorityLevel() : Payload(SVR_AUTHORITY_LEVEL) {}
		SLNet::BitStream& serialize(SLNet::BitStream &os) override;
		virtual void deserialize(SLNet::BitStream &is) override;
	};

	class SPL_SpawnResponse : public Payload {
	public:
		SPL_SpawnResponse() : SPL_SpawnResponse(nullptr) {}
		SPL_SpawnResponse(GameObject *gameObject)
			: Payload(Payload::SVR_SPAWN_RESPONSE), 
			gameObject(gameObject), approved(true) {}
		SLNet::BitStream& serialize(SLNet::BitStream &os) override;
		virtual void deserialize(SLNet::BitStream &is) override;

		bool approved = false;
		GameObject *gameObject;
	};

	class SPL_StateTick : public Payload {
	public:
		//! If changes == nullptr, class will have ownership of updates
		SPL_StateTick(std::vector<UdtObjectPart*> *changes)
			: Payload(Payload::SVR_STATE_TICK), updates(changes) {
			if (!changes) { ownsUpdates = true; updates = new std::vector<UdtObjectPart *>; }
		}
		virtual ~SPL_StateTick();
		SLNet::BitStream& serialize(SLNet::BitStream &os) override;
		virtual void deserialize(SLNet::BitStream &is) override;

		std::vector<UdtObjectPart*> *updates;
	private:
		bool ownsUpdates = false;
	};
	//========================================================================

	//========================================================================
	// CLIENT -> SERVER
	class CPL_JoinRequest : public Payload {
	public:
		CPL_JoinRequest() : Payload(Payload::CLNT_JOIN_REQUEST) {}
		// If larger than MAX_PLAYER_NAME_LEN - 1, name will be altered to fit
		CPL_JoinRequest(std::string playerName);
		SLNet::BitStream& serialize(SLNet::BitStream &os) override;
		virtual void deserialize(SLNet::BitStream &is) override;

		//! raw string with null-terminator
		char playerName[MAX_PLAYER_NAME_LEN];
	};

	class CPL_ReadyState : public Payload {
	public:
		CPL_ReadyState() : Payload(Payload::CLNT_LOBBY_RDY_STATE) {}
		CPL_ReadyState(bool ready) 
			: Payload(Payload::CLNT_LOBBY_RDY_STATE), ready(ready) {}
		SLNet::BitStream& serialize(SLNet::BitStream &os) override;
		virtual void deserialize(SLNet::BitStream &is) override;

		bool ready = false;
	};

	class CPL_RequestAuthLevel : public Payload {
	public:
		CPL_RequestAuthLevel() : Payload(CLNT_REQUEST_AUTH_LEVEL) {}
	};

	class CPL_LaunchGame : public Payload {
	public:
		CPL_LaunchGame() : Payload(CLNT_LAUNCH_GAME) {}
	};

	class CPL_SpawnRequest : public Payload {
	public:
		CPL_SpawnRequest() : Payload(CLNT_SPAWN_REQUEST) {}
		CPL_SpawnRequest(GoType t, glm::vec3 pos) 
			: Payload(CLNT_SPAWN_REQUEST), type(t), pos(pos) {}
		SLNet::BitStream& serialize(SLNet::BitStream &os) override;
		virtual void deserialize(SLNet::BitStream &is) override;

		GoType type;
		glm::vec3 pos;
	};

	class CPL_GameObjectUpdate : public Payload {
	public:
		CPL_GameObjectUpdate() : Payload(CLNT_GAME_OBJ_UPDATE) {}
		CPL_GameObjectUpdate(UdtObjectPart *part)
			: Payload(CLNT_GAME_OBJ_UPDATE), part(part) {}
		virtual SLNet::BitStream& serialize(SLNet::BitStream &os) override;
		virtual void deserialize(SLNet::BitStream &is) override;
		UdtObjectPart *part;
	};
	//========================================================================
}
