#include "stdafx.h"
#include "GameObject.h"
#include "net_common.h"


SLNet::BitStream& Netcom::GameObject::serialize(SLNet::BitStream &os) {
	os.Write(((std::string)guid).c_str(), GUID_LEN);
	os.Write((char*) &type, sizeof(type));
	os.Write((char*)&owner.g, sizeof(owner.g));
	transform.serialize(os);
	return os;
}

void Netcom::GameObject::deserialize(SLNet::BitStream &is) {
	char id[GUID_LEN];
	unsigned long long ownerId;

	is.Read(id, GUID_LEN); 
	is.Read((char*) &type, sizeof(type));
	is.Read((char*)&ownerId, sizeof(ownerId));
	transform.deserialize(is);

	owner = SLNet::RakNetGUID(ownerId);
	guid = xg::Guid(std::string(id));
}
