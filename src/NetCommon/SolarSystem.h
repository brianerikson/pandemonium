#pragma once
#include "Serializable.h"
#include "Planet.h"

namespace Netcom {
	class SolarSystem : public Serializable {
	public:
		SolarSystem() {}
		virtual SLNet::BitStream& serialize(SLNet::BitStream &os) override;
		virtual void deserialize(SLNet::BitStream &is) override;

		std::vector<Planet> planets;
	};
}